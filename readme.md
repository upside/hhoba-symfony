#### Запуск docker в первый раз для дев среды

###### Нужно для совместимости с windows
`docker volume create --name=postgres_database`

`docker volume create --name=clickhouse_database`

###### Запускаем контейнеры
`docker-compose up -d`

###### Устанавливаем зависимости
`docker exec -it hhoba_php composer install`

###### Создаём миграцию на основе Entity
`docker exec -it hhoba_php php bin/console make:migration`

###### Применяем миграцию
`docker exec -it hhoba_php php bin/console doctrine:migrations:migrate`

###### Загружаем тестовые данные
`docker exec -it hhoba_php php bin/console doctrine:fixtures:load`

---


kamayev.shura@inbox.ru

dmitriy.platonov.91@bk.ru

ivanovsaniancrf@mail.ua

dima_demidov_2021@list.ru
