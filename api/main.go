package main

import (
	"hhoba/api/src/apiserver"
	"log"
)

func main() {

	c := apiserver.NewConfig()
	s := apiserver.NewAPIServer(c)

	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
}
