package global

import "github.com/json-iterator/go"

func GetJson() jsoniter.API {
	return jsoniter.ConfigCompatibleWithStandardLibrary
}
