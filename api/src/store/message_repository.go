package store

import (
	"context"
	"hhoba/api/src/messages"
)

type MessageRepository struct {
	store *Store
}

func (r *MessageRepository) Create(m messages.Message) error {

	_, err := r.store.pool.Exec(
		context.Background(),
		"INSERT INTO messenger_messages (body, headers, queue_name, created_at, available_at) VALUES ($1, $2, $3, $4, $5)",
		m.Body(),
		m.Headers(),
		m.QueueName(),
		m.CreatedAt(),
		m.AvailableAt(),
	)

	if err != nil {
		return err
	}

	return nil
}
