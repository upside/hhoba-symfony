package store

import (
	"os"
)

type Config struct {
	DatabaseUrl string
}

func NewConfig() *Config {

	databaseUrl, exists := os.LookupEnv("DATABASE_URL")

	if exists == false {
		databaseUrl = "host=localhost port=5432 user=hhoba password=hhoba dbname=hhoba sslmode=disable"
	}

	return &Config{
		DatabaseUrl: databaseUrl,
	}
}
