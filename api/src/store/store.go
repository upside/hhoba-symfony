package store

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	config            *Config
	pool              *pgxpool.Pool
	messageRepository *MessageRepository
}

func NewStore(c *Config) (*Store, error) {

	pool, err := pgxpool.Connect(context.Background(), c.DatabaseUrl)

	if err != nil {
		return nil, err
	}

	return &Store{
		config: c,
		pool:   pool,
	}, nil
}

func (s *Store) MessageRepository() *MessageRepository {

	if s.messageRepository == nil {
		s.messageRepository = &MessageRepository{store: s}
	}

	return s.messageRepository
}
