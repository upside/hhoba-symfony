package handlers

import (
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
	"hhoba/api/src/messages"
	"hhoba/api/src/store"
	"net/http"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func GetClick(storage *store.Store, logger *logrus.Logger) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		streamUuid, err := uuid.Parse(mux.Vars(r)["stream"])

		if err != nil {
			logger.Error(err)
			w.WriteHeader(http.StatusBadRequest)
			_ = json.NewEncoder(w).Encode("Неверный формармат запроса")
			return
		} else {
			clickUuid := uuid.New().String()

			w.WriteHeader(http.StatusOK)
			_ = json.NewEncoder(w).Encode(clickUuid)

			message := messages.NewClickMessage(streamUuid.String(), clickUuid, messages.QueueDefault)

			if err := storage.MessageRepository().Create(message); err != nil {
				logger.Error(err)
			}
		}
	}
}
