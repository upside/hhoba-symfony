package handlers

import (
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"hhoba/api/src/global"
	"hhoba/api/src/messages"
	"hhoba/api/src/store"
	"net/http"
)

func SendLead(storage *store.Store, logger *logrus.Logger) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		clickUuid, err := uuid.Parse(mux.Vars(r)["click"])

		if err != nil {
			logger.Error(err)
			w.WriteHeader(http.StatusBadRequest)
			_ = global.GetJson().NewEncoder(w).Encode("Неверный формармат запроса")
			return
		} else {
			leadUuid := uuid.New().String()
			phone := r.FormValue("phone")

			w.WriteHeader(http.StatusOK)
			_ = global.GetJson().NewEncoder(w).Encode(leadUuid)

			message := messages.NewLeadMessage(clickUuid.String(), leadUuid, phone, messages.QueueDefault)

			if err := storage.MessageRepository().Create(message); err != nil {
				logger.Error(err)
			}
		}
	}
}
