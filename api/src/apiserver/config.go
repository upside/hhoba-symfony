package apiserver

import (
	"hhoba/api/src/store"
	"os"
)

type Config struct {
	ServerName string
	Port       string
	LogLevel   string
	Store      *store.Config
}

func NewConfig() *Config {

	serverName, exists := os.LookupEnv("SERVER_NAME")
	if exists == false {
		serverName = "APIServer"
	}

	port, exists := os.LookupEnv("SERVER_PORT")
	if exists == false {
		port = ":8080"
	}

	logLevel, exists := os.LookupEnv("LOG_LEVEL")
	if exists == false {
		logLevel = "debug"
	}

	return &Config{
		ServerName: serverName,
		Port:       port,
		LogLevel:   logLevel,
		Store:      store.NewConfig(),
	}
}
