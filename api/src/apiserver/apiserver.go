package apiserver

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"hhoba/api/src/handlers"
	"hhoba/api/src/store"
	"net/http"
)

type APIServer struct {
	config *Config
	logger *logrus.Logger
	router *mux.Router
	store  *store.Store
}

func NewAPIServer(config *Config) *APIServer {

	return &APIServer{
		config: config,
		logger: logrus.New(),
		router: mux.NewRouter(),
	}
}

func (s *APIServer) Start() error {

	if err := s.configureLogger(); err != nil {
		return err
	}

	if err := s.configureStore(); err != nil {
		return err
	}

	s.configureRouter()
	s.logger.Info("Starting " + s.config.ServerName + " on port" + s.config.Port + " ...")

	return http.ListenAndServe(s.config.Port, s.router)
}

func (s *APIServer) configureLogger() error {

	level, err := logrus.ParseLevel(s.config.LogLevel)

	if err != nil {
		return err
	}

	s.logger.SetLevel(level)

	return nil
}

func (s *APIServer) configureStore() error {

	st, err := store.NewStore(s.config.Store)

	if err != nil {
		return err
	}

	s.store = st

	return nil
}

func (s *APIServer) configureRouter() {
	s.router.HandleFunc("/click/{stream}", handlers.GetClick(s.store, s.logger)).Methods(http.MethodGet, http.MethodOptions)
	s.router.HandleFunc("/lead/{click}", handlers.SendLead(s.store, s.logger)).Methods(http.MethodPost, http.MethodOptions)

	s.router.Use(mux.CORSMethodMiddleware(s.router))
	s.router.Use(jsonMiddleware)
}
