package messages

import (
	"hhoba/api/src/global"
	"time"
)

type Click struct {
	*ClickBody `json:"body"`
	*Header    `json:"headers"`
	Queue      string `json:"queue_name"`
	Created    string `json:"created_at"`
	Available  string `json:"available_at"`
}

type ClickBody struct {
	StreamUuid string              `json:"streamUuid"`
	ClickUuid  string              `json:"clickUuid"`
	Created    string              `json:"createdAt"`
	Params     map[string][]string `json:"params"`
}

func NewClickMessage(streamUuid string, clickUuid string, queue string) *Click {

	now := time.Now().Format(global.DatetimeYmdhis)

	return &Click{
		ClickBody: &ClickBody{
			StreamUuid: streamUuid,
			ClickUuid:  clickUuid,
			Created:    now,
			Params:     nil,
		},
		Header: &Header{
			MessageType:  ClickMessageType,
			BusNameStamp: BusNameDefault,
			ContentType:  ContentTypeApplicationJson,
		},
		Queue:     queue,
		Created:   now,
		Available: now,
	}
}

func (m *Click) Body() string {

	body, err := global.GetJson().Marshal(&m.ClickBody)

	if err != nil {
		return ""
	}

	return string(body)
}

func (m *Click) Headers() string {
	headers, err := global.GetJson().Marshal(&m.Header)

	if err != nil {
		return ""
	}

	return string(headers)
}

func (m *Click) QueueName() string {
	return m.Queue
}

func (m *Click) CreatedAt() string {
	return m.Created
}

func (m *Click) AvailableAt() string {
	return m.Available
}

func (m *Click) String() string {
	bytes, _ := global.GetJson().Marshal(&m)
	return string(bytes)
}
