package messages

type Message interface {
	Body() string
	Headers() string
	QueueName() string
	CreatedAt() string
	AvailableAt() string
	String() string
}
