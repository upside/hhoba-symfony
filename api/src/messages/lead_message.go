package messages

import (
	"hhoba/api/src/global"
	"time"
)

type Lead struct {
	*LeadBody `json:"body"`
	*Header   `json:"headers"`
	Queue     string `json:"queue_name"`
	Created   string `json:"created_at"`
	Available string `json:"available_at"`
}

type LeadBody struct {
	ClickUuid string              `json:"clickUuid"`
	LeadUuid  string              `json:"leadUuid"`
	Phone     string              `json:"phone"`
	Created   string              `json:"createdAt"`
	Params    map[string][]string `json:"params"`
}

func NewLeadMessage(clickUuid string, leadUuid string, phone string, queue string) *Lead {

	now := time.Now().Format(global.DatetimeYmdhis)

	return &Lead{
		LeadBody: &LeadBody{
			ClickUuid: clickUuid,
			LeadUuid:  leadUuid,
			Phone:     phone,
			Created:   now,
			Params:    nil,
		},
		Header: &Header{
			MessageType:  LeadMessageType,
			BusNameStamp: BusNameDefault,
			ContentType:  ContentTypeApplicationJson,
		},
		Queue:     queue,
		Created:   now,
		Available: now,
	}
}

func (m *Lead) Body() string {

	body, err := global.GetJson().Marshal(&m.LeadBody)

	if err != nil {
		return ""
	}

	return string(body)
}

func (m *Lead) Headers() string {
	headers, err := global.GetJson().Marshal(&m.Header)

	if err != nil {
		return ""
	}

	return string(headers)
}

func (m *Lead) QueueName() string {
	return m.Queue
}

func (m *Lead) CreatedAt() string {
	return m.Created
}

func (m *Lead) AvailableAt() string {
	return m.Available
}

func (m *Lead) String() string {
	bytes, _ := global.GetJson().Marshal(&m)
	return string(bytes)
}
