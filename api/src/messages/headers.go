package messages

type Header struct {
	MessageType  string `json:"type"`
	BusNameStamp string `json:"X-Message-Stamp-Symfony\\Component\\Messenger\\Stamp\\BusNameStamp"`
	ContentType  string `json:"Content-Type"`
}
