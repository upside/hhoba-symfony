module hhoba/api

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/jackc/pgx/v4 v4.6.0
	github.com/json-iterator/go v1.1.9
	github.com/sirupsen/logrus v1.6.0
)
