<?php

namespace App\Security\Voter;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\User;
use App\Security\RolesEnum;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Доступ к кабинетам агенства
 */
class AgencyCabinetVoter extends Voter
{
    const SHOW = 'show';
    const EDIT = 'edit';
    const SPEND_FUNDS_TO_CABINET = 'spend_funds_to_cabinet';
    const SPEND_FUNDS_CABINET_TO_CABINET = 'spend_funds_cabinet_to_cabinet';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::EDIT, self::SHOW, self::SPEND_FUNDS_TO_CABINET, self::SPEND_FUNDS_CABINET_TO_CABINET]) && $subject instanceof AgencyCabinet;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        // ROLE_ADMIN can do anything! The power!
        if ($this->security->isGranted(RolesEnum::ADMIN)) {
            return true;
        }

        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($user, $subject);
                break;
            case self::SHOW:
                return $this->canView($user, $subject);
                break;
            case self::SPEND_FUNDS_TO_CABINET:
                return $this->canSpendFundsToCabinet($user, $subject);
                break;
            case self::SPEND_FUNDS_CABINET_TO_CABINET:
                return $this->canSpendfundsCabinetToCabinet($user, $subject);
                break;
        }

        return false;
    }


    private function canEdit(User $user, AgencyCabinet $agencyCabinet)
    {
        return $agencyCabinet->getUser() === $user;
    }

    private function canView(User $user, AgencyCabinet $agencyCabinet)
    {
        return $agencyCabinet->getUser() === $user;
    }

    private function canSpendFundsToCabinet(User $user, AgencyCabinet $agencyCabinet)
    {
        return $agencyCabinet->getUser() === $user;
    }

    private function canSpendfundsCabinetToCabinet(User $user, AgencyCabinet $agencyCabinet)
    {
        return $agencyCabinet->getUser() === $user;
    }
}
