<?php

namespace App\Security\Voter;

use App\Entity\Main\Stream;
use App\Entity\Main\User;
use App\Security\RolesEnum;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Доступ к потокам
 */
class StreamVoter extends Voter
{
    const SHOW = 'show';
    const EDIT = 'edit';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::EDIT, self::SHOW]) && $subject instanceof Stream;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        // ROLE_ADMIN can do anything! The power!
        if ($this->security->isGranted(RolesEnum::ADMIN)) {
            return true;
        }

        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($subject, $user);
                break;
            case self::SHOW:
                return $this->canView($subject, $user);
                break;
        }

        return false;
    }

    private function canEdit(Stream $stream, User $user)
    {
        return $stream->getTrafficsource()->getUser() === $user;
    }

    private function canView(Stream $stream, User $user)
    {
        return $stream->getTrafficsource()->getUser() === $user;
    }
}
