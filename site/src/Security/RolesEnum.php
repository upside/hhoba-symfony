<?php

namespace App\Security;

interface RolesEnum
{
    public const USER = 'ROLE_USER';
    public const ADMIN = 'ROLE_ADMIN';
    public const ALLOWED_TO_SWITCH = 'ROLE_ALLOWED_TO_SWITCH';
    public const PREVIOUS_ADMIN = 'IS_IMPERSONATOR';
}
