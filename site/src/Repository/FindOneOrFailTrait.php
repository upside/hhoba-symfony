<?php

namespace App\Repository;

use Doctrine\ORM\EntityNotFoundException;

trait FindOneOrFailTrait
{
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return object
     * @throws EntityNotFoundException
     */
    public function findOneByOrFail(array $criteria, array $orderBy = null)
    {
        $entity = $this->findOneBy($criteria, $orderBy);
        if (is_null($entity)) {
            throw new EntityNotFoundException();
        }

        return $entity;
    }
}
