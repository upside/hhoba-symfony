<?php

namespace App\Repository;

trait FindOrCreateTrait
{
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return object
     */
    public function findOrCreate(array $criteria, array $orderBy = null)
    {
        $entity = $this->findOneBy($criteria, $orderBy);
        if (is_null($entity)) {

            $entityClass = $this->getClassName();
            $entity = new $entityClass();

            foreach ($criteria as $field => $value) {
                $setter = 'set' . ucfirst($field);
                if (method_exists($entity, $setter)) {
                    $entity->$setter($value);
                }
            }

            $this->_em->persist($entity);
            $this->_em->flush();
        }

        return $entity;
    }
}
