<?php

namespace App\Repository\Statistic;

use App\Entity\Main\Lead;
use App\Entity\Main\User;
use App\Entity\Statistic\Statistic;
use App\Helpers\DateTimeHelper;
use App\Repository\Main\TokenDictionaryRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Statistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistic[]    findAll()
 * @method Statistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticRepository extends ServiceEntityRepository
{
    /**
     * @var TokenDictionaryRepository
     */
    private TokenDictionaryRepository $tokenDictionaryRepository;

    /**
     * @param ManagerRegistry $registry
     * @param TokenDictionaryRepository $tokenDictionaryRepository
     */
    public function __construct(ManagerRegistry $registry, TokenDictionaryRepository $tokenDictionaryRepository)
    {
        parent::__construct($registry, Statistic::class);
        $this->tokenDictionaryRepository = $tokenDictionaryRepository;
    }

    /**
     * @param string $alias
     * @param null $indexBy
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null): QueryBuilder
    {
        return $this->getEntityManager()
            ->getConnection()
            ->createQueryBuilder()
            ->select($alias)
            ->from('statistics', $alias);
    }

    /**
     * @return QueryBuilder
     */
    public function getStat(): QueryBuilder
    {
        return $this->createQueryBuilder('s')
            ->select([
                's.user_id',
                'toInt32(count(s.click_id)) as totalClicks',
                'round((toInt32(coalesce(sumIf(s.lead_price, s.lead_status = :leadStatusConfirm), 0)) / toInt32(count(s.click_id))), 2) as epc',
                'round((toInt32(coalesce(countIf(s.lead_status = :leadStatusConfirm), 0)) / toInt32(count(s.click_id))), 2) as cr',
                'toInt32(coalesce(countIf(s.lead_id is not null), 0)) as totalLeads',
                'toInt32(coalesce(countIf(s.lead_status = :leadStatusNew), 0)) as newLeads',
                'toInt32(coalesce(countIf(s.lead_status = :leadStatusConfirm), 0)) as confirmLeads',
                'toInt32(coalesce(countIf(s.lead_status = :leadStatusDecline), 0)) as declineLeads',
                'toInt32(coalesce(countIf(s.lead_status = :leadStatusTrash), 0)) as trashLeads',
                'toInt32(coalesce(sum(s.lead_price), 0)) as totalMoney',
                'toInt32(coalesce(sumIf(s.lead_price, s.lead_status = :leadStatusNew), 0)) as newMoney',
                'toInt32(coalesce(sumIf(s.lead_price, s.lead_status = :leadStatusConfirm), 0)) as confirmMoney',
                'toInt32(coalesce(sumIf(s.lead_price, s.lead_status = :leadStatusDecline), 0)) as declineMoney',
                'toInt32(coalesce(sumIf(s.lead_price, s.lead_status = :leadStatusTrash), 0)) as trashMoney',
            ])
            ->groupBy('s.user_id')
            ->setParameter(':leadStatusNew', Lead::STATUS_NEW, Types::INTEGER)
            ->setParameter(':leadStatusConfirm', Lead::STATUS_CONFIRM, Types::INTEGER)
            ->setParameter(':leadStatusDecline', Lead::STATUS_DECLINE, Types::INTEGER)
            ->setParameter(':leadStatusTrash', Lead::STATUS_TRASH, Types::INTEGER);
    }

    /**
     * @param User $user
     * @param DateTimeInterface|null $dateFrom
     * @param DateTimeInterface|null $dateTo
     * @param string[] $groups
     * @param array $filters
     * @return array
     */
    public function getStatForUser(
        User $user,
        ?DateTimeInterface $dateFrom,
        ?DateTimeInterface $dateTo,
        array $groups = [],
        array $filters = []
    ): array
    {

        $dateFrom = $dateFrom ?? new DateTime(DateTimeHelper::FIRST_DAY);
        $dateTo = $dateTo ?? new DateTime(DateTimeHelper::NOW);

        $stmt = $this->getStat()
            ->andWhere('user_id = :user_id')
            ->andWhere('s.date between :date_from and :date_to')
            ->setParameter(':user_id', $user->getId(), Types::INTEGER)
            ->setParameter(':date_from', $dateFrom->format(DateTimeHelper::DATE_YMD), Types::STRING)
            ->setParameter(':date_to', $dateTo->format(DateTimeHelper::DATE_YMD), Types::STRING);

        $this->addGroups($stmt, ...$groups);

        foreach ($filters as $filterKey => $filterValue) {
            $stmt->setParameter(':' . $filterKey, $filterValue, Connection::PARAM_INT_ARRAY);
            $stmt->andWhere('s.' . $filterKey . ' in (:' . $filterKey . ')');
        }

        return $stmt->execute()->fetchAll();
    }

    /**
     * Добавляет к запросу группировки
     *
     * @param QueryBuilder $queryBuilder
     * @param string ...$groups
     * @return QueryBuilder
     */
    private function addGroups(QueryBuilder $queryBuilder, string ...$groups): QueryBuilder
    {
        $tokenFields = [
            'token1_id',
            'token2_id',
            'token3_id',
            'token4_id',
            'token5_id',
            'token6_id',
            'token7_id',
            'token8_id',
            'token9_id',
            'token10_id',
            'token11_id',
            'token12_id',
            'token13_id',
            'token14_id',
            'token15_id',
            'token16_id',
        ];

        foreach ($groups as $group) {
            $queryBuilder->addGroupBy('s.' . $group);

            switch (true) {
                case $group === 'geo_id':
                    $queryBuilder->addSelect(["dictGetStringOrDefault('geo', 'iso_2', toUInt64(s.geo_id), toString(s.geo_id)) AS geo_id"]);
                    break;
                case $group === 'stream_id':
                    $queryBuilder->addSelect(["dictGetStringOrDefault('streams', 'name', toUInt64(s.stream_id), toString(s.stream_id)) AS stream_id"]);
                    break;
                case $group === 'offer_id':
                    $queryBuilder->addSelect(["dictGetStringOrDefault('offers', 'name', toUInt64(s.offer_id), toString(s.offer_id)) AS offer_id"]);
                    break;
                case in_array($group, $tokenFields):
                    $queryBuilder->addSelect(["dictGetStringOrDefault('tokens', 'value', toUInt64(s." . $group . "), toString(s." . $group . ")) AS " . $group]);
                    break;
                default:
                    $queryBuilder->addSelect(['s.' . $group]);
            }
        }

        return $queryBuilder;
    }

    /**
     * @param Statistic ...$statistics
     * @return bool
     * @throws DBALException
     */
    public function persist(Statistic ...$statistics)
    {
        $queryBuilder = $this->createQueryBuilder('s');
        $values = [];
        foreach ($statistics as $statistic) {
            $values[] = '(' . implode(', ', [
                    $queryBuilder->createNamedParameter($statistic->getClickId(), Types::INTEGER), //click_id
                    $queryBuilder->createNamedParameter($statistic->getClickCreatedAt()->format(DateTimeHelper::DATE_TIME_YMD), Types::STRING), //click_created_at
                    $queryBuilder->createNamedParameter($statistic->getTrafficSourceId(), Types::INTEGER), //click_created_at
                    $queryBuilder->createNamedParameter($statistic->getUserId(), Types::INTEGER), //user_id
                    $queryBuilder->createNamedParameter($statistic->getStreamId(), Types::INTEGER), //stream_id
                    $queryBuilder->createNamedParameter($statistic->getOfferId(), Types::INTEGER), //offer_id
                    $queryBuilder->createNamedParameter($statistic->getGeoId(), Types::INTEGER), //geo_id
                    $queryBuilder->createNamedParameter($statistic->getLeadId(), Types::INTEGER), //lead_id
                    $queryBuilder->createNamedParameter($statistic->getLeadStatus(), Types::INTEGER), //lead_status
                    $queryBuilder->createNamedParameter($statistic->getLeadPrice(), Types::DECIMAL), //lead_price
                    $queryBuilder->createNamedParameter($statistic->getLeadCurrencyCode(), Types::STRING), //lead_currency_code
                    $queryBuilder->createNamedParameter($statistic->getLeadCurrencyRate(), Types::DECIMAL), //lead_currency_rate
                    $queryBuilder->createNamedParameter(is_null($statistic->getLeadCreatedAt()) ? null : $statistic->getLeadCreatedAt()->format(DateTimeHelper::DATE_TIME_YMD), Types::STRING), //lead_created_at
                    $queryBuilder->createNamedParameter($statistic->getHour(), Types::INTEGER), //hour
                    $queryBuilder->createNamedParameter($statistic->getDay(), Types::INTEGER), //day
                    $queryBuilder->createNamedParameter($statistic->getWeek(), Types::INTEGER), //week
                    $queryBuilder->createNamedParameter($statistic->getMonth(), Types::INTEGER), //month
                    $queryBuilder->createNamedParameter($statistic->getYear(), Types::INTEGER), //year
                    $queryBuilder->createNamedParameter($statistic->getToken1Id(), Types::INTEGER), //token1_id
                    $queryBuilder->createNamedParameter($statistic->getToken2Id(), Types::INTEGER), //token2_id
                    $queryBuilder->createNamedParameter($statistic->getToken3Id(), Types::INTEGER), //token3_id
                    $queryBuilder->createNamedParameter($statistic->getToken4Id(), Types::INTEGER), //token4_id
                    $queryBuilder->createNamedParameter($statistic->getToken5Id(), Types::INTEGER), //token5_id
                    $queryBuilder->createNamedParameter($statistic->getToken6Id(), Types::INTEGER), //token6_id
                    $queryBuilder->createNamedParameter($statistic->getToken7Id(), Types::INTEGER), //token7_id
                    $queryBuilder->createNamedParameter($statistic->getToken8Id(), Types::INTEGER), //token8_id
                    $queryBuilder->createNamedParameter($statistic->getToken9Id(), Types::INTEGER), //token9_id
                    $queryBuilder->createNamedParameter($statistic->getToken10Id(), Types::INTEGER), //token10_id
                    $queryBuilder->createNamedParameter($statistic->getToken11Id(), Types::INTEGER), //token11_id
                    $queryBuilder->createNamedParameter($statistic->getToken12Id(), Types::INTEGER), //token12_id
                    $queryBuilder->createNamedParameter($statistic->getToken13Id(), Types::INTEGER), //token13_id
                    $queryBuilder->createNamedParameter($statistic->getToken14Id(), Types::INTEGER), //token14_id
                    $queryBuilder->createNamedParameter($statistic->getToken15Id(), Types::INTEGER), //token15_id
                    $queryBuilder->createNamedParameter($statistic->getToken16Id(), Types::INTEGER), //token16_id
                    $queryBuilder->createNamedParameter($statistic->getDate()->format(DateTimeHelper::DATE_TIME_YMD), Types::STRING), //date
                    $queryBuilder->createNamedParameter($statistic->getVersion(), Types::INTEGER), //version
                ]) . ')';
        }

        $fields = [
            'click_id',
            'click_created_at',
            'traffic_source_id',
            'user_id',
            'stream_id',
            'offer_id',
            'geo_id',
            'lead_id',
            'lead_status',
            'lead_price',
            'lead_currency_code',
            'lead_currency_rate',
            'lead_created_at',
            'hour',
            'day',
            'week',
            'month',
            'year',
            'token1_id',
            'token2_id',
            'token3_id',
            'token4_id',
            'token5_id',
            'token6_id',
            'token7_id',
            'token8_id',
            'token9_id',
            'token10_id',
            'token11_id',
            'token12_id',
            'token13_id',
            'token14_id',
            'token15_id',
            'token16_id',
            'date',
            'version',
        ];

        $SQL = 'INSERT INTO statistics (' . implode(', ', $fields) . ') VALUES ' . implode(', ', $values);

        $stmt = $queryBuilder->getConnection()->prepare($SQL);

        return $stmt->execute($queryBuilder->getParameters());
    }
}
