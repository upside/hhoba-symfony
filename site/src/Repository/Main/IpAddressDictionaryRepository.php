<?php

namespace App\Repository\Main;

use App\Entity\Main\IpAddressDictionary;
use App\Repository\FindOrCreateTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IpAddressDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method IpAddressDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method IpAddressDictionary[]    findAll()
 * @method IpAddressDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method IpAddressDictionary      findOrCreate(array $criteria, array $orderBy = null)
 */
class IpAddressDictionaryRepository extends ServiceEntityRepository
{
    use FindOrCreateTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IpAddressDictionary::class);
    }
}
