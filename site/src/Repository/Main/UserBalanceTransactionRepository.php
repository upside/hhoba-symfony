<?php

namespace App\Repository\Main;

use App\Entity\Main\UserBalanceTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserBalanceTransaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserBalanceTransaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserBalanceTransaction[]    findAll()
 * @method UserBalanceTransaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserBalanceTransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserBalanceTransaction::class);
    }
}
