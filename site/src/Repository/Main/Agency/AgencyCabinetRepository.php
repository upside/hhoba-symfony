<?php

namespace App\Repository\Main\Agency;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\User;
use App\Repository\FindOrCreateTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AgencyCabinet|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgencyCabinet|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgencyCabinet[]    findAll()
 * @method AgencyCabinet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method AgencyCabinet      findOrCreate(array $criteria, array $orderBy = null)
 */
class AgencyCabinetRepository extends ServiceEntityRepository
{
    use FindOrCreateTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgencyCabinet::class);
    }

    public function getByUserAndType(User $user, string $type): Query
    {
        return $this->createQueryBuilder('ac')
            ->where('ac.user = :user')
            ->andWhere('ac.type = :type')
            ->orderBy('ac.name')
            ->setParameter(':user', $user)
            ->setParameter(':type', $type)
            ->getQuery();
    }
}
