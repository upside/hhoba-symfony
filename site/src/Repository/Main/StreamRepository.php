<?php

namespace App\Repository\Main;

use App\Entity\Main\Stream;
use App\Entity\Main\User;
use App\Repository\FindOneOrFailTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stream|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stream|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stream      findOneByOrFail(array $criteria, array $orderBy = null)
 * @method Stream[]    findAll()
 * @method Stream[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StreamRepository extends ServiceEntityRepository
{
    use FindOneOrFailTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stream::class);
    }

    /**
     * Возвращает массив доступных пользователю потоков для select`а
     *
     * @param User $user
     * @return array|int|string
     */
    public function getStatisticChoices(User $user): array
    {
        $items = $this->createQueryBuilder('s')
            ->select(['s.id', 's.name'])
            ->join('s.trafficSource', 'ts')
            ->where('ts.user=:userId')
            ->setParameter(':userId', $user->getId())
            ->getQuery()
            ->getScalarResult();

        $choices = [];
        foreach ($items as $item) {
            $choices[$item['name']] = $item['id'];
        }

        return $choices;
    }
}
