<?php

namespace App\Repository\Main;

use App\Entity\Main\Tariff;
use App\Repository\FindOneOrFailTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tariff|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tariff|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tariff      findOneByOrFail(array $criteria, array $orderBy = null)
 * @method Tariff[]    findAll()
 * @method Tariff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TariffRepository extends ServiceEntityRepository
{
    use FindOneOrFailTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tariff::class);
    }
}
