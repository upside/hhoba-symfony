<?php

namespace App\Repository\Main;

use App\Entity\Main\TrafficSource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrafficSource|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrafficSource|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrafficSource[]    findAll()
 * @method TrafficSource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrafficSourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrafficSource::class);
    }
}
