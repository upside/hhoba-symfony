<?php

namespace App\Repository\Main;

use App\Entity\Main\UserAgentDictionary;
use App\Repository\FindOrCreateTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserAgentDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAgentDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAgentDictionary[]    findAll()
 * @method UserAgentDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method UserAgentDictionary      findOrCreate(array $criteria, array $orderBy = null)
 */
class UserAgentDictionaryRepository extends ServiceEntityRepository
{
    use FindOrCreateTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAgentDictionary::class);
    }
}
