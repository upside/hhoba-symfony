<?php

namespace App\Repository\Main;

use App\Entity\Main\Geo;
use App\Entity\Main\User;
use App\Repository\FindOneOrFailTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Geo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Geo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Geo      findOneByOrFail(array $criteria, array $orderBy = null)
 * @method Geo[]    findAll()
 * @method Geo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeoRepository extends ServiceEntityRepository
{
    use FindOneOrFailTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Geo::class);
    }

    /**
     * Возвращает массив доступных пользователю geo для select`а
     *
     * @param User $user
     * @return array|int|string
     */
    public function getStatisticChoices(User $user): array
    {
        $items = $this->createQueryBuilder('g')
            ->select(['g.id', 'g.iso_2'])
            ->join('g.clicks', 'c')
            ->join('c.stream', 's')
            ->join('s.trafficSource', 'ts')
            ->where('ts.user=:userId')
            ->setParameter(':userId', $user->getId())
            ->getQuery()
            ->getArrayResult();

        $choices = [];
        foreach ($items as $item) {
            $choices[$item['iso_2']] = $item['id'];
        }

        return $choices;
    }


    /**
     * @param string ...$codes
     * @return Geo[]
     * @throws QueryException
     */
    public function getByIsoCode(string ...$codes)
    {
        return $this->createQueryBuilder('g')
            ->indexBy('g', 'g.iso_2')
            ->where('g.iso_2 IN (:isoCodes2)')
            ->orWhere('g.iso_3 IN (:isoCodes3)')
            ->setParameter(':isoCodes2', $codes)
            ->setParameter(':isoCodes3', $codes)
            ->getQuery()
            ->getResult();
    }
}
