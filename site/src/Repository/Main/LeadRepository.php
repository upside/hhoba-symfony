<?php

namespace App\Repository\Main;

use App\Entity\Main\Lead;
use App\Entity\Main\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lead|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lead|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lead[]    findAll()
 * @method Lead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lead::class);
    }

    /**
     * Возвращает query для пагинатора в в спмске в админке
     *
     * @return Query
     */
    public function getForAdminList(): Query
    {
        return $this->createQueryBuilder('l')
            ->join('l.click', 'c')
            ->join('l.currency', 'cur')
            ->addSelect(['c', 'cur'])
            ->getQuery();
    }

    /**
     * @param User $user
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getCountByUserAndDate(User $user, string $dateFrom, string $dateTo): array
    {
        $stmt = $this->getEntityManager()->getConnection()->createQueryBuilder()
            ->select([
                'count(l.id) as total',
                'count(l.status = :leadStatusNew OR NULL) as new',
                'count(l.status = :leadStatusConfirm OR NULL) as confirm',
                'count(l.status = :leadStatusDecline OR NULL) as decline',
                'count(l.status = :leadStatusTrash OR NULL) as trash'
            ])
            ->from('leads', 'l')
            ->join('l', 'clicks', 'c', 'c.id = l.click_id')
            ->join('c', 'streams', 's', 's.id = c.stream_id')
            ->join('s', 'traffic_source', 'ts', 'ts.id = s.traffic_source_id')
            ->where('l.created_at BETWEEN :dateFrom AND :dateTo')
            ->andWhere('ts.user_id = :userId')
            ->setParameter(':leadStatusNew', Lead::STATUS_NEW, Types::INTEGER)
            ->setParameter(':leadStatusConfirm', Lead::STATUS_CONFIRM, Types::INTEGER)
            ->setParameter(':leadStatusDecline', Lead::STATUS_DECLINE, Types::INTEGER)
            ->setParameter(':leadStatusTrash', Lead::STATUS_TRASH, Types::INTEGER)
            ->setParameter(':dateFrom', $dateFrom, Types::STRING)
            ->setParameter(':dateTo', $dateTo, Types::STRING)
            ->setParameter(':userId', $user->getId(), Types::INTEGER)
            ->execute();

        return $stmt->fetch();
    }
}
