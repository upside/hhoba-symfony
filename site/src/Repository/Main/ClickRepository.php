<?php

namespace App\Repository\Main;

use App\Entity\Main\Click;
use App\Entity\Main\User;
use App\Repository\FindOneOrFailTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Click|null find($id, $lockMode = null, $lockVersion = null)
 * @method Click|null findOneBy(array $criteria, array $orderBy = null)
 * @method Click      findOneByOrFail(array $criteria, array $orderBy = null)
 * @method Click[]    findAll()
 * @method Click[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClickRepository extends ServiceEntityRepository
{
    use FindOneOrFailTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Click::class);
    }

    /**
     * @param User $user
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCountByUserAndDate(User $user, string $dateFrom, string $dateTo): int
    {
        $queryBuilder = $this->createQueryBuilder('click');

        return $queryBuilder
            ->select(['count(click.id) as cnt'])
            ->join('click.stream', 'stream')
            ->join('stream.trafficSource', 'trafficSource')
            ->where($queryBuilder->expr()->between('click.created_at', ':dateFrom', ':dateTo'))
            ->andWhere('trafficSource.user = :user')
            ->setParameter(':dateFrom', $dateFrom, Types::STRING)
            ->setParameter(':dateTo', $dateTo, Types::STRING)
            ->setParameter(':user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
