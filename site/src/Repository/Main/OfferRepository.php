<?php

namespace App\Repository\Main;

use App\Entity\Main\Lead;
use App\Entity\Main\Offer;
use App\Entity\Main\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    /**
     * Возвращает массив доступных пользователю офферов для select`а
     *
     * @param User $user
     * @return array|int|string
     */
    public function getStatisticChoices(User $user): array
    {
        $items = $this->createQueryBuilder('o')
            ->select(['o.id', 'o.name'])
            ->join('o.streams', 's')
            ->join('s.trafficSource', 'ts')
            ->where('ts.user=:userId')
            ->setParameter(':userId', $user->getId())
            ->getQuery()
            ->getArrayResult();

        $choices = [];
        foreach ($items as $item) {
            $choices[$item['name']] = $item['id'];
        }

        return $choices;
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTopOffers($top = 5): array
    {
        $stmt = $this->getEntityManager()->getConnection()->createQueryBuilder()
            ->select([
                'o.id',
                'o.name',
                'count(l.id) as total',
                'count(l.status = :leadStatusNew OR NULL) as new',
                'count(l.status = :leadStatusConfirm OR NULL) as confirm',
                'count(l.status = :leadStatusDecline OR NULL) as decline',
                'count(l.status = :leadStatusTrash OR NULL) as trash'
            ])
            ->from('offers', 'o')
            ->join('o', 'streams', 's', 'o.id = s.offer_id')
            ->join('s', 'clicks', 'c', 's.id = c.stream_id')
            ->join('c', 'leads', 'l', 'c.id = l.click_id')
            ->groupBy('o.id')
            ->orderBy('total', 'DESC')
            ->setParameter(':leadStatusNew', Lead::STATUS_NEW, Types::INTEGER)
            ->setParameter(':leadStatusConfirm', Lead::STATUS_CONFIRM, Types::INTEGER)
            ->setParameter(':leadStatusDecline', Lead::STATUS_DECLINE, Types::INTEGER)
            ->setParameter(':leadStatusTrash', Lead::STATUS_TRASH, Types::INTEGER)
            ->setMaxResults($top)
            ->execute();

        return $stmt->fetchAll();
    }
}
