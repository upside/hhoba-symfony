<?php

namespace App\Repository\Main;

use App\Entity\Main\TokenDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TokenDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method TokenDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method TokenDictionary[]    findAll()
 * @method TokenDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TokenDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TokenDictionary::class);
    }

    public function getByNameAndValue(string $name, string $value)
    {
        return $this->createQueryBuilder('t')
            ->select(['t.id'])
            ->where('t.name = :name')
            ->andWhere('t.value LIKE :value')
            ->setParameter(':name', $name, Types::STRING)
            ->setParameter(':value', $value, Types::STRING)
            ->distinct()
            ->getQuery()
            ->getScalarResult();
    }
}
