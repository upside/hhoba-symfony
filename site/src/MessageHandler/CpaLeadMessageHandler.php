<?php

namespace App\MessageHandler;

use App\AdvertisersApi\AdvertiserService;
use App\Message\CpaLeadMessage;
use App\Repository\Main\LeadRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CpaLeadMessageHandler implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var LeadRepository
     */
    private LeadRepository $leadRepository;

    /**
     * @var AdvertiserService
     */
    private AdvertiserService $advertiserService;

    /**
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     * @param LeadRepository $leadRepository
     * @param AdvertiserService $advertiserService
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        LeadRepository $leadRepository,
        AdvertiserService $advertiserService
    )
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->leadRepository = $leadRepository;
        $this->advertiserService = $advertiserService;
    }

    /**
     * @param CpaLeadMessage $message
     * @return void
     * @throws Exception
     */
    public function __invoke(CpaLeadMessage $message)
    {
        $lead = $this->leadRepository->find($message->getLeadId());

        $cpaResponse = $this->advertiserService->sendLeadToAdvertiser($lead);

        if ($cpaResponse->isError()) {
            $this->logger->error($cpaResponse->getData());
        } else {
            $lead->setCpaId($cpaResponse->getId());
            $this->entityManager->persist($lead);
            $this->entityManager->flush();
            $this->logger->info($cpaResponse->getData());
        }
    }
}
