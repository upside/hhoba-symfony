<?php

namespace App\MessageHandler;

use App\Agency\MyTargetApi;
use App\Agency\Vo\MyTarget\Error;
use App\Entity\Main\UserBalanceTransaction;
use App\Message\UserBalanceTransactionMessage;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use App\Repository\Main\UserBalanceTransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final class UserBalanceTransactionMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var UserBalanceTransactionRepository
     */
    private UserBalanceTransactionRepository $userBalanceTransactionRepository;

    /**
     * @var AgencyCabinetRepository
     */
    private AgencyCabinetRepository $agencyCabinetRepository;

    /**
     * @var MyTargetApi
     */
    private MyTargetApi $myTargetApi;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * UserBalanceTransactionMessageHandler constructor.
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     * @param UserBalanceTransactionRepository $userBalanceTransactionRepository
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param MyTargetApi $myTargetApi
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        UserBalanceTransactionRepository $userBalanceTransactionRepository,
        AgencyCabinetRepository $agencyCabinetRepository,
        MyTargetApi $myTargetApi
    )
    {
        $this->entityManager = $entityManager;
        $this->userBalanceTransactionRepository = $userBalanceTransactionRepository;
        $this->agencyCabinetRepository = $agencyCabinetRepository;
        $this->myTargetApi = $myTargetApi;
        $this->logger = $logger;
    }

    /**
     * @param UserBalanceTransactionMessage $message
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(UserBalanceTransactionMessage $message)
    {
        $this->entityManager->beginTransaction();

        try {

            $userBalanceTransaction = $this->userBalanceTransactionRepository->find($message->getTransactionId());

            if (is_null($userBalanceTransaction)) {
                throw new UnrecoverableMessageHandlingException('Транзакция ненайдена');
            }

            switch ($userBalanceTransaction->getOperation()) {
                case UserBalanceTransaction::OPERATION_DEPOSIT:
                    $this->operationDeposit($userBalanceTransaction);
                    break;
                case UserBalanceTransaction::OPERATION_TO_MT_CABINET:
                    $this->operationToMtCabinet($userBalanceTransaction);
                    break;
                case UserBalanceTransaction::OPERATION_FROM_MT_CABINET_TO_MT_CABINET:
                    $this->operationFromMtCabinetToMtCabinet($userBalanceTransaction);
                    break;
                default:
                    throw new UnrecoverableMessageHandlingException('Неизвесный тип транзакции');

            }

            $this->entityManager->flush();
            $this->entityManager->commit();

        } catch (Exception $exception) {

            $this->entityManager->rollback();

            throw $exception;

        }
    }

    /**
     * @param UserBalanceTransaction $userBalanceTransaction
     */
    private function operationDeposit(UserBalanceTransaction $userBalanceTransaction)
    {
        $userBalanceTo = $userBalanceTransaction->getUserBalanceTo();
        $userBalanceTo->setAmount(bcadd($userBalanceTo->getAmount(), $userBalanceTransaction->getAmount()));
        $userBalanceTransaction->setStatus(UserBalanceTransaction::STATUS_SUCCESS);

        $this->entityManager->persist($userBalanceTo);
        $this->entityManager->persist($userBalanceTransaction);
    }

    /**
     * @param UserBalanceTransaction $userBalanceTransaction
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws InvalidArgumentException
     */
    private function operationToMtCabinet(UserBalanceTransaction $userBalanceTransaction)
    {
        $myTargetTransaction = $this->myTargetApi->transaction(
            $userBalanceTransaction->getAgencyCabinetTo()->getApiId(),
            $userBalanceTransaction->getAmount()
        );

        if ($myTargetTransaction instanceof Error) {

            $userBalanceTransaction->setStatus(UserBalanceTransaction::STATUS_ERROR);
            $userBalanceTransaction->setParams([
                'error' => $myTargetTransaction
            ]);

            $userBalanceFrom = $userBalanceTransaction->getUserBalanceFrom();
            $userBalanceFrom->setAmount(bcadd($userBalanceFrom->getAmount(), $userBalanceTransaction->getAmount()));

            $this->entityManager->persist($userBalanceFrom);
            $this->entityManager->persist($userBalanceTransaction);

        } else {
            $userBalanceTransaction->getAgencyCabinetTo()->setBalance($myTargetTransaction->getClientBalance());

            $this->entityManager->persist($userBalanceTransaction);
            $this->entityManager->persist($userBalanceTransaction->getAgencyCabinetTo());

            $this->operationDeposit($userBalanceTransaction);
        }

    }

    private function operationFromMtCabinetToMtCabinet(UserBalanceTransaction $userBalanceTransaction)
    {

        $myTargetTransactionFrom = $this->myTargetApi->transaction(
            $userBalanceTransaction->getAgencyCabinetFrom()->getApiId(),
            $userBalanceTransaction->getAmount(),
            'from'
        );

        if ($myTargetTransactionFrom instanceof Error) {

            $userBalanceTransaction->setStatus(UserBalanceTransaction::STATUS_ERROR);
            $userBalanceTransaction->setParams([
                'error' => $myTargetTransactionFrom
            ]);

            $this->entityManager->persist($userBalanceTransaction);

            return;
        }

        $myTargetTransactionTo = $this->myTargetApi->transaction(
            $userBalanceTransaction->getAgencyCabinetTo()->getApiId(),
            $userBalanceTransaction->getAmount(),
            'to'
        );

        if ($myTargetTransactionTo instanceof Error) {

            $userBalanceTransaction->setStatus(UserBalanceTransaction::STATUS_ERROR);
            $userBalanceTransaction->setParams([
                'error' => $myTargetTransactionTo
            ]);

            $this->entityManager->persist($userBalanceTransaction);

            return;
        }

        $agencyCabinetFrom = $userBalanceTransaction->getAgencyCabinetFrom();
        $agencyCabinetTo = $userBalanceTransaction->getAgencyCabinetTo();

        $agencyCabinetFrom->setBalance($myTargetTransactionFrom->getClientBalance());
        $agencyCabinetTo->setBalance($myTargetTransactionTo->getClientBalance());
        $userBalanceTransaction->setStatus(UserBalanceTransaction::STATUS_SUCCESS);

        $this->entityManager->persist($agencyCabinetFrom);
        $this->entityManager->persist($agencyCabinetTo);
        $this->entityManager->persist($userBalanceTransaction);

    }
}
