<?php

namespace App\MessageHandler;

use App\Entity\Main\Geo;
use App\Entity\Main\Lead;
use App\Entity\Main\Tariff;
use App\Event\LeadPlacedEvent;
use App\Message\CpaLeadMessage;
use App\Message\LeadMessage;
use App\Repository\Main\ClickRepository;
use App\Repository\Main\GeoRepository;
use App\Repository\Main\IpAddressDictionaryRepository;
use App\Repository\Main\TariffRepository;
use App\Repository\Main\UserAgentDictionaryRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

final class LeadMessageHandler implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var ClickRepository
     */
    private ClickRepository $clickRepository;

    /**
     * @var GeoRepository
     */
    private GeoRepository $geoRepository;

    /**
     * @var TariffRepository
     */
    private TariffRepository $tariffRepository;
    /**
     * @var UserAgentDictionaryRepository
     */
    private UserAgentDictionaryRepository $userAgentDictionaryRepository;
    /**
     * @var IpAddressDictionaryRepository
     */
    private IpAddressDictionaryRepository $ipAddressDictionaryRepository;

    public function __construct(
        LoggerInterface $logger,
        EventDispatcherInterface $eventDispatcher,
        MessageBusInterface $messageBus,
        EntityManagerInterface $entityManager,
        ClickRepository $clickRepository,
        GeoRepository $geoRepository,
        TariffRepository $tariffRepository,
        UserAgentDictionaryRepository $userAgentDictionaryRepository,
        IpAddressDictionaryRepository $ipAddressDictionaryRepository
    )
    {
        $this->logger = $logger;
        $this->eventDispatcher = $eventDispatcher;
        $this->messageBus = $messageBus;
        $this->entityManager = $entityManager;
        $this->clickRepository = $clickRepository;
        $this->geoRepository = $geoRepository;
        $this->tariffRepository = $tariffRepository;
        $this->userAgentDictionaryRepository = $userAgentDictionaryRepository;
        $this->ipAddressDictionaryRepository = $ipAddressDictionaryRepository;
    }

    /**
     * Проверка на то что у клика ещё нет лида
     *
     * @param LeadMessage $message
     * @throws Throwable
     */
    public function __invoke(LeadMessage $message)
    {
        $this->entityManager->beginTransaction();

        try {
            $params = $message->getParams();

            $geoIso2Code = $params['geo'] ?? Geo::NOT_DEFINITELY_ISO_2;
            $name = $params['name'] ?? null;
            $comment = $params['comment'] ?? null;
            $useragentParam = $params['useragent'] ?? null;
            $ipAddressParam = $params['ip'] ?? null;

            $click = $this->clickRepository->findOneByOrFail(['uuid' => Uuid::fromString($message->getClickUuid())->toString()]);

            $geo = $this->geoRepository->findOneByOrFail(['iso_2' => $geoIso2Code]);

            $tariff = $this->tariffRepository->findOneBy([
                'offer' => $click->getStream()->getOffer(),
                'geo' => $geo,
                'user' => $click->getStream()->getTrafficSource()->getUser(),
                'type' => Tariff::TYPE_USER
            ]);

            if (is_null($tariff)) {
                $tariff = $this->tariffRepository->findOneByOrFail([
                    'offer' => $click->getStream()->getOffer(),
                    'geo' => $geo,
                    'type' => Tariff::TYPE_MAIN
                ]);
            }

            $lead = new Lead();

            if (is_null($useragentParam) === false) {
                $useragent = $this->userAgentDictionaryRepository->findOrCreate(['value' => $useragentParam]);
                $lead->setUserAgent($useragent);
            }

            if (is_null($ipAddressParam) === false) {
                $ipAddress = $this->ipAddressDictionaryRepository->findOrCreate(['value' => $ipAddressParam]);
                $lead->setIpAddress($ipAddress);
            }

            $lead->setUuid(Uuid::fromString($message->getLeadUuid()));
            $lead->setCreatedAt(new DateTime($message->getCreatedAt()));
            $lead->setClick($click);
            $lead->setGeo($geo);
            $lead->setStatus(Lead::STATUS_NEW);
            $lead->setPhone($message->getPhone());
            $lead->setName($name);
            $lead->setComment($comment);
            $lead->setCurrency($tariff->getCurrency());
            $lead->setPrice($tariff->getValue());

            $this->entityManager->persist($lead);
            $this->entityManager->flush();

            $this->entityManager->commit();

            $this->eventDispatcher->dispatch(new LeadPlacedEvent($lead));

            $this->messageBus->dispatch(new CpaLeadMessage($lead->getId()));

        } catch (Throwable $exception) {

            $this->entityManager->rollback();

            throw $exception;
        }
    }
}
