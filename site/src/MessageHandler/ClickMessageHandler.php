<?php

namespace App\MessageHandler;

use App\Entity\Main\Click;
use App\Entity\Main\Geo;
use App\Event\ClickPlacedEvent;
use App\Message\ClickMessage;
use App\Repository\Main\GeoRepository;
use App\Repository\Main\IpAddressDictionaryRepository;
use App\Repository\Main\StreamRepository;
use App\Repository\Main\UserAgentDictionaryRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Throwable;

final class ClickMessageHandler implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var StreamRepository
     */
    private StreamRepository $streamRepository;

    /**
     * @var GeoRepository
     */
    private GeoRepository $geoRepository;

    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @var IpAddressDictionaryRepository
     */
    private IpAddressDictionaryRepository $ipAddressDictionaryRepository;

    /**
     * @var UserAgentDictionaryRepository
     */
    private UserAgentDictionaryRepository $userAgentDictionaryRepository;


    public function __construct(
        LoggerInterface $logger,
        EventDispatcherInterface $eventDispatcher,
        EntityManagerInterface $entityManager,
        StreamRepository $streamRepository,
        GeoRepository $geoRepository,
        IpAddressDictionaryRepository $ipAddressDictionaryRepository,
        UserAgentDictionaryRepository $userAgentDictionaryRepository
    )
    {
        $this->logger = $logger;
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
        $this->streamRepository = $streamRepository;
        $this->geoRepository = $geoRepository;
        $this->ipAddressDictionaryRepository = $ipAddressDictionaryRepository;
        $this->userAgentDictionaryRepository = $userAgentDictionaryRepository;
    }

    /**
     * @param ClickMessage $message
     * @throws Throwable
     */
    public function __invoke(ClickMessage $message)
    {
        $this->entityManager->beginTransaction();

        try {

            $stream = $this->streamRepository->findOneByOrFail(['uuid' => Uuid::fromString($message->getStreamUuid())->toString()]);

            $params = $message->getParams();

            $geoIso2Code = $params['geo'] ?? Geo::NOT_DEFINITELY_ISO_2;
            $ipAddressString = $params['ip'] ?? null;
            $userAgentString = $params['useragent'] ?? null;

            $click = new Click();

            if ($ipAddressString !== null) {
                $ipAddressDictionary = $this->ipAddressDictionaryRepository->findOrCreate(['value' => $ipAddressString]);
                $click->setIpAddress($ipAddressDictionary);
            }

            if ($userAgentString !== null) {
                $userAgent = $this->userAgentDictionaryRepository->findOrCreate(['value' => $userAgentString]);
                $click->setUserAgent($userAgent);
            }

            $geo = $this->geoRepository->findOneByOrFail(['iso_2' => $geoIso2Code]);

            $click->setStream($stream);
            $click->setGeo($geo);
            $click->setUuid(Uuid::fromString($message->getClickUuid()));
            $click->setCreatedAt(new DateTime($message->getCreatedAt()));

            $this->entityManager->persist($click);
            $this->entityManager->flush();

            $this->entityManager->commit();

            $this->eventDispatcher->dispatch(new ClickPlacedEvent($click));

        } catch (Throwable $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }

    }
}
