<?php

namespace App\Notifier;

use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Notification\ChatNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;

class LowBalanceNotification extends Notification implements ChatNotificationInterface
{
    private string $subject;
    private array $channels;
    /**
     * @var TelegramMessageOptions|null
     */
    private ?TelegramMessageOptions $telegramMessageOptions;

    public function __construct(string $subject = '', array $channels = [], ?TelegramMessageOptions $telegramMessageOptions = null)
    {
        parent::__construct($subject, $channels);
        $this->subject = $subject;
        $this->channels = $channels;
        $this->telegramMessageOptions = $telegramMessageOptions;
    }

    public function asChatMessage(Recipient $recipient, string $transport = null): ?ChatMessage
    {
        return new ChatMessage($this->subject, $this->telegramMessageOptions);
    }
}
