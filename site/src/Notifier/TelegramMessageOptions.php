<?php

namespace App\Notifier;

use Symfony\Component\Notifier\Message\MessageOptionsInterface;

class TelegramMessageOptions implements MessageOptionsInterface
{

    private string $chatId;

    public function __construct(string $chatId)
    {
        $this->chatId = $chatId;
    }


    public function toArray(): array
    {
        return ['chat_id' => $this->chatId];
    }

    public function getRecipientId(): ?string
    {
        return '';
    }
}
