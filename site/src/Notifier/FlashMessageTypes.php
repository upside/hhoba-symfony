<?php

namespace App\Notifier;

interface FlashMessageTypes
{
    public const PRIMARY = 'primary';
    public const DANGER = 'danger';
    public const SUCCESS = 'success';
    public const WARNING = 'warning';
    public const INFO = 'info';
}
