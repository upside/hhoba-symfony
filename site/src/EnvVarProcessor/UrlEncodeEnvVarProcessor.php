<?php

namespace App\EnvVarProcessor;

use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

class UrlEncodeEnvVarProcessor implements EnvVarProcessorInterface
{

    public function getEnv(string $prefix, string $name, \Closure $getEnv)
    {
        $env = $getEnv($name);

        return urlencode($env);
    }

    public static function getProvidedTypes()
    {
        return [
            'urlencode' => 'string',
        ];
    }
}
