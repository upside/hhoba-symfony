<?php

namespace App\Helpers;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;

class DateTimeHelper
{
    public const DATE_TIME_YMD = 'Y-m-d H:i:s';
    public const DATE_YMD = 'Y-m-d';

    /**
     * Начало текущего часа
     */
    public const DATE_YMD_BEGIN_HOUR = 'Y-m-d H:00:00';

    /**
     * Конец текущего часа
     */
    public const DATE_YMD_END_HOUR = 'Y-m-d H:59:59';

    /**
     * Начало текущего дня
     */
    public const DATE_YMD_BEGIN_DAY = 'Y-m-d 00:00:00';

    /**
     * Конец текущего дня
     */
    public const DATE_YMD_END_DAY = 'Y-m-d 23:59:59';

    /**
     * Начало текущего месяца
     */
    public const DATE_YMD_BEGIN_MONTH = 'Y-m-01 00:00:00';

    /**
     * Конец текущего месяца
     */
    public const DATE_YMD_END_MONTH = 'Y-m-t 23:59:59';

    /**
     * Первый день месяца
     */
    public const FIRST_DAY = 'first day';
    public const TODAY = 'today';
    public const NOW = 'now';


    /**
     * Возвращает диапозон дат за текущий час
     *
     * @param DateTimeZone|null $timezone
     * @return DateRangeVo
     * @throws Exception
     */
    public function getCurrentHour(?DateTimeZone $timezone = null): DateRangeVo
    {
        $dateStart = new DateTime(date(self::DATE_YMD_BEGIN_HOUR), $timezone);
        $dateEnd = new DateTime(date(self::DATE_YMD_END_HOUR), $timezone);

        return new DateRangeVo(
            $dateStart,
            $dateEnd
        );
    }

    /**
     * Возвращает дат за текущий день
     *
     * @param DateTimeZone|null $timezone
     * @return DateRangeVo
     * @throws Exception
     */
    public function getCurrentDay(?DateTimeZone $timezone = null): DateRangeVo
    {
        $dateStart = new DateTime(date(self::DATE_YMD_BEGIN_DAY), $timezone);
        $dateEnd = new DateTime(date(self::DATE_YMD_END_DAY), $timezone);

        return new DateRangeVo(
            $dateStart,
            $dateEnd
        );
    }

    /**
     * Возвращает дат за текущую неделю
     *
     * @param DateTimeZone|null $timezone
     * @return DateRangeVo
     * @throws Exception
     */
    public function getCurrentWeek(?DateTimeZone $timezone = null): DateRangeVo
    {
        $dateStart = new DateTime(date(self::DATE_YMD_BEGIN_DAY), $timezone);
        $dateEnd = new DateTime(date(self::DATE_YMD_END_DAY), $timezone);

        $currentWeekDay = $dateStart->format('N');

        $dateStart->sub(new DateInterval('P' . ($currentWeekDay - 1) . 'D'));
        $dateEnd->add(new DateInterval('P' . (7 - $currentWeekDay) . 'D'));

        return new DateRangeVo(
            $dateStart,
            $dateEnd
        );
    }

    /**
     * Возвращает дат за текущий месяц
     *
     * @param DateTimeZone|null $timezone
     * @return DateRangeVo
     * @throws Exception
     */
    public function getCurrentMonth(?DateTimeZone $timezone = null): DateRangeVo
    {
        $dateStart = new DateTime(date(self::DATE_YMD_BEGIN_MONTH), $timezone);
        $dateEnd = new DateTime(date(self::DATE_YMD_END_MONTH), $timezone);

        return new DateRangeVo(
            $dateStart,
            $dateEnd
        );
    }

    /**
     * Возвращает дат за текущий квартал
     *
     * @param DateTimeZone|null $timezone
     * @return DateRangeVo
     * @throws Exception
     */
    public function getCurrentMonth3(?DateTimeZone $timezone = null): DateRangeVo
    {
        $dateStart = new DateTime(date(self::DATE_YMD_BEGIN_MONTH), $timezone);
        $dateEnd = new DateTime(date(self::DATE_YMD_END_MONTH), $timezone);

        $currentMonth = $dateStart->format('n');

        switch ($currentMonth) {
            case 1:
            case 4:
            case 7:
            case 10:
                $dateStart->sub(new DateInterval('P1M'));
                $dateEnd->add(new DateInterval('P1M'));
                break;
            case 2:
            case 5:
            case 8:
            case 11:
                $dateStart->sub(new DateInterval('P2M'));
                break;
            case 3:
            case 6:
            case 9:
            case 12:
                $dateEnd->add(new DateInterval('P2M'));
                break;
        }

        $day = $dateEnd->format('j');
        $totalDays = $dateEnd->format('t');
        if ($totalDays !== $day) {
            $dateEnd->add(new DateInterval('P' . ($totalDays - $day) . 'D'));
        }

        return new DateRangeVo(
            $dateStart,
            $dateEnd
        );
    }

}
