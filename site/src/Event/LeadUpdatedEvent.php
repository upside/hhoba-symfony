<?php

namespace App\Event;

use App\Entity\Main\Lead;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Событие должно запускаться после сохранения изменений Lead
 */
class LeadUpdatedEvent extends Event
{
    /**
     * @var Lead
     */
    private Lead $lead;

    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * @return Lead
     */
    public function getLead(): Lead
    {
        return $this->lead;
    }
}
