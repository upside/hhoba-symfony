<?php

namespace App\Event;

use App\Entity\Main\Lead;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Событие должно запускаться перед сохранения изменений Lead
 */
class LeadUpdateEvent extends Event
{
    /**
     * @var Lead
     */
    private Lead $lead;

    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * @return Lead
     */
    public function getLead(): Lead
    {
        return $this->lead;
    }
}
