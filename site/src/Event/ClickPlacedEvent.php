<?php

namespace App\Event;

use App\Entity\Main\Click;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Событие должно запускаться после оздания и сохранения нового Click
 */
class ClickPlacedEvent extends Event
{
    /**
     * @var Click
     */
    protected Click $click;

    public function __construct(Click $click)
    {
        $this->click = $click;
    }

    /**
     * @return Click
     */
    public function getClick(): Click
    {
        return $this->click;
    }
}
