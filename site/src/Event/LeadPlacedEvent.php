<?php

namespace App\Event;

use App\Entity\Main\Lead;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Событие должно запускаться после создания и сохранения нового Lead
 */
class LeadPlacedEvent extends Event
{
    /**
     * @var Lead
     */
    private Lead $lead;

    public function __construct(Lead $lead)
    {

        $this->lead = $lead;
    }

    /**
     * @return Lead
     */
    public function getLead(): Lead
    {
        return $this->lead;
    }
}
