<?php

namespace App\Agency\Vo\MyTarget;

use Symfony\Component\Serializer\Annotation\SerializedName;

class AdditionalClientInfo
{
    /**
     * @var string Адрес
     */
    private string $address;

    /**
     * @var string Заметки агентства о клиенте
     *
     * @SerializedName("client_info")
     */
    private string $clientInfo;

    /**
     * @var string Имя клиента агентства
     *
     * @SerializedName("client_name")
     */
    private string $clientName;

    /**
     * @var string Имя и фамилия
     */
    private string $name;

    /**
     * @var string Номер телефона
     */
    private string $phone;

    /**
     * @param string $address
     * @param string $clientInfo
     * @param string $clientName
     * @param string $name
     * @param string $phone
     */
    public function __construct(string $address, string $clientInfo, string $clientName, string $name, string $phone)
    {
        $this->address = $address;
        $this->clientInfo = $clientInfo;
        $this->clientName = $clientName;
        $this->name = $name;
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getClientInfo(): string
    {
        return $this->clientInfo;
    }

    /**
     * @return string
     */
    public function getClientName(): string
    {
        return $this->clientName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

}
