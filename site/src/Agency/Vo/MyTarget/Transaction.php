<?php

namespace App\Agency\Vo\MyTarget;

use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Transaction
{
    /**
     * @var string Баланс агентства
     *
     * @SerializedName("agency_balance")
     */
    private string $agencyBalance;

    /**
     * @var string Сумма
     */
    private string $amount;

    /**
     * @var string Баланс клиента
     *
     * @SerializedName("client_balance")
     */
    private string $clientBalance;

    /**
     * @var string Имя клиента
     *
     * @SerializedName("client_username")
     */
    private string $clientUsername;

    /**
     * @var DateTimeInterface Дата и время создания
     */
    private DateTimeInterface $created_at;

    public function __construct(string $agencyBalance, string $amount, string $clientBalance, string $clientUsername, DateTimeInterface $created_at)
    {
        $this->agencyBalance = $agencyBalance;
        $this->amount = $amount;
        $this->clientBalance = $clientBalance;
        $this->clientUsername = $clientUsername;
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getAgencyBalance(): string
    {
        return $this->agencyBalance;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getClientBalance(): string
    {
        return $this->clientBalance;
    }

    /**
     * @return string
     */
    public function getClientUsername(): string
    {
        return $this->clientUsername;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->created_at;
    }
}
