<?php

namespace App\Agency\Vo\MyTarget;

use Symfony\Component\Serializer\Annotation\SerializedName;

class ApiToken
{
    /**
     * @var string
     *
     * @SerializedName("access_token")
     */
    private string $accessToken;

    /**
     * @var string
     *
     * @SerializedName("token_type")
     */
    private string $tokenType;

    /**
     * @var int
     *
     * @SerializedName("expires_in")
     */
    private int $expiresIn;

    /**
     * @var array
     */
    private array $scope;

    /**
     * @var string
     *
     * @SerializedName("refresh_token")
     */
    private string $refreshToken;

    /**
     * @var int
     *
     * @ignore()
     * @SerializedName("tokens_left")
     */
    private int $tokensLeft;

    /**
     * @var int
     *
     * @ignore()
     * @SerializedName("created_at")
     */
    private int $createdAt;

    public function __construct(
        string $accessToken,
        string $tokenType,
        int $expiresIn,
        array $scope,
        string $refreshToken,
        int $tokensLeft,
        int $createdAt
    )
    {
        $this->accessToken = $accessToken;
        $this->tokenType = $tokenType;
        $this->expiresIn = $expiresIn;
        $this->scope = $scope;
        $this->refreshToken = $refreshToken;
        $this->tokensLeft = $tokensLeft;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    /**
     * @return array
     */
    public function getScope(): array
    {
        return $this->scope;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @return int
     */
    public function getTokensLeft(): int
    {
        return $this->tokensLeft;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
