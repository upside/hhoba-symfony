<?php

namespace App\Agency\Vo\MyTarget;

use Symfony\Component\Serializer\Annotation\SerializedName;

class UserAccount
{
    /**
     * @var string Живой баланс
     *
     * @SerializedName("a_balance")
     */
    private string $aBalance;

    /**
     * @var string Баланс
     */
    private string $balance;

    /**
     * @var string Средства, замороженные на оплату CPI
     *
     * @SerializedName("currency_balance_hold")
     */
    private string $currencyBalanceHold;

    /**
     * @var int Идентификатор аккаунта
     */
    private int $id;

    /**
     * @var string Тип
     */
    private string $type;

    /**
     * @param string $aBalance
     * @param string $balance
     * @param string $currencyBalanceHold
     * @param int $id
     * @param string $type
     */
    public function __construct(string $aBalance, string $balance, string $currencyBalanceHold, int $id, string $type)
    {
        $this->aBalance = $aBalance;
        $this->balance = $balance;
        $this->currencyBalanceHold = $currencyBalanceHold;
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getABalance(): string
    {
        return $this->aBalance;
    }

    /**
     * @return string
     */
    public function getBalance(): string
    {
        return $this->balance;
    }

    /**
     * @return string
     */
    public function getCurrencyBalanceHold(): string
    {
        return $this->currencyBalanceHold;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

}
