<?php

namespace App\Agency\Vo\MyTarget;

class AgencyClientCollection
{
    /**
     * @var int
     */
    private int $count;

    /**
     * @var AgencyClient[]
     */
    private array $items;

    /**
     * @var int
     */
    private int $limit;

    /**
     * @var int
     */
    private int $offset;

    /**
     * @param int $count
     * @param AgencyClient[] $items
     * @param int $limit
     * @param int $offset
     */
    public function __construct(int $count, array $items, int $limit, int $offset)
    {
        $this->count = $count;
        $this->items = $items;
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return AgencyClient[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
}
