<?php

namespace App\Agency\Vo\MyTarget;

class Error
{
    /**
     * @var ErrorData
     */
    private ErrorData $error;

    /**
     * Error constructor.
     * @param ErrorData $error
     */
    public function __construct(ErrorData $error)
    {
        $this->error = $error;
    }

    /**
     * @return ErrorData
     */
    public function getError(): ErrorData
    {
        return $this->error;
    }
}
