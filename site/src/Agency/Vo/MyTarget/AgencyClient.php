<?php

namespace App\Agency\Vo\MyTarget;

use Exception;
use Symfony\Component\Serializer\Annotation\SerializedName;

class AgencyClient
{
    /**
     *  Активный
     */
    public const STATUS_ACTIVE = 'active';

    /**
     *  Удаленный
     */
    public const STATUS_DELETED = 'deleted';

    /**
     * Заблокированный
     */
    public const STATUS_BLOCKED = 'blocked';

    public const STATUSES = [
        self::STATUS_ACTIVE => 'Активный',
        self::STATUS_DELETED => 'Удаленный',
        self::STATUS_BLOCKED => 'Заблокированный',
    ];

    /**
     * Полный доступ
     */
    public const ACCESS_TYPE_FULL_ACCESS = 'full_access';

    /**
     * Только чтение
     */
    public const ACCESS_TYPE_READONLY = 'readonly';

    /**
     * Без доступа к счету
     */
    public const ACCESS_TYPE_FIN_READONLY = 'fin_readonly';

    /**
     * Только доступ к счету
     */
    public const ACCESS_TYPE_ADS_READONLY = 'ads_readonly';


    public const ACCESS_TYPES = [
        self::ACCESS_TYPE_FULL_ACCESS => 'Полный доступ',
        self::ACCESS_TYPE_READONLY => 'Только чтение',
        self::ACCESS_TYPE_FIN_READONLY => 'Без доступа к счету',
        self::ACCESS_TYPE_ADS_READONLY => 'Только доступ к счету',
    ];

    /**
     * @var string Тип доступа агентства к учетной записи клиента.
     *
     *
     * @SerializedName("access_type")
     */
    private string $accessType;

    /**
     * @var string Статус отношения между клиентом и агенством.
     */
    private string $status;

    /**
     * @var UserClient Клиент
     */
    private UserClient $user;

    /**
     * @param string $accessType Тип доступа агентства к учетной записи клиента.
     * @param string $status Статус отношения между клиентом и агенством.
     * @param UserClient $user Клиент
     * @throws Exception
     */
    public function __construct(string $accessType, string $status, UserClient $user)
    {
        if (array_key_exists($accessType, self::ACCESS_TYPES) === false) {
            throw new Exception('Неизвестный тип доступа');
        }

        if (array_key_exists($status, self::STATUSES) === false) {
            throw new Exception('Неизвестный статус');
        }

        $this->accessType = $accessType;
        $this->status = $status;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getAccessType(): string
    {
        return $this->accessType;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return UserClient
     */
    public function getUser(): UserClient
    {
        return $this->user;
    }
}
