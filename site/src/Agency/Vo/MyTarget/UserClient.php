<?php

namespace App\Agency\Vo\MyTarget;

use Exception;
use Symfony\Component\Serializer\Annotation\SerializedName;

class UserClient
{
    /**
     * Активный
     */
    public const STATUS_ACTIVE = 'active';

    /**
     * Удаленный
     */
    public const STATUS_DELETED = 'deleted';

    /**
     * Заблокированный
     */
    public const STATUS_BLOCKED = 'blocked';

    public const STATUSES = [
        self::STATUS_ACTIVE => 'Активный',
        self::STATUS_DELETED => 'Удаленный',
        self::STATUS_BLOCKED => 'Заблокированный',
    ];

    /**
     * @var UserAccount Информация о финансовом счете клиента.
     */
    private UserAccount $account;

    /**
     * @var AdditionalClientInfo Объект, хранящий дополнительную информацию о клиенте.
     *
     * @SerializedName("additional_info")
     */
    private ?AdditionalClientInfo $additionalInfo;

    /**
     * @var int Идентификатор клиента.
     */
    private int $id;

    /**
     * @var string Статус клиента.
     */
    private string $status;

    /**
     * @var string Имя клиента внутри сервиса.
     */
    private string $username;

    /**
     * @var string[] Список дополнительных email адресов пользователя.
     *
     * @SerializedName("additional_emails")
     */
    private array $additionalEmails;

    /**
     * @param UserAccount $account Информация о финансовом счете клиента.
     * @param AdditionalClientInfo|null $additionalInfo Объект, хранящий дополнительную информацию о клиенте.
     * @param int $id Идентификатор клиента.
     * @param string $status Статус клиента.
     * @param string $username Имя клиента внутри сервиса.
     * @param string[] $additionalEmails Список дополнительных email адресов пользователя.
     * @throws Exception
     */
    public function __construct(UserAccount $account, ?AdditionalClientInfo $additionalInfo, int $id, string $status, string $username, array $additionalEmails = [])
    {
        if (array_key_exists($status, self::STATUSES) === false) {
            throw new Exception('Неизвестный статус');
        }

        $this->account = $account;
        $this->additionalInfo = $additionalInfo;
        $this->id = $id;
        $this->status = $status;
        $this->username = $username;
        $this->additionalEmails = $additionalEmails;
    }

    /**
     * @return UserAccount
     */
    public function getAccount(): UserAccount
    {
        return $this->account;
    }

    /**
     * @return AdditionalClientInfo|null
     */
    public function getAdditionalInfo(): ?AdditionalClientInfo
    {
        return $this->additionalInfo;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return array
     */
    public function getAdditionalEmails(): array
    {
        return $this->additionalEmails;
    }
}
