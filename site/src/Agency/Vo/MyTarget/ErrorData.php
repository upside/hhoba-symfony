<?php

namespace App\Agency\Vo\MyTarget;

class ErrorData
{
    /**
     * @var string
     */
    private string $message;

    /**
     * @var string
     */
    private string $code;

    /**
     * ErrorData constructor.
     * @param string $message
     * @param string $code
     */
    public function __construct(string $message, string $code)
    {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

}
