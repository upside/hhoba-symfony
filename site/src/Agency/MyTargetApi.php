<?php

namespace App\Agency;

use App\Agency\Vo\MyTarget\AgencyClient;
use App\Agency\Vo\MyTarget\AgencyClientCollection;
use App\Agency\Vo\MyTarget\ApiToken;
use App\Agency\Vo\MyTarget\Error;
use App\Agency\Vo\MyTarget\Transaction;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MyTargetApi
{
    public const API_URL = 'https://target.my.com/api/v2/';
    public const TOKEN_CACHE_KEY = 'myTargetToken';

    private string $clientId;
    private string $clientSecret;

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @var AdapterInterface
     */
    private AdapterInterface $cache;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @param string $clientId
     * @param string $clientSecret
     * @param HttpClientInterface $httpClient
     * @param AdapterInterface $cache
     * @param LoggerInterface $logger
     * @param SerializerInterface $serializer
     */
    public function __construct(
        string $clientId,
        string $clientSecret,
        HttpClientInterface $httpClient,
        AdapterInterface $cache,
        LoggerInterface $logger,
        SerializerInterface $serializer
    )
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->httpClient = $httpClient;
        $this->cache = $cache;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * @return ApiToken
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getToken(): ApiToken
    {
        $cachedToken = $this->cache->getItem(self::TOKEN_CACHE_KEY);

        $token = $cachedToken->get();

        if (is_null($token)) {

            $data = $this->makeRequest('POST', self::API_URL . 'oauth2/token.json', [
                'body' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->clientId,
                    'client_secret' => $this->clientSecret,
                ],
            ]);

            $tokenData = json_decode($data, true);

            $token = new ApiToken(
                $tokenData['access_token'],
                $tokenData['token_type'],
                $tokenData['expires_in'],
                $tokenData['scope'],
                $tokenData['refresh_token'],
                $tokenData['tokens_left'],
                time()
            );

            $cachedToken->set($token);
            $this->cache->save($cachedToken);
        }

        if (($token->getCreatedAt() + $token->getExpiresIn()) <= time()) {
            return $this->refreshToken($token);
        }

        return $token;
    }

    /**
     * @param ApiToken $apiToken
     * @return ApiToken
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function refreshToken(ApiToken $apiToken): ApiToken
    {
        $data = $this->makeRequest('POST', self::API_URL . 'oauth2/token.json', [
            'body' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $apiToken->getRefreshToken(),
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ],
        ]);

        $tokenData = json_decode($data, true);

        $token = new ApiToken(
            $tokenData['access_token'],
            $tokenData['token_type'],
            $tokenData['expires_in'],
            $tokenData['scope'],
            $tokenData['refresh_token'],
            $tokenData['tokens_left'] ?? 4,
            time()
        );

        $cachedToken = $this->cache->getItem(self::TOKEN_CACHE_KEY);
        $cachedToken->set($token);
        $this->cache->save($cachedToken);

        return $token;
    }

    /**
     * @return bool
     * @throws TransportExceptionInterface
     * @throws InvalidArgumentException
     */
    private function clearTokens(): bool
    {
        $cachedToken = $this->cache->getItem(self::TOKEN_CACHE_KEY);
        $cachedToken->set(null);
        $this->cache->save($cachedToken);

        $response = $this->httpClient->request('POST', self::API_URL . 'oauth2/token/delete.json', [
            'body' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ],
        ]);

        return true;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @return string
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function makeRequest(string $method, string $url, array $params = []): string
    {
        $response = $this->httpClient->request($method, $url, $params);

        if ($response->getStatusCode() === 401) {
            $this->clearTokens();
            $response = $this->httpClient->request($method, $url, $params);
        }

        return $response->getContent(false);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return AgencyClientCollection
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getClients(int $limit = 20, int $offset = 0): AgencyClientCollection
    {
        $data = $this->makeRequest('GET', self::API_URL . 'agency/clients.json', [
            'query' => [
                'limit' => $limit,
                'offset' => $offset
            ],
            'headers' => $this->getHeaders(),
        ]);

        /** @var AgencyClientCollection $agencyClientCollection */
        $agencyClientCollection = $this->serializer->deserialize($data, AgencyClientCollection::class, 'json');

        return $agencyClientCollection;
    }

    /**
     * @param string $id
     * @return AgencyClient
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getClientById(string $id): AgencyClient
    {
        $data = $this->makeRequest('GET', self::API_URL . 'agency/clients.json', [
            'query' => [
                'limit' => 1,
                'offset' => 0,
                '_user__id' => $id
            ],
            'headers' => $this->getHeaders(),
        ]);

        /** @var AgencyClientCollection $agencyClientCollection */
        $agencyClientCollection = $this->serializer->deserialize($data, AgencyClientCollection::class, 'json');

        return $agencyClientCollection->getItems()[0];
    }

    /**
     * @param string $username
     * @return AgencyClient|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getClientByUsername(string $username): ?AgencyClient
    {
        $data = $this->makeRequest('GET', self::API_URL . 'agency/clients.json', [
            'query' => [
                'limit' => 1,
                'offset' => 0,
                '_user__username' => $username
            ],
            'headers' => $this->getHeaders(),
        ]);

        /** @var AgencyClientCollection $agencyClientCollection */
        $agencyClientCollection = $this->serializer->deserialize($data, AgencyClientCollection::class, 'json');

        return $agencyClientCollection->getItems()[0] ?? null;
    }

    /**
     *
     * @param string ...$ids
     * @return AgencyClient[]
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getClientsByIds(string ...$ids): array
    {
        $chunkedIds = array_chunk($ids, 50);

        $agencyClients = [];

        foreach ($chunkedIds as $key => $userIds) {

            $data = $this->makeRequest('GET', self::API_URL . 'agency/clients.json', [
                'query' => [
                    'limit' => 50,
                    'offset' => $key * 50,
                    '_user__id__in' => implode(',', $ids)
                ],
                'headers' => $this->getHeaders(),
            ]);

            /** @var AgencyClientCollection $agencyClientCollection */
            $agencyClientCollection = $this->serializer->deserialize($data, AgencyClientCollection::class, 'json');

            $agencyClients = array_merge($agencyClients, $agencyClientCollection->getItems());
        }

        return $agencyClients;
    }

    /**
     * @return array|string[]
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . $this->getToken()->getAccessToken()
        ];
    }

    /**
     * @param string $agencyCabinetApiId
     * @param string $amount
     * @param string $mode
     * @return Transaction|Error
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function transaction(string $agencyCabinetApiId, string $amount, string $mode = 'to'): object
    {

        $data = $this->makeRequest('POST', self::API_URL . 'billing/transactions/' . $mode . '/' . $agencyCabinetApiId . '.json', [
            'json' => ['amount' => $amount],
            'headers' => $this->getHeaders()
        ]);

        try {
            /** @var Transaction $transaction */
            $transaction = $this->serializer->deserialize($data, Transaction::class, 'json');

            return $transaction;

        } catch (\Exception $exception) {

            /** @var Error $error */
            $error = $this->serializer->deserialize($data, Error::class, 'json');

            $this->logger->error('Ошибка создания транзакции Mytarget', ['error' => $error]);

            return $error;
        }
    }
}
