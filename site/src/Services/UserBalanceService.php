<?php

namespace App\Services;

use App\Entity\Main\User;
use App\Entity\Main\UserBalance;
use Symfony\Component\Security\Core\Security;

class UserBalanceService
{
    /**
     * @var Security
     */
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getMyTargetUsd()
    {
        /** @var User $user */
        $user = $this->security->getUser();

        /** @var UserBalance[] $userBalances */
        $userBalances = $user->getUserBalances();

        foreach ($userBalances as $userBalance) {

            if ($userBalance->getType() === UserBalance::TYPE_AGENCY_USD) {
                return $userBalance->getAmount();
            }
        }

        return '0';
    }

}
