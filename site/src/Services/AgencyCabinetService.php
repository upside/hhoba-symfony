<?php

namespace App\Services;

use App\Agency\MyTargetApi;
use App\Entity\Main\Agency\AgencyCabinet;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AgencyCabinetService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var MyTargetApi
     */
    private MyTargetApi $myTargetApi;

    /**
     * @var AgencyCabinetRepository
     */
    private AgencyCabinetRepository $agencyCabinetRepository;


    public function __construct(
        EntityManagerInterface $entityManager,
        MyTargetApi $myTargetApi,
        AgencyCabinetRepository $agencyCabinetRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->myTargetApi = $myTargetApi;
        $this->agencyCabinetRepository = $agencyCabinetRepository;
    }

    /**
     * @param AgencyCabinet $agencyCabinet
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function syncCabinet(AgencyCabinet $agencyCabinet): bool
    {
        return $this->syncCabinets($agencyCabinet);
    }

    /**
     * @param AgencyCabinet ...$agencyCabinets
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws InvalidArgumentException
     */
    public function syncCabinets(AgencyCabinet ...$agencyCabinets): bool
    {
        $this->entityManager->beginTransaction();

        try {

            $cabinetApiIds = [];
            foreach ($agencyCabinets as $agencyCabinet) {
                $cabinetApiIds[] = $agencyCabinet->getApiId();
            }

            $apiCabinets = $this->myTargetApi->getClientsByIds(...$cabinetApiIds);
            $apiCabinetsSorted = [];
            foreach ($apiCabinets as $apiCabinet) {
                $apiCabinetsSorted[$apiCabinet->getUser()->getId()] = $apiCabinet;
            }

            foreach ($agencyCabinets as $agencyCabinet) {
                $agencyCabinet->setBalance($apiCabinetsSorted[$agencyCabinet->getApiId()]->getUser()->getAccount()->getBalance());
                $this->entityManager->persist($agencyCabinet);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();

        } catch (Exception $exception) {

            $this->entityManager->rollback();
            throw $exception;

        }

        return true;
    }

}
