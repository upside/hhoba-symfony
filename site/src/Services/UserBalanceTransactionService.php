<?php

namespace App\Services;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\UserBalance;
use App\Entity\Main\UserBalanceTransaction;
use App\Message\UserBalanceTransactionMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class UserBalanceTransactionService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    public function __construct(EntityManagerInterface $entityManager, MessageBusInterface $messageBus)
    {
        $this->entityManager = $entityManager;
        $this->messageBus = $messageBus;
    }

    public function newTransaction(
        UserBalance $userBalanceFrom,
        UserBalance $userBalanceTo,
        int $operation,
        string $amount,
        array $params = [],
        ?AgencyCabinet $agencyCabinetFrom = null,
        ?AgencyCabinet $agencyCabinetTo = null
    )
    {
        $this->entityManager->beginTransaction();

        try {

            if ($operation !== UserBalanceTransaction::OPERATION_FROM_MT_CABINET_TO_MT_CABINET) {
                $userBalanceFrom->setAmount(bcsub($userBalanceFrom->getAmount(), $amount, 2));
                $this->entityManager->persist($userBalanceFrom);
            }

            $userBalanceTransaction = new UserBalanceTransaction();
            $userBalanceTransaction->setUserBalanceFrom($userBalanceFrom);
            $userBalanceTransaction->setUserBalanceTo($userBalanceTo);
            $userBalanceTransaction->setOperation($operation);
            $userBalanceTransaction->setStatus(UserBalanceTransaction::STATUS_NEW);
            $userBalanceTransaction->setAmount($amount);
            $userBalanceTransaction->setName('Test');
            $userBalanceTransaction->setDescription('Test Description');
            $userBalanceTransaction->setParams($params);
            $userBalanceTransaction->setAgencyCabinetFrom($agencyCabinetFrom);
            $userBalanceTransaction->setAgencyCabinetTo($agencyCabinetTo);

            $this->entityManager->persist($userBalanceTransaction);
            $this->entityManager->flush();

            $this->entityManager->commit();

            //После сохранения транзакции создаём задачу на обработку транзакции

            $this->messageBus->dispatch(new UserBalanceTransactionMessage($userBalanceTransaction->getId()));

        } catch (\Exception $exception) {

            $this->entityManager->rollback();
        }

    }

}
