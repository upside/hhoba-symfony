<?php

namespace App\Command;

use App\Entity\Statistic\Statistic;
use App\EventSubscriber\StatisticSyncSubscriber;
use App\Repository\Statistic\StatisticRepository;
use Doctrine\DBAL\DBALException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StatisticSyncCommand extends Command
{
    protected static $defaultName = 'statistic:sync';

    /**
     * @var AdapterInterface
     */
    private AdapterInterface $adapter;

    /**
     * @var StatisticRepository
     */
    private StatisticRepository $statisticRepository;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param AdapterInterface $adapter
     * @param StatisticRepository $statisticRepository
     * @param LoggerInterface $logger
     */
    public function __construct(AdapterInterface $adapter, StatisticRepository $statisticRepository, LoggerInterface $logger)
    {
        parent::__construct(null);

        $this->adapter = $adapter;
        $this->statisticRepository = $statisticRepository;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Отправляет данные из кеша в clickhouse');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws InvalidArgumentException
     * @throws DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $item = $this->adapter->getItem(StatisticSyncSubscriber::CLICKHOUSE_CACHE_KEY);

        /** @var Statistic[] $statistics */
        $statistics = $item->get();

        if(!empty($statistics)){
            $this->statisticRepository->persist(...$statistics);
        }

        $item->set([]);
        $this->adapter->save($item);

        return 0;
    }
}
