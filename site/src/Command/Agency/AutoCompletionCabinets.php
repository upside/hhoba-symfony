<?php

namespace App\Command\Agency;

use App\Agency\MyTargetApi;
use App\Agency\Vo\MyTarget\UserClient;
use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\User;
use App\Entity\Main\UserBalanceTransaction;
use App\Notifier\LowBalanceNotification;
use App\Notifier\TelegramMessageOptions;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use App\Repository\Main\CurrencyRepository;
use App\Repository\Main\UserBalanceRepository;
use App\Repository\Main\UserRepository;
use App\Services\AgencyCabinetService;
use App\Services\UserBalanceTransactionService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Команда для синхронизации всех кабинетов
 *
 * Создаёт недостающие кабинеты обновляет существующие
 */
class AutoCompletionCabinets extends Command
{
    protected static $defaultName = 'agency:auto-completion-cabinets';

    /**
     * @var MyTargetApi
     */
    private MyTargetApi $myTargetApi;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var AgencyCabinetRepository
     */
    private AgencyCabinetRepository $agencyCabinetRepository;

    /**
     * @var AgencyCabinetService
     */
    private AgencyCabinetService $agencyCabinetService;

    /**
     * @var UserBalanceTransactionService
     */
    private UserBalanceTransactionService $userBalanceTransactionService;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    /**
     * @var UserBalanceRepository
     */
    private UserBalanceRepository $userBalanceRepository;
    /**
     * @var CurrencyRepository
     */
    private CurrencyRepository $currencyRepository;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var NotifierInterface
     */
    private NotifierInterface $notifier;

    public function __construct(
        LoggerInterface $logger,
        UserRepository $userRepository,
        UserBalanceRepository $userBalanceRepository,
        UserBalanceTransactionService $userBalanceTransactionService,
        MyTargetApi $myTargetApi,
        EntityManagerInterface $entityManager,
        CurrencyRepository $currencyRepository,
        AgencyCabinetRepository $agencyCabinetRepository,
        NotifierInterface $notifier
    )
    {
        parent::__construct(null);
        $this->myTargetApi = $myTargetApi;
        $this->entityManager = $entityManager;
        $this->agencyCabinetRepository = $agencyCabinetRepository;
        $this->userBalanceTransactionService = $userBalanceTransactionService;
        $this->userRepository = $userRepository;
        $this->userBalanceRepository = $userBalanceRepository;
        $this->currencyRepository = $currencyRepository;
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    protected function configure()
    {
        $this
            ->setDescription('Auto completion agency cabinets')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws QueryException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var AgencyCabinet[] $cabinets */
        $cabinets = $this->agencyCabinetRepository
            ->createQueryBuilder('ac')
            ->indexBy('ac', 'ac.apiId')
            ->where('ac.autoCompletion = :autoCompletion')
            ->andWhere('ac.status IN (:statuses)')
            ->setParameter(':autoCompletion', true)
            ->setParameter(':statuses', [AgencyCabinet::STATUS_ACTIVE, AgencyCabinet::STATUS_HIDDEN])
            ->getQuery()
            ->getResult();

        $myTargetUser = $this->userRepository->findOneBy(['email' => User::SYSTEM_USER_MYTARGET]);

        $currency = $this->currencyRepository->findOneBy(['code' => 'USD']);

        $userBalanceTo = $this->userBalanceRepository->findOneBy([
            'user' => $myTargetUser,
            'currency' => $currency,
        ]);

        $apiCabinets = $this->myTargetApi->getClientsByIds(...array_keys($cabinets));

        foreach ($apiCabinets as $apiCabinet) {

            if ($apiCabinet->getUser()->getStatus() === UserClient::STATUS_ACTIVE) {
                try {
                    $cabinet = $cabinets[$apiCabinet->getUser()->getId()];

                    $userBalanceFrom = $this->userBalanceRepository->findOneBy([
                        'user' => $cabinet->getUser(),
                        'currency' => $currency,
                    ]);

                    if (bccomp($cabinet->getBalanceBelow(), $apiCabinet->getUser()->getAccount()->getBalance(), 2) === 1) { // Если баланс ниже чем указан в настройке
                        if (bccomp($userBalanceFrom->getAmount(), $cabinet->getReplenishmentAmount(), 2) >= 0) { // Если на балансе достаточно средств
                            $this->userBalanceTransactionService->newTransaction(
                                $userBalanceFrom,
                                $userBalanceTo,
                                UserBalanceTransaction::OPERATION_TO_MT_CABINET,
                                $cabinet->getReplenishmentAmount(),
                                [],
                                null,
                                $cabinet
                            );
                        } else {
                            // На балансе недостаточно средств для пополнения кабинета
                            $message = new LowBalanceNotification('На балансе недостаточно средств для пополнения кабинета: ' . $cabinet->getName() . ', требуется ' . $cabinet->getReplenishmentAmount() . ' USD, на балансе ' . $userBalanceFrom->getAmount() . ' USD', ['chat/telegram'], new TelegramMessageOptions('525775843'));

                            $this->notifier->send($message, new Recipient('525775843'));

                            $cabinet->setAutoCompletion(false);
                            $this->entityManager->persist($cabinet);
                            $this->entityManager->flush();

                        }
                    }

                } catch (Exception $exception) {
                    $this->logger->error($exception->getMessage(), ['exception' => $exception]);
                }
            }
        }

        return 0;
    }
}
