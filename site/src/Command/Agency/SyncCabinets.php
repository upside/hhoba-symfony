<?php

namespace App\Command\Agency;

use App\Agency\MyTargetApi;
use App\Agency\Vo\MyTarget\UserClient;
use App\Entity\Main\Agency\AgencyCabinet;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Команда для синхронизации всех кабинетов
 *
 * Создаёт недостающие кабинеты обновляет существующие
 */
class SyncCabinets extends Command
{
    protected static $defaultName = 'agency:sync-cabinets';
    /**
     * @var MyTargetApi
     */
    private MyTargetApi $myTargetApi;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var AgencyCabinetRepository
     */
    private AgencyCabinetRepository $agencyCabinetRepository;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    public function __construct(
        MyTargetApi $myTargetApi,
        EntityManagerInterface $entityManager,
        AgencyCabinetRepository $agencyCabinetRepository,
        SerializerInterface $serializer
    )
    {
        parent::__construct(null);
        $this->myTargetApi = $myTargetApi;
        $this->entityManager = $entityManager;
        $this->agencyCabinetRepository = $agencyCabinetRepository;
        $this->serializer = $serializer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Sync agency cabinets')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $updatedAt = new DateTime();

        $this->entityManager->beginTransaction();

        try {

            /** @var AgencyCabinet[] $agencyCabinets */
            $agencyCabinets = $this->agencyCabinetRepository
                ->createQueryBuilder('ac')
                ->indexBy('ac', 'ac.apiId')
                ->getQuery()
                ->getResult();

            $apiAgencyCabinets = $this->myTargetApi->getClientsByIds(...array_keys($agencyCabinets));

            foreach ($apiAgencyCabinets as $apiAgencyCabinet) {

                $agencyCabinet = $agencyCabinets[$apiAgencyCabinet->getUser()->getId()];

                $agencyCabinet->setBalance($apiAgencyCabinet->getUser()->getAccount()->getBalance());
                $agencyCabinet->setParams(json_decode($this->serializer->serialize($apiAgencyCabinet, 'json'), true));
                $agencyCabinet->setUpdatedAt($updatedAt);

                switch ($apiAgencyCabinet->getUser()->getStatus()) {
                    case UserClient::STATUS_BLOCKED:
                        $agencyCabinet->setStatus(AgencyCabinet::STATUS_BLOCKED);
                        $agencyCabinet->setAutoCompletion(false);
                        break;
                    case UserClient::STATUS_DELETED:
                        $agencyCabinet->setStatus(AgencyCabinet::STATUS_DELETED);
                        $agencyCabinet->setAutoCompletion(false);
                        break;
                }

                $this->entityManager->persist($agencyCabinet);

            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (Exception $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }

        return 0;
    }
}
