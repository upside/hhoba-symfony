<?php

namespace App\Message;

final class LeadMessage
{
    private string $clickUuid;
    private string $leadUuid;
    private string $phone;
    private string $createdAt;
    private array $params;

    public function __construct(
        string $clickUuid,
        string $leadUuid,
        string $phone,
        string $createdAt,
        array $params = []
    )
    {
        $this->clickUuid = $clickUuid;
        $this->leadUuid = $leadUuid;
        $this->phone = $phone;
        $this->createdAt = $createdAt;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getClickUuid(): string
    {
        return $this->clickUuid;
    }

    /**
     * @return string
     */
    public function getLeadUuid(): string
    {
        return $this->leadUuid;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
