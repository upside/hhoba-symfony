<?php

namespace App\Message;

final class CpaLeadMessage
{
    /**
     * @var int
     */
    private int $leadId;

    public function __construct(int $leadId)
    {
        $this->leadId = $leadId;
    }

    /**
     * @return int
     */
    public function getLeadId(): int
    {
        return $this->leadId;
    }
}
