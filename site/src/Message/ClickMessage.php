<?php

namespace App\Message;

final class ClickMessage
{
    private string $streamUuid;
    private string $clickUuid;
    private string $createdAt;
    private array $params;

    public function __construct(
        string $streamUuid,
        string $clickUuid,
        string $createdAt,
        array $params = []
    )
    {
        $this->streamUuid = $streamUuid;
        $this->clickUuid = $clickUuid;
        $this->createdAt = $createdAt;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getStreamUuid(): string
    {
        return $this->streamUuid;
    }

    /**
     * @return string
     */
    public function getClickUuid(): string
    {
        return $this->clickUuid;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
