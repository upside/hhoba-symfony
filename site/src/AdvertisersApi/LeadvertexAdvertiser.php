<?php

namespace App\AdvertisersApi;

use App\Entity\Main\Lead;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LeadvertexAdvertiser implements AdvertisersApiInterface
{
    public const STATUS_NEW = 'new';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_TRASH = 'trash';

    public const STATUSES = [
        self::STATUS_NEW => Lead::STATUS_NEW,
        self::STATUS_REJECTED => Lead::STATUS_DECLINE,
        self::STATUS_CONFIRMED => Lead::STATUS_CONFIRM,
        self::STATUS_TRASH => Lead::STATUS_TRASH,
    ];

    /**
     * @var UrlGeneratorInterface
     */
    private UrlGeneratorInterface $urlGenerator;

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param HttpClientInterface $httpClient
     */
    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        HttpClientInterface $httpClient
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->httpClient = $httpClient;
    }

    /**
     * @param Lead $lead
     * @return CpaResponseVo
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function send(Lead $lead): CpaResponseVo
    {
        $response = $this->httpClient->request('POST', $this->getApiUrl(), [
            'query' => [
                'api_key' => '',
            ],
            'body' => [
                'name' => $lead->getId(),
                'phone' => $lead->getPhone(),
                'ip' => $lead->getIpAddress()->getValue(),
                'userAgent' => $lead->getUserAgent()->getValue(),
                'campaignHash' => $lead->getClick()->getStream()->getOffer()->getParams()['campaignHash'],
                'country_call' => $lead->getGeo()->getIso2(),
            ],
        ]);

        $data = $response->toArray(true);

        if (key_exists('success', $data)) {
            return new CpaResponseVo($data['data']['click_id'], $response->getContent());
        } else {
            return new CpaResponseVo($data['data']['click_id'], $response->getContent(), true);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'LuckyOnline';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'https://lucky.online/api-webmaster/lead.html';
    }

    /**
     * @return array|string[]
     */
    public function getPostBackParams(): array
    {
        return [
            'click_id' => '{click_id}',
            'status' => '{status}',
            'amount' => '{amount}',
            'click_country' => '{click.country}',
            'get_utm_source' => '{get.utm_source}',
            'get_utm_content' => '{get.utm_content}',
            'get_utm_term' => '{get.utm_term}',
            'get_utm_medium' => '{get.utm_medium}',
            'get_utm_campaign' => '{get.utm_campaign}',
            'get_subid1' => '{get.subid1}',
            'get_subid2' => '{get.subid2}',
            'get_subid3' => '{get.subid3}',
            'click_ts' => '{click_ts}',
            'lead_ts' => '{lead_ts}',
            'offer_id' => '{offer_id}',
            'click_ip' => '{click.ip}',
            'currency' => '{currency}',
        ];
    }

    /**
     * @return string
     */
    public function getIdParam(): string
    {
        return 'click_id';
    }

    /**
     * @return string
     */
    public function getStatusParam(): string
    {
        return 'status';
    }

    /**
     * @return string
     */
    public function getCountryParam(): string
    {
        return 'click_country';
    }

    /**
     * @param string $status
     * @return int
     * @throws Exception
     */
    public function getMappedStatus(string $status): int
    {
        if (isset(self::STATUSES[$status])) {
            return self::STATUSES[$status];
        }

        throw new Exception('Неизвесный статус: ' . $status);
    }
}
