<?php

namespace App\AdvertisersApi;

use App\Entity\Main\Advertiser;
use App\Entity\Main\Lead;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Container\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdvertiserService
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var UrlGeneratorInterface
     */
    private UrlGeneratorInterface $urlGenerator;

    /**
     * @param ContainerInterface $container
     * @param EntityManagerInterface $entityManager
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param Lead $lead
     * @return CpaResponseVo
     * @throws Exception
     */
    public function sendLeadToAdvertiser(Lead $lead)
    {
        return $this
            ->getAdvertiserApi($lead->getClick()->getStream()->getOffer()->getAdvertiser()->getApiId())
            ->send($lead);
    }

    /**
     * @param Advertiser $advertiser
     * @return string
     * @throws Exception
     */
    public function getPostbackUrl(Advertiser $advertiser): string
    {
        return $this->urlGenerator->generate('api_postback', [
            'advertiser' => $advertiser->getId(),
            ...$this->getAdvertiserApi($advertiser->getId())->getPostBackParams()
        ]);
    }

    /**
     * @param int $advertiserApiId
     * @return AdvertisersApiInterface
     * @throws Exception
     */
    private function getAdvertiserApi(int $advertiserApiId): AdvertisersApiInterface
    {
        switch ($advertiserApiId) {
            case AdvertisersApiInterface::ADVERTISER_LUCKY_ONLINE:
                return $this->container->get(LuckyOnlineAdvertiser::class);
                break;
            case AdvertisersApiInterface::ADVERTISER_LEAD_VERTEX:
                return $this->container->get(LeadvertexAdvertiser::class);
                break;
            case AdvertisersApiInterface::ADVERTISER_EVERAD:
                return $this->container->get(EveradAdvertiser::class);
                break;
            default:
                throw new Exception('Неизвесный апи: ' . $advertiserApiId);
        }
    }
}
