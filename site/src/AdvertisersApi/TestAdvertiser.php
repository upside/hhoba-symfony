<?php

namespace App\AdvertisersApi;

use App\Entity\Main\Lead;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TestAdvertiser implements AdvertisersApiInterface
{
    public const STATUS_NEW = 'new';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_TRASH = 'trash';

    public const STATUSES = [
        self::STATUS_NEW => Lead::STATUS_NEW,
        self::STATUS_REJECTED => Lead::STATUS_DECLINE,
        self::STATUS_CONFIRMED => Lead::STATUS_CONFIRM,
        self::STATUS_TRASH => Lead::STATUS_TRASH,
    ];

    /**
     * @var UrlGeneratorInterface
     */
    private UrlGeneratorInterface $urlGenerator;

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param HttpClientInterface $httpClient
     */
    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        HttpClientInterface $httpClient
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->httpClient = $httpClient;
    }

    /**
     * @param Lead $lead
     * @return CpaResponseVo
     */
    public function send(Lead $lead): CpaResponseVo
    {
        if (true) {
            return new CpaResponseVo($lead->getId(), 'test success');
        } else {
            return new CpaResponseVo('', 'test error', true);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Test';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return '';
    }

    /**
     * @return array|string[]
     */
    public function getPostBackParams(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getIdParam(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getStatusParam(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getCountryParam(): string
    {
        return '';
    }

    /**
     * @param string $status
     * @return int
     * @throws Exception
     */
    public function getMappedStatus(string $status): int
    {
        if (isset(self::STATUSES[$status])) {
            return self::STATUSES[$status];
        }

        throw new Exception('Неизвесный статус: ' . $status);
    }
}
