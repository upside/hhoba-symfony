<?php

namespace App\AdvertisersApi;

use App\Entity\Main\Lead;

interface AdvertisersApiInterface
{
    public const ADVERTISER_TEST = 0;
    public const ADVERTISER_LUCKY_ONLINE = 1;
    public const ADVERTISER_LEAD_VERTEX = 2;
    public const ADVERTISER_EVERAD = 3;

    public const ADVERTISERS = [
        self::ADVERTISER_TEST => 'Test',
        self::ADVERTISER_LUCKY_ONLINE => 'LuckyOnline',
        self::ADVERTISER_LEAD_VERTEX => 'LeadVertex',
        self::ADVERTISER_EVERAD => 'Everad',
    ];


    /**
     * @param Lead $lead
     * @return CpaResponseVo
     */
    public function send(Lead $lead): CpaResponseVo;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getApiUrl(): string;

    /**
     * @return array
     */
    public function getPostBackParams(): array;

    /**
     * @return string
     */
    public function getIdParam(): string;

    /**
     * @return string
     */
    public function getStatusParam(): string;

    /**
     * @return string
     */
    public function getCountryParam(): string;

    /**
     * @param string $status
     * @return int
     */
    public function getMappedStatus(string $status): int;
}
