<?php

namespace App\AdvertisersApi;

class CpaResponseVo
{
    private string $id;
    private string $data;
    private bool $error;

    public function __construct(string $id, string $data, bool $error = false)
    {
        $this->id = $id;
        $this->data = $data;
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error;
    }
}
