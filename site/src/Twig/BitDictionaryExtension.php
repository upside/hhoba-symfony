<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class BitDictionaryExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('bit_dictionary', [$this, 'showBitTitles']),
        ];
    }

    public function showBitTitles($bits, $dictionary)
    {
        return implode(', ', array_filter($dictionary, function ($bit) use ($bits) {
            return ($bit & $bits) > 0;
        }, ARRAY_FILTER_USE_KEY));
    }
}
