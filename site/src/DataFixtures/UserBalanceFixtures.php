<?php

namespace App\DataFixtures;

use App\Entity\Main\UserBalance;
use App\Repository\Main\CurrencyRepository;
use App\Repository\Main\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserBalanceFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var CurrencyRepository
     */
    private CurrencyRepository $currencyRepository;

    public function __construct(UserRepository $userRepository, CurrencyRepository $currencyRepository)
    {
        $this->userRepository = $userRepository;
        $this->currencyRepository = $currencyRepository;
    }

    public function load(ObjectManager $manager)
    {

        $currency = $this->currencyRepository->findOneBy(['code' => 'USD']);
        $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            $userBalance = new UserBalance();
            $userBalance->setUser($user);
            $userBalance->setCurrency($currency);
            $userBalance->setAmount('0.00');
            $userBalance->setType(UserBalance::TYPE_AGENCY_USD);
            $manager->persist($userBalance);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CurrencyFixtures::class,
        ];
    }
}
