<?php

namespace App\DataFixtures;

use App\Entity\Main\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CurrencyFixtures extends Fixture
{
    public const CURRENCY_RUB = 'currency_rub';
    public const CURRENCY_USD = 'currency_usd';

    public function load(ObjectManager $manager)
    {
        $currency1 = new Currency();
        $currency1->setNameEn('Ruble');
        $currency1->setNameRu('Рубль');
        $currency1->setAbbreviationEn('rub');
        $currency1->setAbbreviationRu('руб');
        $currency1->setCode('RUB');
        $currency1->setRate(1);
        $manager->persist($currency1);

        $currency2 = new Currency();
        $currency2->setNameEn('Dollar');
        $currency2->setNameRu('Доллар');
        $currency2->setAbbreviationEn('usd');
        $currency2->setAbbreviationRu('дол');
        $currency2->setCode('USD');
        $currency2->setRate('74.7119');
        $manager->persist($currency2);

        $this->addReference(self::CURRENCY_RUB, $currency1);
        $this->addReference(self::CURRENCY_USD, $currency2);

        $manager->flush();
    }
}
