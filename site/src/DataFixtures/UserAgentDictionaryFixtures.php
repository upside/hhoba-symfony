<?php

namespace App\DataFixtures;

use App\Entity\Main\UserAgentDictionary;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserAgentDictionaryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        return true;

        $faker = Factory::create('ru_RU');

        for ($i = 0; $i < 100; $i++){
            for ($o = 0; $o < 100; $o++){
                $userAgent = new UserAgentDictionary();
                $userAgent->setValue($faker->userAgent);
                $manager->persist($userAgent);
            }
            $manager->flush();
        }
    }
}
