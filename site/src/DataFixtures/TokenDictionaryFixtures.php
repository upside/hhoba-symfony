<?php

namespace App\DataFixtures;

use App\Entity\Main\TokenDictionary;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TokenDictionaryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        return true;
        $faker = Factory::create('ru_RU');

        for ($i = 18; $i < 71; $i++){
            $tokenDictionary1 = new TokenDictionary();
            $tokenDictionary1->setName(TokenDictionary::TOKEN1_NAME);
            $tokenDictionary1->setValue($i);
            $manager->persist($tokenDictionary1);
        }

        //utm_medium
        $utmMedium = [
            'cpc',
            'display',
            'price',
            'retargeting',
            'affiliate',
            'qrcode',
            'social_cpc',
            'social_post',
            'special',
        ];

        foreach ($utmMedium as $medium){
            $tokenDictionary3 = new TokenDictionary();
            $tokenDictionary3->setName(TokenDictionary::TOKEN3_NAME);
            $tokenDictionary3->setValue($medium);
            $manager->persist($tokenDictionary3);
        }

        //utm_source
        $utmSource = [
            'yandex',
            'yandex_direct',
            'google',
            'google_adwords',
            'myTarget',
            'vk',
            'facebook',
            'instagram',
            'mail',
            'newslater',
            'avito',
            'rbc',
        ];

        foreach ($utmSource as $source){

            $tokenDictionary4 = new TokenDictionary();
            $tokenDictionary4->setName(TokenDictionary::TOKEN4_NAME);
            $tokenDictionary4->setValue($source);
            $manager->persist($tokenDictionary4);
        }

        for ($i = 0; $i < 1000; $i++) {

            //banner_id
            $tokenDictionary2 = new TokenDictionary();
            $tokenDictionary2->setName(TokenDictionary::TOKEN2_NAME);
            $tokenDictionary2->setValue(rand(10000000, 99999999));
            $manager->persist($tokenDictionary2);

            //utm_campaign
            $tokenDictionary5 = new TokenDictionary();
            $tokenDictionary5->setName(TokenDictionary::TOKEN5_NAME);
            $tokenDictionary5->setValue($faker->text);
            $manager->persist($tokenDictionary5);

            //utm_term
            $tokenDictionary6 = new TokenDictionary();
            $tokenDictionary6->setName(TokenDictionary::TOKEN6_NAME);
            $tokenDictionary6->setValue($faker->text);
            $manager->persist($tokenDictionary6);

            //utm_content
            $tokenDictionary7 = new TokenDictionary();
            $tokenDictionary7->setName(TokenDictionary::TOKEN7_NAME);
            $tokenDictionary7->setValue($faker->text);
            $manager->persist($tokenDictionary7);

            $tokenDictionary8 = new TokenDictionary();
            $tokenDictionary8->setName(TokenDictionary::TOKEN8_NAME);
            $tokenDictionary8->setValue($faker->text);
            $manager->persist($tokenDictionary8);

            $tokenDictionary9 = new TokenDictionary();
            $tokenDictionary9->setName(TokenDictionary::TOKEN9_NAME);
            $tokenDictionary9->setValue($faker->text);
            $manager->persist($tokenDictionary9);

            $tokenDictionary10 = new TokenDictionary();
            $tokenDictionary10->setName(TokenDictionary::TOKEN10_NAME);
            $tokenDictionary10->setValue($faker->text);
            $manager->persist($tokenDictionary10);

            $tokenDictionary11 = new TokenDictionary();
            $tokenDictionary11->setName(TokenDictionary::TOKEN11_NAME);
            $tokenDictionary11->setValue($faker->text);
            $manager->persist($tokenDictionary11);

            $tokenDictionary12 = new TokenDictionary();
            $tokenDictionary12->setName(TokenDictionary::TOKEN12_NAME);
            $tokenDictionary12->setValue($faker->text);
            $manager->persist($tokenDictionary12);

            $tokenDictionary13 = new TokenDictionary();
            $tokenDictionary13->setName(TokenDictionary::TOKEN13_NAME);
            $tokenDictionary13->setValue($faker->text);
            $manager->persist($tokenDictionary13);

            $tokenDictionary14 = new TokenDictionary();
            $tokenDictionary14->setName(TokenDictionary::TOKEN14_NAME);
            $tokenDictionary14->setValue($faker->text);
            $manager->persist($tokenDictionary14);

            $tokenDictionary15 = new TokenDictionary();
            $tokenDictionary15->setName(TokenDictionary::TOKEN15_NAME);
            $tokenDictionary15->setValue($faker->text);
            $manager->persist($tokenDictionary15);

            $tokenDictionary16 = new TokenDictionary();
            $tokenDictionary16->setName(TokenDictionary::TOKEN16_NAME);
            $tokenDictionary16->setValue($faker->text);
            $manager->persist($tokenDictionary16);

        }

        $manager->flush();
    }
}
