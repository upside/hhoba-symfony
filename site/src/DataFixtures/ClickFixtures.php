<?php

namespace App\DataFixtures;

use App\Entity\Main\Click;
use App\Repository\Main\GeoRepository;
use App\Repository\Main\IpAddressDictionaryRepository;
use App\Repository\Main\StreamRepository;
use App\Repository\Main\TokenDictionaryRepository;
use App\Repository\Main\UserAgentDictionaryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class ClickFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var TokenDictionaryRepository
     */
    private TokenDictionaryRepository $tokenDictionaryRepository;
    /**
     * @var GeoRepository
     */
    private GeoRepository $geoRepository;
    /**
     * @var StreamRepository
     */
    private StreamRepository $streamRepository;

    /**
     * @var IpAddressDictionaryRepository
     */
    private IpAddressDictionaryRepository $ipAddressDictionaryRepository;

    /**
     * @var UserAgentDictionaryRepository
     */
    private UserAgentDictionaryRepository $userAgentDictionaryRepository;

    public function __construct(
        TokenDictionaryRepository $tokenDictionaryRepository,
        GeoRepository $geoRepository,
        StreamRepository $streamRepository,
        IpAddressDictionaryRepository $ipAddressDictionaryRepository,
        UserAgentDictionaryRepository $userAgentDictionaryRepository
    )
    {
        $this->tokenDictionaryRepository = $tokenDictionaryRepository;
        $this->geoRepository = $geoRepository;
        $this->streamRepository = $streamRepository;
        $this->ipAddressDictionaryRepository = $ipAddressDictionaryRepository;
        $this->userAgentDictionaryRepository = $userAgentDictionaryRepository;
    }

    public function load(ObjectManager $manager)
    {
        return true;
        $geo = $this->geoRepository->getByIsoCode(...[
            'RU',
            'BY',
            'KZ',
            'UA',
            'GE',
            'AM',
            'IT',
            'ES',
            'DE',
            'KG',
            'GR',
            'RO',
            'BG',
            'PL',
            'AZ',
            'AT',
            'PT',
            'HU',
            'ID',
            'TH',
            'VN',
            'HR',
            'RS',
            'BA',
            'AE',
        ]);
        $streams = $this->streamRepository->findAll();
        $ipAddresses = $this->ipAddressDictionaryRepository->findAll();
        $userAgents = $this->userAgentDictionaryRepository->findAll();

        $token1 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_1')
            ->getQuery()
            ->getResult();

        $token2 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_2')
            ->getQuery()
            ->getResult();

        $token3 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_3')
            ->getQuery()
            ->getResult();

        $token4 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_4')
            ->getQuery()
            ->getResult();

        $token5 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_5')
            ->getQuery()
            ->getResult();

        $token6 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_6')
            ->getQuery()
            ->getResult();

        $token7 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_7')
            ->getQuery()
            ->getResult();

        $token8 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_8')
            ->getQuery()
            ->getResult();

        $token9 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_9')
            ->getQuery()
            ->getResult();

        $token10 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_10')
            ->getQuery()
            ->getResult();

        $token11 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_11')
            ->getQuery()
            ->getResult();

        $token12 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_12')
            ->getQuery()
            ->getResult();

        $token13 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_13')
            ->getQuery()
            ->getResult();

        $token14 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_14')
            ->getQuery()
            ->getResult();

        $token15 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_15')
            ->getQuery()
            ->getResult();

        $token16 = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->where('td.name = :token')
            ->setParameter(':token', 'token_16')
            ->getQuery()
            ->getResult();

        for ($i = 0; $i < 100; $i++) {
            for ($k = 0; $k < 1000; $k++) {
                $click = new Click();
                $click->setUuid(Uuid::uuid4());
                $click->setStream($streams[array_rand($streams)]);
                $click->setGeo($geo[array_rand($geo)]);
                $click->setIpAddress($ipAddresses[array_rand($ipAddresses)]);
                $click->setUserAgent($userAgents[array_rand($userAgents)]);
                $click->addToken($token1[array_rand($token1)]);
                $click->addToken($token2[array_rand($token2)]);
                $click->addToken($token3[array_rand($token3)]);
                $click->addToken($token4[array_rand($token4)]);
                $click->addToken($token5[array_rand($token5)]);
                $click->addToken($token6[array_rand($token6)]);
                $click->addToken($token7[array_rand($token7)]);
                $click->addToken($token8[array_rand($token8)]);
                $click->addToken($token9[array_rand($token9)]);
                $click->addToken($token10[array_rand($token10)]);
                $click->addToken($token11[array_rand($token11)]);
                $click->addToken($token12[array_rand($token12)]);
                $click->addToken($token13[array_rand($token13)]);
                $click->addToken($token14[array_rand($token14)]);
                $click->addToken($token15[array_rand($token15)]);
                $click->addToken($token16[array_rand($token16)]);
                $manager->persist($click);
            }
            $manager->flush();
        }

    }

    public function getDependencies()
    {
        return [
            GeoFixtures::class,
            StreamFixtures::class,
            TokenDictionaryFixtures::class,
            IpAddressDictionaryFixtures::class,
            UserAgentDictionaryFixtures::class,
        ];
    }
}
