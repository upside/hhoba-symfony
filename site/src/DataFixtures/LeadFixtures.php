<?php

namespace App\DataFixtures;

use App\Entity\Main\Lead;
use App\Repository\Main\ClickRepository;
use App\Repository\Main\GeoRepository;
use App\Repository\Main\IpAddressDictionaryRepository;
use App\Repository\Main\TokenDictionaryRepository;
use App\Repository\Main\UserAgentDictionaryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Ramsey\Uuid\Uuid;

class LeadFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ClickRepository
     */
    private ClickRepository $clickRepository;

    /**
     * @var TokenDictionaryRepository
     */
    private TokenDictionaryRepository $tokenDictionaryRepository;

    /**
     * @var GeoRepository
     */
    private GeoRepository $geoRepository;

    /**
     * @var IpAddressDictionaryRepository
     */
    private IpAddressDictionaryRepository $ipAddressDictionaryRepository;

    /**
     * @var UserAgentDictionaryRepository
     */
    private UserAgentDictionaryRepository $userAgentDictionaryRepository;

    public function __construct(
        GeoRepository $geoRepository,
        ClickRepository $clickRepository,
        TokenDictionaryRepository $tokenDictionaryRepository,
        IpAddressDictionaryRepository $ipAddressDictionaryRepository,
        UserAgentDictionaryRepository $userAgentDictionaryRepository
    )
    {
        $this->clickRepository = $clickRepository;
        $this->tokenDictionaryRepository = $tokenDictionaryRepository;
        $this->geoRepository = $geoRepository;
        $this->ipAddressDictionaryRepository = $ipAddressDictionaryRepository;
        $this->userAgentDictionaryRepository = $userAgentDictionaryRepository;
    }

    public function load(ObjectManager $manager)
    {
        return true;

        $clickIds = $this->clickRepository->createQueryBuilder('c')
            ->select('c.id')
            ->getQuery()
            ->setMaxResults(20000)
            ->getArrayResult();

        $tokenIds = $this->tokenDictionaryRepository->createQueryBuilder('td')
            ->select(['td.id'])
            ->getQuery()
            ->getArrayResult();

        $geo = $this->geoRepository->getByIsoCode(...[
            'RU',
            'BY',
            'KZ',
            'UA',
            'GE',
            'AM',
            'IT',
            'ES',
            'DE',
            'KG',
            'GR',
            'RO',
            'BG',
            'PL',
            'AZ',
            'AT',
            'PT',
            'HU',
            'ID',
            'TH',
            'VN',
            'HR',
            'RS',
            'BA',
            'AE',
        ]);
        $ipAddresses = $this->ipAddressDictionaryRepository->findAll();
        $userAgents = $this->userAgentDictionaryRepository->findAll();

        $faker = Factory::create('ru_RU');

        foreach ($clickIds as $clickId) {

            $click = $this->clickRepository->find($clickId['id']);

            $keys = array_rand($tokenIds, 16);
            $ids = [];
            foreach ($keys as $key) {
                $ids[] = $tokenIds[$key]['id'];
            }

            $tokens = $this->tokenDictionaryRepository->createQueryBuilder('td')
                ->where('td.id IN (:ids)')
                ->setParameter(':ids', $ids)
                ->getQuery()
                ->getResult();

            $lead = new Lead();
            $lead->setUuid(Uuid::uuid4());
            $lead->setClick($click);
            $lead->setGeo($geo[array_rand($geo)]);
            $lead->setCurrency($click->getStream()->getOffer()->getTariffs()[0]->getCurrency());
            $lead->setPrice($click->getStream()->getOffer()->getTariffs()[0]->getValue());
            $lead->setStatus(rand(1, 4));
            $lead->setPhone($faker->phoneNumber);
            $lead->setName($faker->name);
            $lead->setComment($faker->text);
            $lead->setCpaId(uniqid());
            $lead->setCpaGeo($geo[array_rand($geo)]);
            $lead->setIpAddress($ipAddresses[array_rand($ipAddresses)]);
            $lead->setUserAgent($userAgents[array_rand($userAgents)]);

            foreach ($tokens as $token) {
                $lead->addToken($token);
            }

            $manager->persist($lead);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ClickFixtures::class,
            TokenDictionaryFixtures::class,
            IpAddressDictionaryFixtures::class,
            UserAgentDictionaryFixtures::class,
        ];
    }
}
