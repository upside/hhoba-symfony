<?php

namespace App\DataFixtures;

use App\AdvertisersApi\AdvertisersApiInterface;
use App\Entity\Main\Advertiser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AdvertiserFixtures extends Fixture
{

    public const ADVERTISER_LUCKY = 'advertiser_lucky';
    public const ADVERTISER_LEADVERTEX = 'advertiser_leadvertex';
    public const ADVERTISER_EVERAD = 'advertiser_everad';

    public function load(ObjectManager $manager)
    {
        $advertiser1 = new Advertiser();
        $advertiser1->setName('Lucky Online Advertiser');
        $advertiser1->setStatus(Advertiser::STATUS_ACTIVE);
        $advertiser1->setApiId(AdvertisersApiInterface::ADVERTISER_LUCKY_ONLINE);
        $advertiser1->setParams([
            'api_key' => 'test',
        ]);
        $manager->persist($advertiser1);

        $advertiser2 = new Advertiser();
        $advertiser2->setName('Leadvertex Advertiser');
        $advertiser2->setStatus(Advertiser::STATUS_ACTIVE);
        $advertiser2->setApiId(AdvertisersApiInterface::ADVERTISER_LEAD_VERTEX);
        $advertiser2->setParams([
            'token' => 'test',
            'WebmasterID' => 1,
        ]);
        $manager->persist($advertiser2);

        $advertiser3 = new Advertiser();
        $advertiser3->setName('Everad Advertiser');
        $advertiser3->setStatus(Advertiser::STATUS_ACTIVE);
        $advertiser3->setApiId(AdvertisersApiInterface::ADVERTISER_EVERAD);
        $advertiser3->setParams([]);
        $manager->persist($advertiser3);

        $this->addReference(self::ADVERTISER_LUCKY, $advertiser1);
        $this->addReference(self::ADVERTISER_LEADVERTEX, $advertiser2);
        $this->addReference(self::ADVERTISER_EVERAD, $advertiser3);

        $manager->flush();
    }
}
