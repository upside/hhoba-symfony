<?php

namespace App\DataFixtures;

use App\Entity\Main\IpAddressDictionary;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class IpAddressDictionaryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        return true;

        $faker = Factory::create('ru_RU');

        for ($i = 0; $i < 100; $i++){
            for ($o = 0; $o < 100; $o++){
                $ipAddress = new IpAddressDictionary();
                $ipAddress->setValue($faker->ipv4);
                $manager->persist($ipAddress);
            }
            $manager->flush();
        }
    }
}
