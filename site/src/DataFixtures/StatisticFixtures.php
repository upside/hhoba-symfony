<?php

namespace App\DataFixtures;

use App\Entity\Main\Click;
use App\Entity\Main\TokenDictionary;
use App\Entity\Statistic\Statistic;
use App\Repository\Main\ClickRepository;
use App\Repository\Statistic\StatisticRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

class StatisticFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ClickRepository
     */
    private ClickRepository $clickRepository;

    /**
     * @var StatisticRepository
     */
    private StatisticRepository $statisticRepository;

    public function __construct(ClickRepository $clickRepository, StatisticRepository $statisticRepository)
    {
        $this->clickRepository = $clickRepository;
        $this->statisticRepository = $statisticRepository;
    }

    public function load(ObjectManager $manager)
    {
        return;
        $totalClicks = $this->clickRepository->createQueryBuilder('c')
            ->select('count(c)')
            ->getQuery()
            ->getSingleScalarResult();

        $maxResults = 100;
        for ($i = 0; $i <= $totalClicks; $i += $maxResults) {

            /** @var Click[] $clicks */
            $clicks = $this->clickRepository->createQueryBuilder('c')
                ->addSelect(['l', 'g', 's', 'o'])
                ->leftJoin('c.lead', 'l')
                ->join('c.geo', 'g')
                ->join('c.stream', 's')
                ->join('s.offer', 'o')
                ->setFirstResult($i)
                ->setMaxResults($maxResults)
                ->orderBy('c.id')
                ->getQuery()
                ->getResult();

            $statistics = [];
            /** @var Click[] $clicks */
            foreach ($clicks as $click) {

                $statistic = new Statistic();
                $statistic->setClickId($click->getId());
                $statistic->setClickCreatedAt($click->getCreatedAt());
                $statistic->setTrafficSourceId($click->getStream()->getTrafficSource()->getId());
                $statistic->setUserId($click->getStream()->getTrafficSource()->getUser()->getId());
                $statistic->setStreamId($click->getStream()->getId());
                $statistic->setOfferId($click->getStream()->getOffer()->getId());
                $statistic->setGeoId($click->getGeo()->getId());
                $statistic->setHour($click->getCreatedAt()->format('H'));
                $statistic->setDay($click->getCreatedAt()->format('d'));
                $statistic->setWeek($click->getCreatedAt()->format('W'));
                $statistic->setMonth($click->getCreatedAt()->format('m'));
                $statistic->setYear($click->getCreatedAt()->format('Y'));

                if (is_null($click->getLead()) === false) {
                    $statistic->setLeadId($click->getLead()->getId());
                    $statistic->setLeadStatus($click->getLead()->getStatus());
                    $statistic->setLeadPrice($click->getLead()->getPrice());
                    $statistic->setLeadCurrencyCode($click->getLead()->getCurrency()->getCode());
                    $statistic->setLeadCurrencyRate($click->getLead()->getCurrency()->getRate());
                    $statistic->setLeadCreatedAt($click->getLead()->getCreatedAt());
                }

                $statistic->setDate(new DateTime('now'));
                $statistic->setVersion(1);

                foreach ($click->getTokens() as $token) {
                    switch ($token->getName()) {
                        case TokenDictionary::TOKEN1_NAME:
                            $statistic->setToken1Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN2_NAME:
                            $statistic->setToken2Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN3_NAME:
                            $statistic->setToken3Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN4_NAME:
                            $statistic->setToken4Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN5_NAME:
                            $statistic->setToken5Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN6_NAME:
                            $statistic->setToken6Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN7_NAME:
                            $statistic->setToken7Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN8_NAME:
                            $statistic->setToken8Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN9_NAME:
                            $statistic->setToken9Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN10_NAME:
                            $statistic->setToken10Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN11_NAME:
                            $statistic->setToken11Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN12_NAME:
                            $statistic->setToken12Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN13_NAME:
                            $statistic->setToken13Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN14_NAME:
                            $statistic->setToken14Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN15_NAME:
                            $statistic->setToken15Id($token->getId());
                            break;
                        case TokenDictionary::TOKEN16_NAME:
                            $statistic->setToken16Id($token->getId());
                            break;
                    }

                }

                $statistics[] = $statistic;

            }

            try {
                $this->statisticRepository->persist(...$statistics);
            } catch (Exception $exception) {
                dump($exception);
            }

        }
    }

    public function getDependencies()
    {
        return [
            LeadFixtures::class,
        ];
    }
}
