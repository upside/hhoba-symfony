<?php

namespace App\DataFixtures;

use App\Entity\Main\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setEmail('admin@mail.ru');
        $user1->setRoles(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_ALLOWED_TO_SWITCH']);
        $user1->setPassword($this->passwordEncoder->encodePassword($user1, '12345678'));
        $manager->persist($user1);

        $user2 = new User();
        $user2->setEmail('upside89@gmail.com');
        $user2->setRoles(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_ALLOWED_TO_SWITCH']);
        $user2->setPassword($this->passwordEncoder->encodePassword($user2, '12345678'));
        $manager->persist($user2);

        $user3 = new User();
        $user3->setEmail('itcutlet@gmail.com');
        $user3->setRoles(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_ALLOWED_TO_SWITCH']);
        $user3->setPassword($this->passwordEncoder->encodePassword($user3, '12345678'));
        $manager->persist($user3);

        $user4 = new User();
        $user4->setEmail('test@mail.ru');
        $user4->setRoles(['ROLE_USER']);
        $user4->setPassword($this->passwordEncoder->encodePassword($user4, '12345678'));
        $manager->persist($user4);

        $user4 = new User();
        $user4->setEmail('mytarget@mytarget.ru');
        $user4->setRoles(['ROLE_SYSTEM']);
        $user4->setPassword('-');
        $manager->persist($user4);

        $manager->flush();
    }
}
