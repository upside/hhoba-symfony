DROP TABLE IF EXISTS statistics;

CREATE TABLE statistics
(
    click_id           UInt32,
    click_created_at   DateTime,
    traffic_source_id  UInt32,
    user_id            UInt32,
    stream_id          UInt32,
    offer_id           UInt32,
    geo_id             UInt32,
    lead_id            Nullable(UInt32),
    lead_status        Nullable(Int8),
    lead_price         Nullable(Decimal(8, 2)),
    lead_currency_code Nullable(String),
    lead_currency_rate Nullable(Decimal(16, 10)),
    lead_created_at    Nullable(DateTime),
    hour               Int8,
    day                Int8,
    week               Int8,
    month              Int8,
    year               Int16,
    token1_id          Nullable(UInt32),
    token2_id          Nullable(UInt32),
    token3_id          Nullable(UInt32),
    token4_id          Nullable(UInt32),
    token5_id          Nullable(UInt32),
    token6_id          Nullable(UInt32),
    token7_id          Nullable(UInt32),
    token8_id          Nullable(UInt32),
    token9_id          Nullable(UInt32),
    token10_id         Nullable(UInt32),
    token11_id         Nullable(UInt32),
    token12_id         Nullable(UInt32),
    token13_id         Nullable(UInt32),
    token14_id         Nullable(UInt32),
    token15_id         Nullable(UInt32),
    token16_id         Nullable(UInt32),
    date               Date,
    version            Int8
) ENGINE = CollapsingMergeTree(date, (click_id), 8192, version);
