DROP TRIGGER t_clicks ON clicks;
DROP TRIGGER t_clicks ON leads;
DROP TRIGGER t_clicks ON click_token_dictionary;
DROP FUNCTION sync_statistic();
DROP FUNCTION sync_token_statistic();

CREATE OR REPLACE FUNCTION sync_statistic() RETURNS TRIGGER AS
$$
DECLARE
    user_id     int;
    stream_name varchar;
    offer_id    int;
    offer_name  varchar;
    geo_iso_2   varchar;
BEGIN
    IF TG_TABLE_NAME = 'clicks' THEN

        SELECT s.user_id,
               s.offer_id,
               s.name
        INTO
            user_id,
            offer_id,
            stream_name
        FROM streams s
        WHERE s.id = NEW.stream_id
        LIMIT 1;

        SELECT o.name
        INTO offer_name
        FROM offers o
        WHERE o.id = offer_id
        LIMIT 1;

        SELECT g.iso_2
        INTO geo_iso_2
        FROM geo g
        WHERE id = NEW.geo_id
        LIMIT 1;

        INSERT INTO statistics
        (hour,
         day,
         week,
         month,
         year,
         user_id,
         stream_id,
         stream_name,
         offer_id,
         offer_name,
         geo_id,
         geo_iso2,
         click_id,
         created_at)
        VALUES (extract(hour from NEW.created_at),
                extract(day from NEW.created_at),
                extract(week from NEW.created_at),
                extract(month from NEW.created_at),
                extract(year from NEW.created_at),
                user_id,
                NEW.stream_id,
                stream_name,
                offer_id,
                offer_name,
                NEW.geo_id,
                geo_iso_2,
                NEW.id,
                now());
    ELSIF TG_TABLE_NAME = 'leads' THEN
        UPDATE statistics
        SET lead_id     = NEW.id,
            lead_status = NEW.status,
            lead_price  = NEW.price,
            updated_at  = now()
        WHERE statistics.click_id = NEW.click_id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sync_token_statistic() RETURNS TRIGGER AS
$$
DECLARE
    token      int;
    click      int;
    token_name varchar;
BEGIN
    SELECT td.id, td.name, ctd.click_id
    INTO token, token_name, click
    FROM click_token_dictionary ctd
             JOIN token_dictionary td ON td.id = ctd.token_dictionary_id
    WHERE ctd.token_dictionary_id = NEW.token_dictionary_id
    LIMIT 1;

    IF token_name = 'token_1' THEN
        UPDATE statistics SET token1_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_2' THEN
        UPDATE statistics SET token2_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_3' THEN
        UPDATE statistics SET token3_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_4' THEN
        UPDATE statistics SET token4_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_5' THEN
        UPDATE statistics SET token5_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_6' THEN
        UPDATE statistics SET token6_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_7' THEN
        UPDATE statistics SET token7_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_8' THEN
        UPDATE statistics SET token8_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_9' THEN
        UPDATE statistics SET token9_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_10' THEN
        UPDATE statistics SET token10_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_11' THEN
        UPDATE statistics SET token11_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_12' THEN
        UPDATE statistics SET token12_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_13' THEN
        UPDATE statistics SET token13_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_14' THEN
        UPDATE statistics SET token14_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_15' THEN
        UPDATE statistics SET token15_id = token WHERE click_id = click;
    END IF;

    IF token_name = 'token_16' THEN
        UPDATE statistics SET token16_id = token WHERE click_id = click;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER t_clicks
    AFTER INSERT
    ON clicks
    FOR EACH ROW
EXECUTE PROCEDURE sync_statistic();

CREATE TRIGGER t_clicks
    AFTER INSERT OR UPDATE
    ON leads
    FOR EACH ROW
EXECUTE PROCEDURE sync_statistic();

CREATE TRIGGER t_clicks
    AFTER INSERT
    ON click_token_dictionary
    FOR EACH ROW
EXECUTE PROCEDURE sync_token_statistic();
