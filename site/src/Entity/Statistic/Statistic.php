<?php

namespace App\Entity\Statistic;

use App\Repository\Main\StreamRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="statistics", options={"comment":"Статистика"})
 * @ORM\Entity(repositoryClass=StreamRepository::class)
 */
class Statistic
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"comment":"ID клика"})
     */
    private ?int $clickId = null;

    /**
     * @ORM\Column(type="datetime", options={"comment":"Дата создания клика"})
     */
    private ?DateTimeInterface $click_created_at = null;

    /**
     * @ORM\Column(type="integer", options={"comment":"ID пользователя"})
     */
    private ?int $userId = null;

    /**
     * @ORM\Column(type="integer", options={"comment":"ID потока"})
     */
    private ?int $streamId = null;

    /**
     * @ORM\Column(type="integer", options={"comment":"ID источника трафика"})
     */
    private ?int $trafficSourceId = null;

    /**
     * @ORM\Column(type="integer", options={"comment":"ID оффера"})
     */
    private ?int $offerId = null;

    /**
     * @ORM\Column(type="integer", options={"comment":"ID гео"})
     */
    private ?int $geoId = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID лида"})
     */
    private ?int $leadId = null;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"comment":"Cтатус лида"})
     */
    private ?int $leadStatus = null;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true, options={"comment":"Стоимость лида"})
     */
    private ?string $leadPrice = null;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"fixed" = true, "comment":"Iso код вылюты"})
     */
    private ?string $leadCurrencyCode = null;

    /**
     * @ORM\Column(type="decimal", precision=16, scale=10, nullable=true, options={"comment":"Курс вылюты лида"})
     */
    private ?string $leadCurrencyRate = null;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"comment":"Дата создания клика"})
     */
    private ?DateTimeInterface $lead_created_at = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Час"})
     */
    private ?int $hour = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"День"})
     */
    private ?int $day = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Неделя"})
     */
    private ?int $week = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Месяц"})
     */
    private ?int $month = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Год"})
     */
    private ?int $year = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 1"})
     */
    private ?int $token1Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 2"})
     */
    private ?int $token2Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 3"})
     */
    private ?int $token3Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 4"})
     */
    private ?int $token4Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 5"})
     */
    private ?int $token5Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 6"})
     */
    private ?int $token6Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 7"})
     */
    private ?int $token7Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 8"})
     */
    private ?int $token8Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 9"})
     */
    private ?int $token9Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 10"})
     */
    private ?int $token10Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 11"})
     */
    private ?int $token11Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 12"})
     */
    private ?int $token12Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 13"})
     */
    private ?int $token13Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 14"})
     */
    private ?int $token14Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 15"})
     */
    private ?int $token15Id = null;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ID токена 16"})
     */
    private ?int $token16Id = null;

    /**
     * @ORM\Column(type="date", options={"comment":"Дата добавления в статистику"})
     */
    private ?DateTimeInterface $date = null;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"comment":"Версия записи"})
     */
    private ?int $version = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->clickId;
    }

    /**
     * @return int
     */
    public function getClickId(): int
    {
        return $this->clickId;
    }

    /**
     * @param int $clickId
     */
    public function setClickId(int $clickId): void
    {
        $this->clickId = $clickId;
    }

    /**
     * @return DateTimeInterface
     */
    public function getClickCreatedAt(): DateTimeInterface
    {
        return $this->click_created_at;
    }

    /**
     * @param DateTimeInterface $click_created_at
     */
    public function setClickCreatedAt(DateTimeInterface $click_created_at): void
    {
        $this->click_created_at = $click_created_at;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getStreamId(): int
    {
        return $this->streamId;
    }

    /**
     * @param int $streamId
     */
    public function setStreamId(int $streamId): void
    {
        $this->streamId = $streamId;
    }

    /**
     * @return int
     */
    public function getOfferId(): int
    {
        return $this->offerId;
    }

    /**
     * @param int $offerId
     */
    public function setOfferId(int $offerId): void
    {
        $this->offerId = $offerId;
    }

    /**
     * @return int
     */
    public function getGeoId(): int
    {
        return $this->geoId;
    }

    /**
     * @param int $geoId
     */
    public function setGeoId(int $geoId): void
    {
        $this->geoId = $geoId;
    }

    /**
     * @return int|null
     */
    public function getLeadId(): ?int
    {
        return $this->leadId;
    }

    /**
     * @param int|null $leadId
     */
    public function setLeadId(?int $leadId): void
    {
        $this->leadId = $leadId;
    }

    /**
     * @return int|null
     */
    public function getLeadStatus(): ?int
    {
        return $this->leadStatus;
    }

    /**
     * @param int|null $leadStatus
     */
    public function setLeadStatus(?int $leadStatus): void
    {
        $this->leadStatus = $leadStatus;
    }

    /**
     * @return string|null
     */
    public function getLeadPrice(): ?string
    {
        return $this->leadPrice;
    }

    /**
     * @param string|null $leadPrice
     */
    public function setLeadPrice(?string $leadPrice): void
    {
        $this->leadPrice = $leadPrice;
    }

    /**
     * @return string|null
     */
    public function getLeadCurrencyCode(): ?string
    {
        return $this->leadCurrencyCode;
    }

    /**
     * @param string|null $leadCurrencyCode
     */
    public function setLeadCurrencyCode(?string $leadCurrencyCode): void
    {
        $this->leadCurrencyCode = $leadCurrencyCode;
    }

    /**
     * @return string|null
     */
    public function getLeadCurrencyRate(): ?string
    {
        return $this->leadCurrencyRate;
    }

    /**
     * @param string|null $leadCurrencyRate
     */
    public function setLeadCurrencyRate(?string $leadCurrencyRate): void
    {
        $this->leadCurrencyRate = $leadCurrencyRate;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLeadCreatedAt(): ?DateTimeInterface
    {
        return $this->lead_created_at;
    }

    /**
     * @param DateTimeInterface|null $lead_created_at
     */
    public function setLeadCreatedAt(?DateTimeInterface $lead_created_at): void
    {
        $this->lead_created_at = $lead_created_at;
    }

    /**
     * @return int
     */
    public function getHour(): int
    {
        return $this->hour;
    }

    /**
     * @param int $hour
     */
    public function setHour(int $hour): void
    {
        $this->hour = $hour;
    }

    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->day;
    }

    /**
     * @param int $day
     */
    public function setDay(int $day): void
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * @param int $week
     */
    public function setWeek(int $week): void
    {
        $this->week = $week;
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @param int $month
     */
    public function setMonth(int $month): void
    {
        $this->month = $month;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return int|null
     */
    public function getToken1Id(): ?int
    {
        return $this->token1Id;
    }

    /**
     * @param int|null $token1Id
     */
    public function setToken1Id(?int $token1Id): void
    {
        $this->token1Id = $token1Id;
    }

    /**
     * @return int|null
     */
    public function getToken2Id(): ?int
    {
        return $this->token2Id;
    }

    /**
     * @param int|null $token2Id
     */
    public function setToken2Id(?int $token2Id): void
    {
        $this->token2Id = $token2Id;
    }

    /**
     * @return int|null
     */
    public function getToken3Id(): ?int
    {
        return $this->token3Id;
    }

    /**
     * @param int|null $token3Id
     */
    public function setToken3Id(?int $token3Id): void
    {
        $this->token3Id = $token3Id;
    }

    /**
     * @return int|null
     */
    public function getToken4Id(): ?int
    {
        return $this->token4Id;
    }

    /**
     * @param int|null $token4Id
     */
    public function setToken4Id(?int $token4Id): void
    {
        $this->token4Id = $token4Id;
    }

    /**
     * @return int|null
     */
    public function getToken5Id(): ?int
    {
        return $this->token5Id;
    }

    /**
     * @param int|null $token5Id
     */
    public function setToken5Id(?int $token5Id): void
    {
        $this->token5Id = $token5Id;
    }

    /**
     * @return int|null
     */
    public function getToken6Id(): ?int
    {
        return $this->token6Id;
    }

    /**
     * @param int|null $token6Id
     */
    public function setToken6Id(?int $token6Id): void
    {
        $this->token6Id = $token6Id;
    }

    /**
     * @return int|null
     */
    public function getToken7Id(): ?int
    {
        return $this->token7Id;
    }

    /**
     * @param int|null $token7Id
     */
    public function setToken7Id(?int $token7Id): void
    {
        $this->token7Id = $token7Id;
    }

    /**
     * @return int|null
     */
    public function getToken8Id(): ?int
    {
        return $this->token8Id;
    }

    /**
     * @param int|null $token8Id
     */
    public function setToken8Id(?int $token8Id): void
    {
        $this->token8Id = $token8Id;
    }

    /**
     * @return int|null
     */
    public function getToken9Id(): ?int
    {
        return $this->token9Id;
    }

    /**
     * @param int|null $token9Id
     */
    public function setToken9Id(?int $token9Id): void
    {
        $this->token9Id = $token9Id;
    }

    /**
     * @return int|null
     */
    public function getToken10Id(): ?int
    {
        return $this->token10Id;
    }

    /**
     * @param int|null $token10Id
     */
    public function setToken10Id(?int $token10Id): void
    {
        $this->token10Id = $token10Id;
    }

    /**
     * @return int|null
     */
    public function getToken11Id(): ?int
    {
        return $this->token11Id;
    }

    /**
     * @param int|null $token11Id
     */
    public function setToken11Id(?int $token11Id): void
    {
        $this->token11Id = $token11Id;
    }

    /**
     * @return int|null
     */
    public function getToken12Id(): ?int
    {
        return $this->token12Id;
    }

    /**
     * @param int|null $token12Id
     */
    public function setToken12Id(?int $token12Id): void
    {
        $this->token12Id = $token12Id;
    }

    /**
     * @return int|null
     */
    public function getToken13Id(): ?int
    {
        return $this->token13Id;
    }

    /**
     * @param int|null $token13Id
     */
    public function setToken13Id(?int $token13Id): void
    {
        $this->token13Id = $token13Id;
    }

    /**
     * @return int|null
     */
    public function getToken14Id(): ?int
    {
        return $this->token14Id;
    }

    /**
     * @param int|null $token14Id
     */
    public function setToken14Id(?int $token14Id): void
    {
        $this->token14Id = $token14Id;
    }

    /**
     * @return int|null
     */
    public function getToken15Id(): ?int
    {
        return $this->token15Id;
    }

    /**
     * @param int|null $token15Id
     */
    public function setToken15Id(?int $token15Id): void
    {
        $this->token15Id = $token15Id;
    }

    /**
     * @return int|null
     */
    public function getToken16Id(): ?int
    {
        return $this->token16Id;
    }

    /**
     * @param int|null $token16Id
     */
    public function setToken16Id(?int $token16Id): void
    {
        $this->token16Id = $token16Id;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param DateTimeInterface|null $date
     */
    public function setDate(?DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return int|null
     */
    public function getTrafficSourceId(): ?int
    {
        return $this->trafficSourceId;
    }

    /**
     * @param int|null $trafficSourceId
     */
    public function setTrafficSourceId(?int $trafficSourceId): void
    {
        $this->trafficSourceId = $trafficSourceId;
    }
}
