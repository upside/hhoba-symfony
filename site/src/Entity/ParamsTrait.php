<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ParamsTrait
{
    /**
     * @ORM\Column(type="json", nullable=true, options={"comment":"Дополнительные параметры, настройки"})
     */
    private ?array $params = [];

    /**
     * @return array|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @param array|null $params
     * @return $this
     */
    public function setParams(?array $params): self
    {
        $this->params = $params;

        return $this;
    }
}
