<?php

namespace App\Entity;

use App\Entity\Main\IpAddressDictionary;

trait IpAddressDictionaryTrait
{
    /**
     * @return IpAddressDictionary|null
     */
    public function getIpAddress(): ?IpAddressDictionary
    {
        return $this->ipAddress;
    }

    /**
     * @param IpAddressDictionary|null $ipAddress
     * @return $this
     */
    public function setIpAddress(?IpAddressDictionary $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }
}
