<?php

namespace App\Entity;

use App\Entity\Main\Geo;

trait GeoTrait
{

    /**
     * @return Geo|null
     */
    public function getGeo(): ?Geo
    {
        return $this->geo;
    }

    /**
     * @param Geo $geo
     * @return $this
     */
    public function setGeo(Geo $geo): self
    {
        $this->geo = $geo;

        return $this;
    }
}
