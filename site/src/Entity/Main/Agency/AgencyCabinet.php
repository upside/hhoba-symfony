<?php

namespace App\Entity\Main\Agency;

use App\Entity\Main\User;
use App\Entity\Main\UserBalanceTransaction;
use App\Entity\ParamsTrait;
use App\Entity\TimestampTrait;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="agency_cabinets", options={"comment":"Кабинеты пользователей из рекламных сетей"})
 * @ORM\Entity(repositoryClass=AgencyCabinetRepository::class)
 */
class AgencyCabinet
{
    use ParamsTrait;
    use TimestampTrait;

    public const TYPE_MYTARGET = 'mytarget';

    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 2;
    public const STATUS_BLOCKED = 3;
    public const STATUS_HIDDEN = 4;

    public const STATUSES = [
        self::STATUS_ACTIVE => 'Активный',
        self::STATUS_DELETED => 'Удаленный',
        self::STATUS_BLOCKED => 'Заблокированный',
        self::STATUS_HIDDEN => 'Скрыт',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Кабинета"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"ID в системе рекламной сети"})
     */
    private ?string $apiId = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Статус кабинета"})
     */
    private int $status;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Тип рекламной сети"})
     */
    private ?string $type = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="agencyCabinets")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Название кабинета"})
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Название в системе рекламной сети"})
     */
    private ?string $apiName = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Пароль от кабинета"})
     */
    private ?string $password = null;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Баланс кабинета"})
     */
    private string $balance = '0';

    /**
     * @ORM\Column(type="boolean", options={"comment":"Признак включения автопополнения"})
     */
    private bool $autoCompletion = false;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Сумма автопополнения"})
     */
    private string $replenishmentAmount = '0';

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"comment":"Пополнять при балансе ниже"})
     */
    private string $balanceBelow = '0';

    /**
     * @ORM\OneToMany(targetEntity=UserBalanceTransaction::class, mappedBy="agencyCabinetFrom")
     */
    private Collection $userBalanceTransactionsFrom;

    /**
     * @ORM\OneToMany(targetEntity=UserBalanceTransaction::class, mappedBy="agencyCabinetTo")
     */
    private Collection $userBalanceTransactionsTo;

    public function __construct()
    {
        $this->userBalanceTransactionsFrom = new ArrayCollection();
        $this->userBalanceTransactionsTo = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName() . ' (' . $this->getApiName() . ')';
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBalance(): ?string
    {
        return $this->balance;
    }

    /**
     * @param string $balance
     * @return $this
     */
    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAutoCompletion(): ?bool
    {
        return $this->autoCompletion;
    }

    /**
     * @param bool $autoCompletion
     * @return $this
     */
    public function setAutoCompletion(bool $autoCompletion): self
    {
        $this->autoCompletion = $autoCompletion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReplenishmentAmount(): ?string
    {
        return $this->replenishmentAmount;
    }

    /**
     * @param string $replenishmentAmount
     * @return $this
     */
    public function setReplenishmentAmount(string $replenishmentAmount): self
    {
        $this->replenishmentAmount = $replenishmentAmount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBalanceBelow(): ?string
    {
        return $this->balanceBelow;
    }

    /**
     * @param string $balanceBelow
     * @return $this
     */
    public function setBalanceBelow(string $balanceBelow): self
    {
        $this->balanceBelow = $balanceBelow;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiId(): ?string
    {
        return $this->apiId;
    }

    /**
     * @param string|null $apiId
     */
    public function setApiId(?string $apiId): void
    {
        $this->apiId = $apiId;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getApiName(): ?string
    {
        return $this->apiName;
    }

    /**
     * @param string|null $apiName
     */
    public function setApiName(?string $apiName): void
    {
        $this->apiName = $apiName;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Collection|UserBalanceTransaction[]
     */
    public function getUserBalanceTransactionsFrom(): Collection
    {
        return $this->userBalanceTransactionsFrom;
    }

    public function addUserBalanceTransactionsFrom(UserBalanceTransaction $userBalanceTransactionsFrom): self
    {
        if (!$this->userBalanceTransactionsFrom->contains($userBalanceTransactionsFrom)) {
            $this->userBalanceTransactionsFrom[] = $userBalanceTransactionsFrom;
            $userBalanceTransactionsFrom->setAgencyCabinetFrom($this);
        }

        return $this;
    }

    public function removeUserBalanceTransactionsFrom(UserBalanceTransaction $userBalanceTransactionsFrom): self
    {
        if ($this->userBalanceTransactionsFrom->contains($userBalanceTransactionsFrom)) {
            $this->userBalanceTransactionsFrom->removeElement($userBalanceTransactionsFrom);
            // set the owning side to null (unless already changed)
            if ($userBalanceTransactionsFrom->getAgencyCabinetFrom() === $this) {
                $userBalanceTransactionsFrom->setAgencyCabinetFrom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserBalanceTransaction[]
     */
    public function getUserBalanceTransactionsTo(): Collection
    {
        return $this->userBalanceTransactionsTo;
    }

    public function addUserBalanceTransactionsTo(UserBalanceTransaction $userBalanceTransactionsTo): self
    {
        if (!$this->userBalanceTransactionsTo->contains($userBalanceTransactionsTo)) {
            $this->userBalanceTransactionsTo[] = $userBalanceTransactionsTo;
            $userBalanceTransactionsTo->setAgencyCabinetTo($this);
        }

        return $this;
    }

    public function removeUserBalanceTransactionsTo(UserBalanceTransaction $userBalanceTransactionsTo): self
    {
        if ($this->userBalanceTransactionsTo->contains($userBalanceTransactionsTo)) {
            $this->userBalanceTransactionsTo->removeElement($userBalanceTransactionsTo);
            // set the owning side to null (unless already changed)
            if ($userBalanceTransactionsTo->getAgencyCabinetTo() === $this) {
                $userBalanceTransactionsTo->setAgencyCabinetTo(null);
            }
        }

        return $this;
    }
}
