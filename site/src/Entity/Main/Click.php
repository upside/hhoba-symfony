<?php

namespace App\Entity\Main;

use App\Entity\GeoTrait;
use App\Entity\IpAddressDictionaryTrait;
use App\Entity\TimestampTrait;
use App\Entity\TokenDictionaryTrait;
use App\Entity\UserAgentDictionaryTrait;
use App\Entity\UuidTrait;
use App\Repository\Main\ClickRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="clicks", indexes={@ORM\Index(columns={"created_at"})}, options={"comment":"Клики"})
 * @ORM\Entity(repositoryClass=ClickRepository::class)
 */
class Click
{
    use UuidTrait;
    use TimestampTrait;
    use IpAddressDictionaryTrait;
    use TokenDictionaryTrait;
    use UserAgentDictionaryTrait;
    use GeoTrait;

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID клика"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Stream::class, inversedBy="clicks")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Stream $stream = null;

    /**
     * @ORM\ManyToOne(targetEntity=Geo::class, inversedBy="clicks")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Geo $geo = null;

    /**
     * @ORM\OneToOne(targetEntity=Lead::class, mappedBy="click", cascade={"persist", "remove"})
     */
    private ?Lead $lead = null;

    /**
     * @ORM\ManyToOne(targetEntity=UserAgentDictionary::class, inversedBy="clicks")
     */
    private ?UserAgentDictionary $userAgent = null;

    /**
     * @ORM\ManyToOne(targetEntity=IpAddressDictionary::class, inversedBy="clicks")
     */
    private ?IpAddressDictionary $ipAddress = null;

    /**
     * @ORM\ManyToMany(targetEntity=TokenDictionary::class, inversedBy="clicks")
     */
    private Collection $tokens;

    public function __construct()
    {
        $this->tokens = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Stream|null
     */
    public function getStream(): ?Stream
    {
        return $this->stream;
    }

    /**
     * @param Stream|null $stream
     * @return $this
     */
    public function setStream(?Stream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    /**
     * @return Lead|null
     */
    public function getLead(): ?Lead
    {
        return $this->lead;
    }

    /**
     * @param Lead|null $lead
     * @return $this
     */
    public function setLead(?Lead $lead): self
    {
        $this->lead = $lead;

        // set (or unset) the owning side of the relation if necessary
        $newClick = null === $lead ? null : $this;
        if ($lead->getClick() !== $newClick) {
            $lead->setClick($newClick);
        }

        return $this;
    }
}
