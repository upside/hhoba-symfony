<?php

namespace App\Entity\Main;

use App\Entity\TimestampTrait;
use App\Repository\Main\TrafficSourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrafficSourceRepository::class)
 */
class TrafficSource
{
    use TimestampTrait;

    public const MODERATION_PENDING = 'pending';
    public const MODERATION_IN_WORK = 'in_work';
    public const MODERATION_APPROVED = 'approved';
    public const MODERATION_DECLINED = 'declined';

    public const MODERATION_STATUSES = [
        self::MODERATION_PENDING => 'Ожидает',
        self::MODERATION_IN_WORK => 'В работе',
        self::MODERATION_APPROVED => 'Подтверждено',
        self::MODERATION_DECLINED => 'Отклонено',
    ];

    public const SOURCE_TYPE_WEBSITE = 1;
    public const  SOURCE_TYPE_DOORWAY = 2;
    public const  SOURCE_TYPE_CONTEXT = 4;
    public const  SOURCE_TYPE_TIZER = 8;
    public const  SOURCE_TYPE_BANNER = 16;
    public const  SOURCE_TYPE_POPUNDER = 32;
    public const  SOURCE_TYPE_SOCIAL = 64;
    public const  SOURCE_TYPE_SOCIAL_GROUP = 128;
    public const  SOURCE_TYPE_SOCIAL_APP = 256;
    public const  SOURCE_TYPE_EMAIL_SPAM = 512;
    public const  SOURCE_TYPE_MOTIVATED = 1024;
    public const  SOURCE_TYPE_MOBILE = 2048;
    public const  SOURCE_TYPE_OTHER = 4096;

    public const SOURCE_TYPES = [
        self::SOURCE_TYPE_WEBSITE => 'Собственный ресурс',
        self::SOURCE_TYPE_DOORWAY => 'Дорвеи',
        self::SOURCE_TYPE_CONTEXT => 'Контекстная реклама',
        self::SOURCE_TYPE_TIZER => 'Тизерные сети',
        self::SOURCE_TYPE_BANNER => 'Баннерные сети',
        self::SOURCE_TYPE_POPUNDER => 'Попандерные сети',
        self::SOURCE_TYPE_SOCIAL => 'Соц. сети',
        self::SOURCE_TYPE_SOCIAL_GROUP => 'Группы соц. сетей',
        self::SOURCE_TYPE_SOCIAL_APP => 'Приложения соц. сетей',
        self::SOURCE_TYPE_EMAIL_SPAM => 'E-mail рассылка',
        self::SOURCE_TYPE_MOTIVATED => 'Мотивированый трафик',
        self::SOURCE_TYPE_MOBILE => 'Мобильный трафик',
        self::SOURCE_TYPE_OTHER => 'Другое'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Источника траффика"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="trafficSources")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user = null;

    /**
     * @ORM\Column(type="string", options={"comment":"Статус модерации"})
     */
    private ?string $moderation = null;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Название источника"})
     */
    private ?string $title = null;

    /**
     * @ORM\Column(type="integer", options={"comment":"Тип источника"})
     */
    private ?int $sourceType = null;

    /**
     * @ORM\OneToMany(targetEntity=Stream::class, mappedBy="trafficSource")
     */
    private Collection $streams;

    public function __construct()
    {
        $this->streams = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getModeration(): ?string
    {
        return $this->moderation;
    }

    /**
     * @param string $moderation
     * @return $this
     */
    public function setModeration(string $moderation): self
    {
        $this->moderation = $moderation;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSourceType(): ?int
    {
        return $this->sourceType;
    }

    /**
     * @param int $sourceType
     * @return $this
     */
    public function setSourceType(int $sourceType): self
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Collection|Lead[]
     */
    public function getStreams(): Collection
    {
        return $this->streams;
    }

    public function addStreams(Stream $stream): self
    {
        if (!$this->streams->contains($stream)) {
            $this->streams[] = $stream;
            $stream->setTrafficSource($this);
        }

        return $this;
    }

    public function removeStreams(Stream $stream): self
    {
        if ($this->streams->contains($stream)) {
            $this->streams->removeElement($stream);
            // set the owning side to null (unless already changed)
            if ($stream->getTrafficSource() === $this) {
                $stream->setTrafficSource(null);
            }
        }

        return $this;
    }
}
