<?php

namespace App\Entity\Main;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\ParamsTrait;
use App\Entity\TimestampTrait;
use App\Repository\Main\UserRepository;
use App\Security\RolesEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="users", options={"comment":"Пользователи"})
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    use TimestampTrait;
    use ParamsTrait;

    public const SYSTEM_USER_MYTARGET = 'mytarget@mytarget.ru';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Пользователя"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=180)
     */
    private ?string $telegram = null;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private ?string $password = null;

    /**
     * @ORM\OneToMany(targetEntity=TrafficSource::class, mappedBy="user")
     */
    private Collection $trafficSources;

    /**
     * @ORM\OneToMany(targetEntity=Tariff::class, mappedBy="user")
     */
    private Collection $tariffs;

    /**
     * @ORM\OneToMany(targetEntity=AgencyCabinet::class, mappedBy="user")
     */
    private Collection $agencyCabinets;

    /**
     * @ORM\OneToMany(targetEntity=UserBalance::class, mappedBy="user")
     */
    private Collection $userBalances;

    public function __construct()
    {
        $this->trafficSources = new ArrayCollection();
        $this->tariffs = new ArrayCollection();
        $this->agencyCabinets = new ArrayCollection();
        $this->userBalances = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    /**
     * @param string|null $telegram
     */
    public function setTelegram(?string $telegram): void
    {
        $this->telegram = $telegram;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = RolesEnum::USER;

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Stream[]
     */
    public function getTrafficSources(): Collection
    {
        return $this->trafficSources;
    }

    /**
     * @param TrafficSource $trafficSource
     * @return $this
     */
    public function addTrafficSource(TrafficSource $trafficSource): self
    {
        if (!$this->trafficSources->contains($trafficSource)) {
            $this->trafficSources[] = $trafficSource;
            $trafficSource->setUser($this);
        }

        return $this;
    }

    /**
     * @param TrafficSource $trafficSource
     * @return $this
     */
    public function removeStream(TrafficSource $trafficSource): self
    {
        if ($this->trafficSources->contains($trafficSource)) {
            $this->trafficSources->removeElement($trafficSource);
            // set the owning side to null (unless already changed)
            if ($trafficSource->getUser() === $this) {
                $trafficSource->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tariff[]
     */
    public function getTariffs(): Collection
    {
        return $this->tariffs;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function addTariff(Tariff $tariff): self
    {
        if (!$this->tariffs->contains($tariff)) {
            $this->tariffs[] = $tariff;
            $tariff->setUser($this);
        }

        return $this;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function removeTariff(Tariff $tariff): self
    {
        if ($this->tariffs->contains($tariff)) {
            $this->tariffs->removeElement($tariff);
            // set the owning side to null (unless already changed)
            if ($tariff->getUser() === $this) {
                $tariff->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgencyCabinet[]
     */
    public function getAgencyCabinets(): Collection
    {
        return $this->agencyCabinets;
    }

    public function addAgencyCabinet(AgencyCabinet $agencyCabinet): self
    {
        if (!$this->agencyCabinets->contains($agencyCabinet)) {
            $this->agencyCabinets[] = $agencyCabinet;
            $agencyCabinet->setUser($this);
        }

        return $this;
    }

    public function removeAgencyCabinet(AgencyCabinet $agencyCabinet): self
    {
        if ($this->agencyCabinets->contains($agencyCabinet)) {
            $this->agencyCabinets->removeElement($agencyCabinet);
            // set the owning side to null (unless already changed)
            if ($agencyCabinet->getUser() === $this) {
                $agencyCabinet->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserBalance[]
     */
    public function getUserBalances(): Collection
    {
        return $this->userBalances;
    }

    public function addUserBalance(UserBalance $userBalance): self
    {
        if (!$this->userBalances->contains($userBalance)) {
            $this->userBalances[] = $userBalance;
            $userBalance->setUser($this);
        }

        return $this;
    }

    public function removeUserBalance(UserBalance $userBalance): self
    {
        if ($this->userBalances->contains($userBalance)) {
            $this->userBalances->removeElement($userBalance);
            // set the owning side to null (unless already changed)
            if ($userBalance->getUser() === $this) {
                $userBalance->setUser(null);
            }
        }

        return $this;
    }
}
