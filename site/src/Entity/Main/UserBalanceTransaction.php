<?php

namespace App\Entity\Main;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\ParamsTrait;
use App\Entity\TimestampTrait;
use App\Repository\Main\UserBalanceTransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_balance_transactions", options={"comment":"Транзакции балансов"})
 * @ORM\Entity(repositoryClass=UserBalanceTransactionRepository::class)
 */
class UserBalanceTransaction
{
    use ParamsTrait;
    use TimestampTrait;

    public const STATUS_NEW = 1;
    public const STATUS_SUCCESS = 2;
    public const STATUS_ERROR = 3;
    public const STATUS_PROCESS = 4;

    public const STATUSES = [
        self::STATUS_NEW => 'Новая',
        self::STATUS_SUCCESS => 'Завершена успешно',
        self::STATUS_ERROR => 'Завершена с ошибкой',
        self::STATUS_PROCESS => 'В процессе',
    ];

    public const OPERATION_TO_USER = 1;
    public const OPERATION_TO_MT_CABINET = 2;
    public const OPERATION_FROM_MT_CABINET_TO_MT_CABINET = 3;
    public const OPERATION_DEPOSIT = 4;

    public const OPERATIONS = [
        self::OPERATION_TO_USER => 'Перевод пользователю',
        self::OPERATION_TO_MT_CABINET => 'Пополнение кабинета MyTarget',
        self::OPERATION_FROM_MT_CABINET_TO_MT_CABINET => 'Перевод из кабинета в кабинет MyTarget',
        self::OPERATION_DEPOSIT => 'Пополнение баланса',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=UserBalance::class, inversedBy="userBalanceTransactionsFrom")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?UserBalance $userBalanceFrom = null;

    /**
     * @ORM\ManyToOne(targetEntity=UserBalance::class, inversedBy="userBalanceTransactionsTo")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?UserBalance $userBalanceTo = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $operation = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $status = self::STATUS_NEW;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private string $amount = '0';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\ManyToOne(targetEntity=AgencyCabinet::class, inversedBy="userBalanceTransactionsFrom")
     */
    private ?AgencyCabinet $agencyCabinetFrom = null;

    /**
     * @ORM\ManyToOne(targetEntity=AgencyCabinet::class, inversedBy="userBalanceTransactionsTo")
     */
    private ?AgencyCabinet $agencyCabinetTo = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return UserBalance|null
     */
    public function getUserBalanceFrom(): ?UserBalance
    {
        return $this->userBalanceFrom;
    }

    /**
     * @param UserBalance|null $userBalanceFrom
     * @return $this
     */
    public function setUserBalanceFrom(?UserBalance $userBalanceFrom): self
    {
        $this->userBalanceFrom = $userBalanceFrom;

        return $this;
    }

    /**
     * @return UserBalance|null
     */
    public function getUserBalanceTo(): ?UserBalance
    {
        return $this->userBalanceTo;
    }

    /**
     * @param UserBalance|null $userBalanceTo
     * @return $this
     */
    public function setUserBalanceTo(?UserBalance $userBalanceTo): self
    {
        $this->userBalanceTo = $userBalanceTo;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOperation(): ?int
    {
        return $this->operation;
    }

    /**
     * @param int $operation
     * @return $this
     */
    public function setOperation(int $operation): self
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return $this
     */
    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAgencyCabinetFrom(): ?AgencyCabinet
    {
        return $this->agencyCabinetFrom;
    }

    public function setAgencyCabinetFrom(?AgencyCabinet $agencyCabinetFrom): self
    {
        $this->agencyCabinetFrom = $agencyCabinetFrom;

        return $this;
    }

    public function getAgencyCabinetTo(): ?AgencyCabinet
    {
        return $this->agencyCabinetTo;
    }

    public function setAgencyCabinetTo(?AgencyCabinet $agencyCabinetTo): self
    {
        $this->agencyCabinetTo = $agencyCabinetTo;

        return $this;
    }
}
