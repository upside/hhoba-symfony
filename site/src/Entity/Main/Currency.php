<?php

namespace App\Entity\Main;

use App\Entity\TimestampTrait;
use App\Repository\Main\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="curencies", options={"comment":"Валюта"})
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    use TimestampTrait;

    public function __toString()
    {
        return $this->getCode();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Валюты"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Название на английском"})
     */
    private ?string $name_en = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Название на русском"})
     */
    private ?string $name_ru = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Сокращение на английском"})
     */
    private ?string $abbreviation_en = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Сокращение на русском"})
     */
    private ?string $abbreviation_ru = null;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"fixed" = true, "comment":"ISO код"})
     */
    private ?string $code = null;

    /**
     * @ORM\Column(type="decimal", precision=16, scale=10, nullable=true, options={"comment":"Курс"})
     */
    private ?string $rate = null;

    /**
     * @ORM\OneToMany(targetEntity=Tariff::class, mappedBy="currency")
     */
    private Collection $tariffs;

    /**
     * @ORM\OneToMany(targetEntity=Lead::class, mappedBy="currency")
     */
    private Collection $leads;

    /**
     * @ORM\OneToMany(targetEntity=UserBalance::class, mappedBy="currency")
     */
    private $userBalances;

    public function __construct()
    {
        $this->tariffs = new ArrayCollection();
        $this->leads = new ArrayCollection();
        $this->userBalances = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNameEn(): ?string
    {
        return $this->name_en;
    }

    /**
     * @param string|null $name_en
     * @return $this
     */
    public function setNameEn(?string $name_en): self
    {
        $this->name_en = $name_en;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNameRu(): ?string
    {
        return $this->name_ru;
    }

    /**
     * @param string|null $name_ru
     * @return $this
     */
    public function setNameRu(?string $name_ru): self
    {
        $this->name_ru = $name_ru;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAbbreviationEn(): ?string
    {
        return $this->abbreviation_en;
    }

    /**
     * @param string|null $abbreviation_en
     * @return $this
     */
    public function setAbbreviationEn(?string $abbreviation_en): self
    {
        $this->abbreviation_en = $abbreviation_en;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAbbreviationRu(): ?string
    {
        return $this->abbreviation_ru;
    }

    /**
     * @param string|null $abbreviation_ru
     * @return $this
     */
    public function setAbbreviationRu(?string $abbreviation_ru): self
    {
        $this->abbreviation_ru = $abbreviation_ru;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return $this
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRate(): ?string
    {
        return $this->rate;
    }

    /**
     * @param string|null $rate
     * @return $this
     */
    public function setRate(?string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @return Collection|Tariff[]
     */
    public function getTariffs(): Collection
    {
        return $this->tariffs;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function addTariff(Tariff $tariff): self
    {
        if (!$this->tariffs->contains($tariff)) {
            $this->tariffs[] = $tariff;
            $tariff->setCurrency($this);
        }

        return $this;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function removeTariff(Tariff $tariff): self
    {
        if ($this->tariffs->contains($tariff)) {
            $this->tariffs->removeElement($tariff);
            // set the owning side to null (unless already changed)
            if ($tariff->getCurrency() === $this) {
                $tariff->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lead[]
     */
    public function getLeads(): Collection
    {
        return $this->leads;
    }

    public function addLead(Lead $lead): self
    {
        if (!$this->leads->contains($lead)) {
            $this->leads[] = $lead;
            $lead->setCurrency($this);
        }

        return $this;
    }

    public function removeLead(Lead $lead): self
    {
        if ($this->leads->contains($lead)) {
            $this->leads->removeElement($lead);
            // set the owning side to null (unless already changed)
            if ($lead->getCurrency() === $this) {
                $lead->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserBalance[]
     */
    public function getUserBalances(): Collection
    {
        return $this->userBalances;
    }

    public function addUserBalance(UserBalance $userBalance): self
    {
        if (!$this->userBalances->contains($userBalance)) {
            $this->userBalances[] = $userBalance;
            $userBalance->setCurrency($this);
        }

        return $this;
    }

    public function removeUserBalance(UserBalance $userBalance): self
    {
        if ($this->userBalances->contains($userBalance)) {
            $this->userBalances->removeElement($userBalance);
            // set the owning side to null (unless already changed)
            if ($userBalance->getCurrency() === $this) {
                $userBalance->setCurrency(null);
            }
        }

        return $this;
    }
}
