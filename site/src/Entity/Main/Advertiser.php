<?php

namespace App\Entity\Main;

use App\Entity\ParamsTrait;
use App\Entity\TimestampTrait;
use App\Repository\Main\AdvertiserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="advertisers", options={"comment":"Рекламодатели"})
 * @ORM\Entity(repositoryClass=AdvertiserRepository::class)
 */
class Advertiser
{
    use TimestampTrait;
    use ParamsTrait;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const STATUSES = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'Inactive',
    ];

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Рекламодателя"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotBlank
     * @Assert\Choice(
     *     choices = {
     *          Advertiser::STATUS_INACTIVE,
     *          Advertiser::STATUS_ACTIVE
     *      },
     *     message="Choose a valid status."
     * )
     */
    private ?int $status = null;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private ?int $apiId = null;

    /**
     * @ORM\OneToMany(targetEntity=Offer::class, mappedBy="advertiser")
     */
    private Collection $offers;

    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getApiId(): ?int
    {
        return $this->apiId;
    }

    /**
     * @param int $apiId
     * @return $this
     */
    public function setApiId(int $apiId): self
    {
        $this->apiId = $apiId;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setAdvertiser($this);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getAdvertiser() === $this) {
                $offer->setAdvertiser(null);
            }
        }

        return $this;
    }
}
