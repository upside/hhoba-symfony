<?php

namespace App\Entity\Main;

use App\Entity\TimestampTrait;
use App\Repository\Main\TariffRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tariffs", options={"comment":"Тарифы"})
 * @ORM\Entity(repositoryClass=TariffRepository::class)
 */
class Tariff
{
    use TimestampTrait;

    public const TYPE_CPA = 1;
    public const TYPE_MAIN = 2;
    public const TYPE_USER = 3;

    public const TYPES = [
        self::TYPE_CPA => 'CPA',
        self::TYPE_MAIN => 'Общий',
        self::TYPE_USER => 'Пользовательский',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class, inversedBy="tariffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Offer $offer = null;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="tariffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Currency $currency = null;

    /**
     * @ORM\ManyToOne(targetEntity=Geo::class, inversedBy="tariffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Geo $geo = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tariffs")
     */
    private ?User $user = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Тип тарифа"})
     */
    private ?int $type = null;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, options={"comment":"Значение тарифа"})
     */
    private ?string $value = null;

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Offer|null
     */
    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer|null $offer
     * @return $this
     */
    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return Currency|null
     */
    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency|null $currency
     * @return $this
     */
    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Geo|null
     */
    public function getGeo(): ?Geo
    {
        return $this->geo;
    }

    /**
     * @param Geo|null $geo
     * @return $this
     */
    public function setGeo(?Geo $geo): self
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
