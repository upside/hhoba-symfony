<?php

namespace App\Entity\Main;

use App\Entity\TimestampTrait;
use App\Repository\Main\TokenDictionaryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="dictionary_token", options={"comment":"Словарь токенов"})
 * @ORM\Entity(repositoryClass=TokenDictionaryRepository::class)
 */
class TokenDictionary
{
    use TimestampTrait;

    public const TOKEN1_NAME = 'token_1';
    public const TOKEN2_NAME = 'token_2';
    public const TOKEN3_NAME = 'token_3';
    public const TOKEN4_NAME = 'token_4';
    public const TOKEN5_NAME = 'token_5';
    public const TOKEN6_NAME = 'token_6';
    public const TOKEN7_NAME = 'token_7';
    public const TOKEN8_NAME = 'token_8';
    public const TOKEN9_NAME = 'token_9';
    public const TOKEN10_NAME = 'token_10';
    public const TOKEN11_NAME = 'token_11';
    public const TOKEN12_NAME = 'token_12';
    public const TOKEN13_NAME = 'token_13';
    public const TOKEN14_NAME = 'token_14';
    public const TOKEN15_NAME = 'token_15';
    public const TOKEN16_NAME = 'token_16';

    public function __toString()
    {
        return $this->value;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Токена"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $value = null;

    /**
     * @ORM\ManyToMany(targetEntity=Click::class, mappedBy="tokens")
     */
    private Collection $clicks;

    /**
     * @ORM\ManyToMany(targetEntity=Lead::class, mappedBy="tokens")
     */
    private Collection $leads;


    public function __construct()
    {
        $this->clicks = new ArrayCollection();
        $this->leads = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|Click[]
     */
    public function getClicks(): Collection
    {
        return $this->clicks;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function addClick(Click $click): self
    {
        if (!$this->clicks->contains($click)) {
            $this->clicks[] = $click;
            $click->addToken($this);
        }

        return $this;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function removeClick(Click $click): self
    {
        if ($this->clicks->contains($click)) {
            $this->clicks->removeElement($click);
            $click->removeToken($this);
        }

        return $this;
    }

    /**
     * @return Collection|Lead[]
     */
    public function getLeads(): Collection
    {
        return $this->leads;
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function addLead(Lead $lead): self
    {
        if (!$this->leads->contains($lead)) {
            $this->leads[] = $lead;
            $lead->addToken($this);
        }

        return $this;
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function removeLead(Lead $lead): self
    {
        if ($this->leads->contains($lead)) {
            $this->leads->removeElement($lead);
            $lead->removeToken($this);
        }

        return $this;
    }
}
