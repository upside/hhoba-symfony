<?php

namespace App\Entity\Main;

use App\Entity\ParamsTrait;
use App\Entity\TimestampTrait;
use App\Repository\Main\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="offers", options={"comment":"Офферы"})
 * @ORM\Entity(repositoryClass=OfferRepository::class)
 */
class Offer
{
    use TimestampTrait;
    use ParamsTrait;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const STATUSES = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'Inactive',
    ];

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Оффера"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Advertiser::class, inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Advertiser $advertiser = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $status = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $image = null;

    /**
     * @ORM\OneToMany(targetEntity=Stream::class, mappedBy="offer")
     */
    private Collection $streams;

    /**
     * @ORM\OneToMany(targetEntity=Tariff::class, mappedBy="offer")
     */
    private Collection $tariffs;

    public function __construct()
    {
        $this->streams = new ArrayCollection();
        $this->tariffs = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Advertiser|null
     */
    public function getAdvertiser(): ?Advertiser
    {
        return $this->advertiser;
    }

    /**
     * @param Advertiser|null $advertiser
     * @return $this
     */
    public function setAdvertiser(?Advertiser $advertiser): self
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     * @return $this
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Stream[]
     */
    public function getStreams(): Collection
    {
        return $this->streams;
    }

    /**
     * @param Stream $stream
     * @return $this
     */
    public function addStream(Stream $stream): self
    {
        if (!$this->streams->contains($stream)) {
            $this->streams[] = $stream;
            $stream->setOffer($this);
        }

        return $this;
    }

    /**
     * @param Stream $stream
     * @return $this
     */
    public function removeStream(Stream $stream): self
    {
        if ($this->streams->contains($stream)) {
            $this->streams->removeElement($stream);
            // set the owning side to null (unless already changed)
            if ($stream->getOffer() === $this) {
                $stream->setOffer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tariff[]
     */
    public function getTariffs(): Collection
    {
        return $this->tariffs;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function addTariff(Tariff $tariff): self
    {
        if (!$this->tariffs->contains($tariff)) {
            $this->tariffs[] = $tariff;
            $tariff->setOffer($this);
        }

        return $this;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function removeTariff(Tariff $tariff): self
    {
        if ($this->tariffs->contains($tariff)) {
            $this->tariffs->removeElement($tariff);
            // set the owning side to null (unless already changed)
            if ($tariff->getOffer() === $this) {
                $tariff->setOffer(null);
            }
        }

        return $this;
    }
}
