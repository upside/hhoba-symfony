<?php

namespace App\Entity\Main;

use App\Entity\ParamsTrait;
use App\Entity\TimestampTrait;
use App\Entity\UuidTrait;
use App\Repository\Main\StreamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Table(name="streams", options={"comment":"Потоки"})
 * @ORM\Entity(repositoryClass=StreamRepository::class)
 */
class Stream
{
    use UuidTrait;
    use TimestampTrait;
    use ParamsTrait;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const STATUSES = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'Inactive',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", options={"comment":"ID Потока"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\ManyToOne(targetEntity=TrafficSource::class, inversedBy="streams")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?TrafficSource $trafficSource = null;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class, inversedBy="streams")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Offer $offer = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $status = null;

    /**
     * @ORM\OneToMany(targetEntity=Click::class, mappedBy="stream")
     */
    private Collection $clicks;

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
        $this->clicks = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return TrafficSource
     */
    public function getTrafficSource(): ?TrafficSource
    {
        return $this->trafficSource;
    }

    /**
     * @param TrafficSource $trafficSource
     * @return $this
     */
    public function setTrafficSource(TrafficSource $trafficSource)
    {
        $this->trafficSource = $trafficSource;

        return $this;
    }

    /**
     * @return Offer|null
     */
    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer|null $offer
     * @return $this
     */
    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Click[]
     */
    public function getClicks(): Collection
    {
        return $this->clicks;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function addClick(Click $click): self
    {
        if (!$this->clicks->contains($click)) {
            $this->clicks[] = $click;
            $click->setStream($this);
        }

        return $this;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function removeClick(Click $click): self
    {
        if ($this->clicks->contains($click)) {
            $this->clicks->removeElement($click);
            // set the owning side to null (unless already changed)
            if ($click->getStream() === $this) {
                $click->setStream(null);
            }
        }

        return $this;
    }
}
