<?php

namespace App\Entity\Main;

use App\Entity\GeoTrait;
use App\Entity\IpAddressDictionaryTrait;
use App\Entity\TimestampTrait;
use App\Entity\TokenDictionaryTrait;
use App\Entity\UserAgentDictionaryTrait;
use App\Entity\UuidTrait;
use App\Repository\Main\LeadRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="leads", indexes={@ORM\Index(columns={"created_at"})}, options={"comment":"Лиды"})
 * @ORM\Entity(repositoryClass=LeadRepository::class)
 */
class Lead
{
    use UuidTrait;
    use TimestampTrait;
    use IpAddressDictionaryTrait;
    use TokenDictionaryTrait;
    use UserAgentDictionaryTrait;
    use GeoTrait;

    public const STATUS_NEW = 1;
    public const STATUS_CONFIRM = 2;
    public const STATUS_DECLINE = 3;
    public const STATUS_TRASH = 4;

    public const STATUSES = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_CONFIRM => 'Подтверждён',
        self::STATUS_DECLINE => 'Откланён',
        self::STATUS_TRASH => 'Трэш',
    ];

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", options={"comment":"ID Лида"})
     */
    private ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity=Click::class, inversedBy="lead", cascade={"persist", "remove"})
     */
    private ?Click $click = null;

    /**
     * @ORM\ManyToOne(targetEntity=Geo::class, inversedBy="leads")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Geo $geo = null;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="leads")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Currency $currency = null;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, length=255, options={"comment":"Стоимость лида"})
     */
    private ?string $price = null;

    /**
     * @ORM\Column(type="smallint", options={"comment":"Статус"})
     */
    private int $status;

    /**
     * @ORM\Column(type="string", length=255, options={"comment":"Телефон клиента"})
     */
    private ?string $phone = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Имя клиента"})
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="text", nullable=true, options={"comment":"Комментарий к заказу"})
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"ID лида от API рекламодателя"})
     */
    private ?string $cpaId = null;

    /**
     * @ORM\ManyToOne(targetEntity=Geo::class)
     */
    private ?Geo $cpaGeo = null;

    /**
     * @ORM\ManyToOne(targetEntity=UserAgentDictionary::class, inversedBy="leads")
     */
    private ?UserAgentDictionary $userAgent = null;

    /**
     * @ORM\ManyToOne(targetEntity=IpAddressDictionary::class, inversedBy="leads")
     */
    private ?IpAddressDictionary $ipAddress = null;

    /**
     * @ORM\ManyToMany(targetEntity=TokenDictionary::class, inversedBy="leads")
     */
    private Collection $tokens;

    public function __construct()
    {
        $this->tokens = new ArrayCollection();
        $this->status = self::STATUS_NEW;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Click|null
     */
    public function getClick(): ?Click
    {
        return $this->click;
    }

    /**
     * @param Click|null $click
     * @return $this
     */
    public function setClick(?Click $click): self
    {
        $this->click = $click;

        return $this;
    }

    /**
     * @return Currency|null
     */
    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency|null $currency
     * @return $this
     */
    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     */
    public function setPrice(?string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * TODO: Возможно для $context ненужно null
     * @param int $status
     * @param array|null $context
     * @return $this
     */
    public function setStatus(int $status, ?array $context = []): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone(string $phone): self
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCpaId(): ?string
    {
        return $this->cpaId;
    }

    /**
     * @param string|null $cpaId
     * @return $this
     */
    public function setCpaId(?string $cpaId): self
    {
        $this->cpaId = $cpaId;

        return $this;
    }

    /**
     * @return Geo|null
     */
    public function getCpaGeo(): ?Geo
    {
        return $this->cpaGeo;
    }

    /**
     * @param Geo|null $cpaGeo
     * @return $this
     */
    public function setCpaGeo(?Geo $cpaGeo): self
    {
        $this->cpaGeo = $cpaGeo;

        return $this;
    }
}
