<?php

namespace App\Entity\Main;

use App\Repository\Main\UserAgentDictionaryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="dictionary_user_agent", options={"comment":"Словарь user agent"})
 * @ORM\Entity(repositoryClass=UserAgentDictionaryRepository::class)
 */
class UserAgentDictionary
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $value = null;

    /**
     * @ORM\OneToMany(targetEntity=Click::class, mappedBy="userAgent")
     */
    private Collection $clicks;

    /**
     * @ORM\OneToMany(targetEntity=Lead::class, mappedBy="userAgent")
     */
    private Collection $leads;

    public function __construct()
    {
        $this->clicks = new ArrayCollection();
        $this->leads = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|Click[]
     */
    public function getClicks(): Collection
    {
        return $this->clicks;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function addClick(Click $click): self
    {
        if (!$this->clicks->contains($click)) {
            $this->clicks[] = $click;
            $click->setUserAgent($this);
        }

        return $this;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function removeClick(Click $click): self
    {
        if ($this->clicks->contains($click)) {
            $this->clicks->removeElement($click);
            // set the owning side to null (unless already changed)
            if ($click->getUserAgent() === $this) {
                $click->setUserAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lead[]
     */
    public function getLeads(): Collection
    {
        return $this->leads;
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function addLead(Lead $lead): self
    {
        if (!$this->leads->contains($lead)) {
            $this->leads[] = $lead;
            $lead->setUserAgent($this);
        }

        return $this;
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function removeLead(Lead $lead): self
    {
        if ($this->leads->contains($lead)) {
            $this->leads->removeElement($lead);
            // set the owning side to null (unless already changed)
            if ($lead->getUserAgent() === $this) {
                $lead->setUserAgent(null);
            }
        }

        return $this;
    }
}
