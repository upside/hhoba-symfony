<?php

namespace App\Entity\Main;

use App\Entity\TimestampTrait;
use App\Repository\Main\GeoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="geo", options={"comment":"Гео"})
 * @ORM\Entity(repositoryClass=GeoRepository::class)
 */
class Geo
{
    public const NOT_DEFINITELY_ISO_2 = '00';

    use TimestampTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"comment":"ID Гео"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Название на английском"})
     */
    private ?string $name_en = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment":"Название на русском"})
     */
    private ?string $name_ru = null;

    /**
     * @ORM\Column(type="string", length=2, nullable=true, options={"fixed" = true, "comment":"ISO код 2"})
     */
    private ?string $iso_2 = null;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"fixed" = true, "comment":"ISO код 3"})
     */
    private ?string $iso_3 = null;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"fixed" = true, "comment":"ISO код числовой"})
     */
    private ?string $iso_num = null;

    /**
     * @ORM\OneToMany(targetEntity=Click::class, mappedBy="geo")
     */
    private Collection $clicks;

    /**
     * @ORM\OneToMany(targetEntity=Lead::class, mappedBy="geo")
     */
    private Collection $leads;

    /**
     * @ORM\OneToMany(targetEntity=Tariff::class, mappedBy="geo")
     */
    private Collection $tariffs;

    public function __construct()
    {
        $this->clicks = new ArrayCollection();
        $this->leads = new ArrayCollection();
        $this->tariffs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNameRu();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNameEn(): ?string
    {
        return $this->name_en;
    }

    /**
     * @param string|null $name_en
     * @return $this
     */
    public function setNameEn(?string $name_en): self
    {
        $this->name_en = $name_en;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNameRu(): ?string
    {
        return $this->name_ru;
    }

    /**
     * @param string|null $name_ru
     * @return $this
     */
    public function setNameRu(?string $name_ru): self
    {
        $this->name_ru = $name_ru;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIso2(): ?string
    {
        return $this->iso_2;
    }

    /**
     * @param string|null $iso_2
     * @return $this
     */
    public function setIso2(?string $iso_2): self
    {
        $this->iso_2 = $iso_2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIso3(): ?string
    {
        return $this->iso_3;
    }

    /**
     * @param string|null $iso_3
     * @return $this
     */
    public function setIso3(?string $iso_3): self
    {
        $this->iso_3 = $iso_3;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIsoNum(): ?string
    {
        return $this->iso_num;
    }

    /**
     * @param string|null $iso_num
     * @return $this
     */
    public function setIsoNum(?string $iso_num): self
    {
        $this->iso_num = $iso_num;

        return $this;
    }

    /**
     * @return Collection|Click[]
     */
    public function getClicks(): Collection
    {
        return $this->clicks;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function addClick(Click $click): self
    {
        if (!$this->clicks->contains($click)) {
            $this->clicks[] = $click;
            $click->setGeo($this);
        }

        return $this;
    }

    /**
     * @param Click $click
     * @return $this
     */
    public function removeClick(Click $click): self
    {
        if ($this->clicks->contains($click)) {
            $this->clicks->removeElement($click);
            // set the owning side to null (unless already changed)
            if ($click->getGeo() === $this) {
                $click->setGeo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lead[]
     */
    public function getLeads(): Collection
    {
        return $this->leads;
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function addLead(Lead $lead): self
    {
        if (!$this->leads->contains($lead)) {
            $this->leads[] = $lead;
            $lead->setGeo($this);
        }

        return $this;
    }

    /**
     * @param Lead $lead
     * @return $this
     */
    public function removeLead(Lead $lead): self
    {
        if ($this->leads->contains($lead)) {
            $this->leads->removeElement($lead);
            // set the owning side to null (unless already changed)
            if ($lead->getGeo() === $this) {
                $lead->setGeo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tariff[]
     */
    public function getTariffs(): Collection
    {
        return $this->tariffs;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function addTariff(Tariff $tariff): self
    {
        if (!$this->tariffs->contains($tariff)) {
            $this->tariffs[] = $tariff;
            $tariff->setGeo($this);
        }

        return $this;
    }

    /**
     * @param Tariff $tariff
     * @return $this
     */
    public function removeTariff(Tariff $tariff): self
    {
        if ($this->tariffs->contains($tariff)) {
            $this->tariffs->removeElement($tariff);
            // set the owning side to null (unless already changed)
            if ($tariff->getGeo() === $this) {
                $tariff->setGeo(null);
            }
        }

        return $this;
    }
}
