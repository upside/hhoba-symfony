<?php

namespace App\Entity\Main;

use App\Entity\TimestampTrait;
use App\Repository\Main\UserBalanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserBalanceRepository::class)
 */
class UserBalance
{
    use TimestampTrait;

    public const TYPE_AGENCY_USD = 1;

    public const TYPES = [
        self::TYPE_AGENCY_USD => 'Баланс рекламных сетей'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userBalances")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user = null;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="userBalances")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Currency $currency = null;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private ?string $amount = '0';

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $type = null;

    /**
     * @ORM\OneToMany(targetEntity=UserBalanceTransaction::class, mappedBy="userBalanceFrom")
     */
    private Collection $userBalanceTransactionsFrom;

    /**
     * @ORM\OneToMany(targetEntity=UserBalanceTransaction::class, mappedBy="userBalanceTo")
     */
    private Collection $userBalanceTransactionsTo;

    public function __construct()
    {
        $this->userBalanceTransactionsFrom = new ArrayCollection();
        $this->userBalanceTransactionsTo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|UserBalanceTransaction[]
     */
    public function getUserBalanceTransactionsFrom(): Collection
    {
        return $this->userBalanceTransactionsFrom;
    }

    public function addUserBalanceTransactionFrom(UserBalanceTransaction $userBalanceTransactionFrom): self
    {
        if (!$this->userBalanceTransactionsFrom->contains($userBalanceTransactionFrom)) {
            $this->userBalanceTransactionsFrom[] = $userBalanceTransactionFrom;
            $userBalanceTransactionFrom->setUserBalance($this);
        }

        return $this;
    }

    public function removeUserBalanceTransactionFrom(UserBalanceTransaction $userBalanceTransactionFrom): self
    {
        if ($this->userBalanceTransactionsFrom->contains($userBalanceTransactionFrom)) {
            $this->userBalanceTransactionsFrom->removeElement($userBalanceTransactionFrom);
            // set the owning side to null (unless already changed)
            if ($userBalanceTransactionFrom->getUserBalance() === $this) {
                $userBalanceTransactionFrom->setUserBalance(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserBalanceTransaction[]
     */
    public function getUserBalanceTransactionsTo(): Collection
    {
        return $this->userBalanceTransactionsTo;
    }

    public function addUserBalanceTransactionTo(UserBalanceTransaction $userBalanceTransactionTo): self
    {
        if (!$this->userBalanceTransactionsTo->contains($userBalanceTransactionTo)) {
            $this->userBalanceTransactionsTo[] = $userBalanceTransactionTo;
            $userBalanceTransactionTo->setUserBalance($this);
        }

        return $this;
    }

    public function removeUserBalanceTransactionTo(UserBalanceTransaction $userBalanceTransactionTo): self
    {
        if ($this->userBalanceTransactionsTo->contains($userBalanceTransactionTo)) {
            $this->userBalanceTransactionsTo->removeElement($userBalanceTransactionTo);
            // set the owning side to null (unless already changed)
            if ($userBalanceTransactionTo->getUserBalance() === $this) {
                $userBalanceTransactionTo->setUserBalance(null);
            }
        }

        return $this;
    }
}
