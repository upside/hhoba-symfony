<?php

namespace App\Entity;

use App\Entity\Main\TokenDictionary;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;

trait TokenDictionaryTrait
{

    /**
     * @return Collection
     */
    public function getTokens(): Collection
    {
        return $this->tokens;
    }

    /**
     * @param string $name
     * @return TokenDictionary|null
     */
    public function getTokenByName(string $name): ?TokenDictionary
    {
        $criteria = new Criteria();
        $criteria->where(new Comparison('name', '=', $name));

        $token = $this->tokens->matching($criteria)->first();

        return $token ? $token : null;
    }

    /**
     * @param TokenDictionary $token
     * @return $this
     */
    public function addToken(TokenDictionary $token): self
    {
        if (!$this->tokens->contains($token)) {
            $this->tokens[] = $token;
        }

        return $this;
    }

    /**
     * @param TokenDictionary $token
     * @return $this
     */
    public function removeToken(TokenDictionary $token): self
    {
        if ($this->tokens->contains($token)) {
            $this->tokens->removeElement($token);
        }

        return $this;
    }
}
