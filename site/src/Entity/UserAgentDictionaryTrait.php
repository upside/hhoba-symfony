<?php

namespace App\Entity;

use App\Entity\Main\UserAgentDictionary;

trait UserAgentDictionaryTrait
{
    /**
     * @return UserAgentDictionary|null
     */
    public function getUserAgent(): ?UserAgentDictionary
    {
        return $this->userAgent;
    }

    /**
     * @param UserAgentDictionary|null $userAgent
     * @return $this
     */
    public function setUserAgent(?UserAgentDictionary $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }
}
