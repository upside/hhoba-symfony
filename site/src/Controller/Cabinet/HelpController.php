<?php

namespace App\Controller\Cabinet;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HelpController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/help", name="cabinet_help_")
 */
class HelpController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('cabinet/help/index.html.twig', [
            'controller_name' => 'HelpController',
        ]);
    }
}
