<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\User;
use App\Repository\Main\OfferRepository;
use Doctrine\ORM\Query\Expr\Join;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OfferController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/offer", name="cabinet_offer_")
 */
class OfferController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param OfferRepository $offerRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(OfferRepository $offerRepository, PaginatorInterface $paginator, int $page = 1)
    {
        /** @var User $user */
        $user = $this->getUser();

        $query = $offerRepository->createQueryBuilder('o')
            ->join('o.advertiser', 'a', Join::WITH, 'a.status=1')
            ->leftJoin('o.tariffs', 't', Join::WITH, 't.type in(2) OR t.user = :userId')
            ->leftJoin('t.geo', 'g')
            ->leftJoin('t.currency', 'c')
            ->where('o.status=1')
            ->setParameter(':userId', $user->getId())
            ->addSelect(['a', 't', 'g', 'c'])
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 20);

        return $this->render('cabinet/offer/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
