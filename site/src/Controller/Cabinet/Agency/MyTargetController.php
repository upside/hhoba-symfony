<?php

namespace App\Controller\Cabinet\Agency;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\User;
use App\Entity\Main\UserBalanceTransaction;
use App\Form\Cabinet\Agency\MyTargetCabinetToCabinetType;
use App\Form\Cabinet\Agency\MyTargetSpendFundsType;
use App\Helpers\DateTimeHelper;
use App\Notifier\FlashMessageTypes;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use App\Repository\Main\CurrencyRepository;
use App\Repository\Main\UserBalanceRepository;
use App\Repository\Main\UserRepository;
use App\Services\AgencyCabinetService;
use App\Services\UserBalanceTransactionService;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


/**
 * @Route("/cabinet/agency/service/mytarget", name="cabinet_agency_service_mytarget_")
 */
class MyTargetController extends AbstractController
{
    /**
     * @Route("/{page?}", name="index", requirements={"page"="\d+"}, defaults={"page"="1"})
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(AgencyCabinetRepository $agencyCabinetRepository, PaginatorInterface $paginator, int $page)
    {
        /** @var User $user */
        $user = $this->getUser();

        $pagination = $paginator->paginate($agencyCabinetRepository->getByUserAndType($user, AgencyCabinet::TYPE_MYTARGET), $page, 100);

        return $this->render('cabinet/agency/mytarget/index.html.twig', [
            'service' => AgencyCabinet::TYPE_MYTARGET,
            'pagination' => $pagination,
        ]);
    }


    /**
     * Редактирование названия кабинета
     *
     * @Route("/cabinet/edit", name="cabinet_edit")
     * @param Request $request
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @return RedirectResponse
     * @throws Exception
     */
    public function editCabinet(Request $request, AgencyCabinetRepository $agencyCabinetRepository)
    {
        $id = $request->request->get('id');
        $name = $request->request->get('name');

        $agencyCabinet = $agencyCabinetRepository->find($id);

        if ($this->getUser() !== $agencyCabinet->getUser() || is_null($agencyCabinet)) {
            throw new Exception('Кабинет ненайден');
        }

        $agencyCabinet->setName($name);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($agencyCabinet);
        $entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Настройка автопополнения
     *
     * @Route("/cabinet/auto-completion", name="cabinet_auto_completion_edit")
     * @param Request $request
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @return RedirectResponse
     * @throws Exception
     */
    public function editAutoCompletion(Request $request, AgencyCabinetRepository $agencyCabinetRepository)
    {
        $id = $request->request->get('id');
        $autoCompletion = $request->request->get('auto_completion');
        $replenishmentAmount = $request->request->get('replenishment_amount');
        $balanceBelow = $request->request->get('balance_below');

        $agencyCabinet = $agencyCabinetRepository->find($id);

        if ($this->getUser() !== $agencyCabinet->getUser() || is_null($agencyCabinet)) {
            throw new Exception('Кабинет ненайден');
        }

        $agencyCabinet->setAutoCompletion($autoCompletion === 'on');
        $agencyCabinet->setReplenishmentAmount($replenishmentAmount);
        $agencyCabinet->setBalanceBelow($balanceBelow);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($agencyCabinet);
        $entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Перевод денег из кабинета в кабинет
     *
     * @Route("/spendfunds/cabinet-to-cabinet/{fromCabinet}", name="spend_funds_cabinet_to_cabinet")
     * @IsGranted(App\Security\Voter\AgencyCabinetVoter::SPEND_FUNDS_CABINET_TO_CABINET, subject="fromCabinet")
     * @param Request $request
     * @param UserBalanceRepository $userBalanceRepository
     * @param CurrencyRepository $currencyRepository
     * @param UserBalanceTransactionService $balanceTransactionService
     * @param AgencyCabinet $fromCabinet
     * @param AgencyCabinetService $agencyCabinetService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function spendfundsCabinetToCabinet(
        Request $request,
        UserBalanceRepository $userBalanceRepository,
        CurrencyRepository $currencyRepository,
        UserBalanceTransactionService $balanceTransactionService,
        AgencyCabinet $fromCabinet,
        AgencyCabinetService $agencyCabinetService
    )
    {
        $agencyCabinetService->syncCabinet($fromCabinet);

        /** @var User $from */
        $from = $this->getUser();

        if ($fromCabinet->getUser()->getId() !== $from->getId()) {
            return $this->redirect($request->headers->get('referer'));
        }

        $form = $this->createForm(MyTargetCabinetToCabinetType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $currency = $currencyRepository->findOneBy(['code' => 'USD']);

            $userBalanceFrom = $userBalanceRepository->findOneBy([
                'user' => $from,
                'currency' => $currency,
            ]);

            $toCabinet = $form->get('cabinet_to')->getData();
            $amount = $form->get('amount')->getData();

            $balanceTransactionService->newTransaction(
                $userBalanceFrom,
                $userBalanceFrom,
                UserBalanceTransaction::OPERATION_FROM_MT_CABINET_TO_MT_CABINET,
                $amount,
                [],
                $fromCabinet,
                $toCabinet
            );

            return $this->redirectToRoute('cabinet_agency_service_mytarget_index');
        }

        return $this->render('cabinet/agency/mytarget/cabinet_to_cabinet.html.twig', [
            'form' => $form->createView(),
            'fromCabinet' => $fromCabinet,
        ]);
    }

    /**
     * Перевод денег в кабинет
     *
     * @Route("/spendfunds/to-cabinet/{cabinet}/", name="spend_funds_to_cabinet")
     * @IsGranted(App\Security\Voter\AgencyCabinetVoter::SPEND_FUNDS_TO_CABINET, subject="cabinet")
     * @param Request $request
     * @param UserBalanceRepository $userBalanceRepository
     * @param CurrencyRepository $currencyRepository
     * @param UserRepository $userRepository
     * @param AgencyCabinet $cabinet
     * @param UserBalanceTransactionService $balanceTransactionService
     * @param AgencyCabinetService $agencyCabinetService
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws InvalidArgumentException
     */
    public function spendFundsToCabinet(
        Request $request,
        UserBalanceRepository $userBalanceRepository,
        CurrencyRepository $currencyRepository,
        UserRepository $userRepository,
        AgencyCabinet $cabinet,
        UserBalanceTransactionService $balanceTransactionService,
        AgencyCabinetService $agencyCabinetService
    )
    {
        $agencyCabinetService->syncCabinet($cabinet);

        /** @var User $from */
        $from = $this->getUser();
        $to = $userRepository->findOneBy(['email' => 'mytarget@mytarget.ru']);

        $currency = $currencyRepository->findOneBy(['code' => 'USD']);

        $form = $this->createForm(MyTargetSpendFundsType::class);
        $form->handleRequest($request);

        $amount = $form->get('amount')->getData();

        $userBalanceFrom = $userBalanceRepository->findOneBy([
            'user' => $from,
            'currency' => $currency,
        ]);

        if ($form->isSubmitted() && $form->isValid()) {

            $userBalanceTo = $userBalanceRepository->findOneBy([
                'user' => $to,
                'currency' => $currency,
            ]);

            if (bccomp($userBalanceFrom->getAmount(), $amount, 2) >= 0) {
                $balanceTransactionService->newTransaction(
                    $userBalanceFrom,
                    $userBalanceTo,
                    UserBalanceTransaction::OPERATION_TO_MT_CABINET,
                    $amount,
                    [],
                    null,
                    $cabinet
                );
            } else {
                throw new Exception('Недостаточно средств на считу');
            }

            return $this->redirectToRoute('cabinet_agency_service_mytarget_index');
        }

        return $this->render('cabinet/agency/mytarget/spendfunds.html.twig', [
            'form' => $form->createView(),
            'userBalanceFrom' => $userBalanceFrom,
            'cabinet' => $cabinet,
        ]);

    }


    /**
     * @Route("/sync/cabinets", name="sync_cabinets")
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param AgencyCabinetService $agencyCabinetService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function syncCabinets(
        AgencyCabinetRepository $agencyCabinetRepository,
        AgencyCabinetService $agencyCabinetService
    )
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var AgencyCabinet[] $cabinets */
        $cabinets = $agencyCabinetRepository->getByUserAndType($user, AgencyCabinet::TYPE_MYTARGET)->getResult();

        $agencyCabinetService->syncCabinets(...$cabinets);

        $this->addFlash(FlashMessageTypes::SUCCESS, 'Все кабинеты обновлены!');

        return $this->redirectToRoute('cabinet_agency_service_mytarget_index');
    }

    /**
     * @Route("/get-cabinet", name="get_cabinet")
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param UserRepository $userRepository
     * @return RedirectResponse
     * @throws Exception
     */
    public function getCabinet(AgencyCabinetRepository $agencyCabinetRepository, UserRepository $userRepository)
    {
        /** @var User $user */
        $user = $this->getUser();

        $mayTargetUser = $userRepository->findOneBy(['email' => User::SYSTEM_USER_MYTARGET]);

        $userParams = $user->getParams();

        if (key_exists('available_cabinets', $userParams) === false) {
            $userParams['available_cabinets'] = 5;
        }

        if ($userParams['available_cabinets'] > 0) {

            $userParams['available_cabinets']--;

            $agencyCabinet = $agencyCabinetRepository->findOneBy([
                'status' => AgencyCabinet::STATUS_ACTIVE,
                'user' => $mayTargetUser,
            ]);

            $user->addAgencyCabinet($agencyCabinet);

            $this->addFlash(FlashMessageTypes::SUCCESS, 'Добавлен кабинет: ' . $agencyCabinet->getName() . '. На сегодня доступно ещё ' . $userParams['available_cabinets']);

        } else {
            $this->addFlash(FlashMessageTypes::WARNING, 'Требуется больше аккаунтов? Обратитесь за помощью к своему менеджеру!');
        }

        $user->setParams($userParams);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('cabinet_agency_service_mytarget_index');

    }
}
