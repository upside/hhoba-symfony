<?php

namespace App\Controller\Cabinet\Agency;

use App\Entity\Main\User;
use App\Entity\Main\UserBalance;
use App\Entity\Main\UserBalanceTransaction;
use App\Repository\Main\UserBalanceRepository;
use App\Repository\Main\UserBalanceTransactionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cabinet/agency/transaction", name="cabinet_agency_transaction_")
 */
class TransactionController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('cabinet/agency/transaction/index.html.twig', [
            'controller_name' => 'TransactionController',
        ]);
    }

    /**
     * Вывод транзакций по типу операции
     *
     * @Route("/operation/{operation}/{page?}", name="operation", requirements={"page"="\d+"}, defaults={"page"="1"})
     * @param UserBalanceRepository $userBalanceRepository
     * @param UserBalanceTransactionRepository $userBalanceTransactionRepository
     * @param PaginatorInterface $paginator
     * @param int $operation
     * @param int $page
     * @return Response
     */
    public function operation(
        UserBalanceRepository $userBalanceRepository,
        UserBalanceTransactionRepository $userBalanceTransactionRepository,
        PaginatorInterface $paginator,
        int $operation,
        int $page
    )
    {

        /** @var User $user */
        $user = $this->getUser();

        $userBalance = $userBalanceRepository->findOneBy([
            'user' => $user,
            'type' => UserBalance::TYPE_AGENCY_USD,
        ]);

        $query = $userBalanceTransactionRepository->createQueryBuilder('ubt')
            ->where('(ubt.userBalanceFrom = :userBalanceFrom OR ubt.userBalanceTo = :userBalanceTo)')
            ->andWhere('ubt.operation = :operation')
            ->andWhere('ubt.status = :status')
            ->orderBy('ubt.created_at', 'DESC')
            ->setParameter(':userBalanceFrom', $userBalance)
            ->setParameter(':userBalanceTo', $userBalance)
            ->setParameter(':operation', $operation)
            ->setParameter(':status', UserBalanceTransaction::STATUS_SUCCESS)
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('cabinet/agency/transaction/operation.html.twig', [
            'operation' => $operation,
            'pagination' => $pagination,
        ]);
    }
}
