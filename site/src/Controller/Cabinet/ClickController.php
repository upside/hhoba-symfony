<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\User;
use App\Form\Cabinet\ClickFilterType;
use App\Repository\Main\ClickRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HelpController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/click", name="cabinet_click_")
 */
class ClickController extends AbstractController
{
    /**
     * @Route("/{page?}", name="index", requirements={"page"="\d+"}, defaults={"page"="1"})
     * @param Request $request
     * @param ClickRepository $clickRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(Request $request, ClickRepository $clickRepository, PaginatorInterface $paginator, int $page)
    {

        $form = $this->createForm(ClickFilterType::class, null, ['method' => 'GET'])->handleRequest($request);

        /** @var User $user */
        $user = $this->getUser();
        $tokenMap = $user->getParams()['tokens'] ?? [];

        $query = $clickRepository->createQueryBuilder('click')
            ->addSelect(['stream', 'trafficSource', 'lead', 'geo', 'userAgent', 'ipAddress', 'tokens', 'offer'])
            ->join('click.stream', 'stream')
            ->join('stream.trafficSource', 'trafficSource')
            ->join('click.geo', 'geo')
            ->join('stream.offer', 'offer')
            ->leftJoin('click.userAgent', 'userAgent')
            ->leftJoin('click.ipAddress', 'ipAddress')
            ->leftJoin('click.lead', 'lead')
            ->leftJoin('click.tokens', 'tokens')
            ->where('trafficSource.user = :user')
            ->setParameter(':user', $user)
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('cabinet/click/index.html.twig', [
            'form' => $form->createView(),
            'pagination' => $pagination,
            'tokenMap' => $tokenMap,
        ]);
    }
}
