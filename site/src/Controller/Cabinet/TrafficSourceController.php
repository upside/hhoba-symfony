<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\TrafficSource;
use App\Entity\Main\User;
use App\Form\Cabinet\TrafficSourceType;
use App\Repository\Main\TrafficSourceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TrafficSourceController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/traffic_source", name="cabinet_traffic_source_")
 */
class TrafficSourceController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param TrafficSourceRepository $trafficSourceRepository
     * @return Response
     */
    public function index(
        TrafficSourceRepository $trafficSourceRepository
    ): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $criteria = ['user' => $user];

        return $this->render('cabinet/traffic_source/index.html.twig', [
            'traffic_sources' => $trafficSourceRepository->findBy($criteria),
            'moderation_statuses' => TrafficSource::MODERATION_STATUSES,
            'source_types' => TrafficSource::SOURCE_TYPES,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $trafficSource = new TrafficSource();
        $form = $this->createForm(TrafficSourceType::class, $trafficSource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($trafficSource);
            $entityManager->flush();

            return $this->redirectToRoute('cabinet_traffic_source_index');
        }

        return $this->render('cabinet/traffic_source/new.html.twig', [
            'traffic_source' => $trafficSource,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     * @param TrafficSource $trafficSource
     * @return Response
     */
    public function show(TrafficSource $trafficSource): Response
    {
        return $this->render('cabinet/traffic_source/show.html.twig', [
            'traffic_source' => $trafficSource,
            'moderation_statuses' => TrafficSource::MODERATION_STATUSES,
            'source_types' => TrafficSource::SOURCE_TYPES,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param TrafficSource $trafficSource
     * @return Response
     */
    public function edit(Request $request, TrafficSource $trafficSource): Response
    {
        $form = $this->createForm(TrafficSourceType::class, $trafficSource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cabinet_traffic_source_index');
        }

        return $this->render('cabinet/traffic_source/edit.html.twig', [
            'traffic_source' => $trafficSource,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param TrafficSource $trafficSource
     * @return Response
     */
    public function delete(Request $request, TrafficSource $trafficSource): Response
    {
        if ($this->isCsrfTokenValid('delete' . $trafficSource->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($trafficSource);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cabinet_traffic_source_index');
    }
}
