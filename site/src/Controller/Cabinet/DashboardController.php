<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\User;
use App\Helpers\DateTimeHelper;
use App\Repository\Main\ClickRepository;
use App\Repository\Main\LeadRepository;
use App\Repository\Main\OfferRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @package App\Controller
 *
 * @Route("/cabinet/dashboard", name="cabinet_dashboard_")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param ClickRepository $clickRepository
     * @param OfferRepository $offerRepository
     * @param LeadRepository $leadRepository
     * @param DateTimeHelper $dateTimeHelper
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function index(
        ClickRepository $clickRepository,
        OfferRepository $offerRepository,
        LeadRepository $leadRepository,
        DateTimeHelper $dateTimeHelper
    )
    {
        /** @var User $user */
        $user = $this->getUser();

        $hourInterval = $dateTimeHelper->getCurrentHour();
        $dayInterval = $dateTimeHelper->getCurrentDay();
        $weekInterval = $dateTimeHelper->getCurrentWeek();
        $monthInterval = $dateTimeHelper->getCurrentMonth();
        $month3Interval = $dateTimeHelper->getCurrentMonth3();

        return $this->render('cabinet/dashboard/index.html.twig', [
            'clicks' => [
                'hour' => $clickRepository->getCountByUserAndDate($user, $hourInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $hourInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'day' => $clickRepository->getCountByUserAndDate($user, $dayInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $dayInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'week' => $clickRepository->getCountByUserAndDate($user, $weekInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $weekInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'month' => $clickRepository->getCountByUserAndDate($user, $monthInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $monthInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'month3' => $clickRepository->getCountByUserAndDate($user, $month3Interval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $month3Interval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
            ],
            'leads' => [
                'hour' => $leadRepository->getCountByUserAndDate($user, $hourInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $hourInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'day' => $leadRepository->getCountByUserAndDate($user, $dayInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $dayInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'week' => $leadRepository->getCountByUserAndDate($user, $weekInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $weekInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'month' => $leadRepository->getCountByUserAndDate($user, $monthInterval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $monthInterval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
                'month3' => $leadRepository->getCountByUserAndDate($user, $month3Interval->getDateFrom()->format(DateTimeHelper::DATE_TIME_YMD), $month3Interval->getDateTo()->format(DateTimeHelper::DATE_TIME_YMD)),
            ],
            'topOffers' => $offerRepository->getTopOffers(),
        ]);
    }
}
