<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\Stream;
use App\Entity\Main\User;
use App\Form\Cabinet\StreamType;
use App\Repository\Main\OfferRepository;
use App\Repository\Main\StreamRepository;
use App\Repository\Main\TrafficSourceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StreamController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/stream", name="cabinet_stream_")
 */
class StreamController extends AbstractController
{
    /**
     * @Route("/{page?}", name="index", requirements={"page"="\d+"}, defaults={"page"="1"})
     * @param StreamRepository $streamRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(StreamRepository $streamRepository, PaginatorInterface $paginator, int $page): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $query = $streamRepository->createQueryBuilder('s')
            ->join('s.offer', 'o')
            ->join('s.trafficSource', 'ts')
            ->where('ts.user = :user')
            ->setParameter(':user', $user)
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 20);

        return $this->render('cabinet/stream/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param OfferRepository $offerRepository
     * @param TrafficSourceRepository $trafficSourceRepository
     * @param Request $request
     * @return Response
     */
    public function new(
        OfferRepository $offerRepository,
        TrafficSourceRepository $trafficSourceRepository,
        Request $request
    ): Response
    {
        $stream = new Stream();

        $offer = $offerRepository->find($request->get('offer', null));
        if ($offer) {
            $stream->setOffer($offer);
        }

        $trafficSource = $trafficSourceRepository->find($request->get('offer', null));
        if ($trafficSource) {
            $stream->setTrafficSource($trafficSource);
        }

        $form = $this->createForm(StreamType::class, $stream);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stream);
            $entityManager->flush();

            return $this->redirectToRoute('cabinet_stream_index');
        }

        return $this->render('cabinet/stream/new.html.twig', [
            'stream' => $stream,
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @IsGranted(App\Security\Voter\StreamVoter::EDIT, subject="stream")
     * @param Request $request
     * @param Stream $stream
     * @return Response
     */
    public function edit(Request $request, Stream $stream): Response
    {
        $form = $this->createForm(StreamType::class, $stream);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cabinet_stream_index');
        }

        return $this->render('cabinet/stream/edit.html.twig', [
            'stream' => $stream,
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/show/{id}", name="show")
     * @IsGranted(App\Security\Voter\StreamVoter::SHOW, subject="stream")
     * @param Stream $stream
     * @return Response
     */
    public function show(Stream $stream): Response
    {
        return $this->render('cabinet/stream/show.html.twig', [
            'stream' => $stream,
        ]);
    }
}
