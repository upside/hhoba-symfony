<?php

namespace App\Controller\Cabinet;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FinanceController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/finance", name="cabinet_finance_")
 */
class FinanceController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('cabinet/finance/index.html.twig', [
            'controller_name' => 'FinanceController',
        ]);
    }
}
