<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\User;
use App\Form\Cabinet\StatisticType;
use App\Repository\Main\TokenDictionaryRepository;
use App\Repository\Statistic\StatisticRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatisticController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/statistic", name="cabinet_statistic_")
 */
class StatisticController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $form = $this->createForm(StatisticType::class, null, ['method' => 'GET'])->handleRequest($request);

        return $this->render('cabinet/statistic/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/jqx-grid", name="jqx_grid")
     * @param Request $request
     * @param StatisticRepository $statisticRepository
     * @param TokenDictionaryRepository $tokenDictionaryRepository
     * @return JsonResponse
     * @throws Exception
     */
    public function jqxGrid(Request $request, StatisticRepository $statisticRepository, TokenDictionaryRepository $tokenDictionaryRepository)
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(StatisticType::class, null, ['method' => 'GET'])->handleRequest($request);

        $dateFrom = $form->get(StatisticType::FIELD_DATE)->getData()['from'] ?? null;
        $dateTo = $form->get(StatisticType::FIELD_DATE)->getData()['to'] ?? null;

        $groups = [
            $form->get(StatisticType::FIELD_GROUP_1)->getData(),
            $form->get(StatisticType::FIELD_GROUP_2)->getData(),
            $form->get(StatisticType::FIELD_GROUP_3)->getData(),
        ];
        $groups = array_filter($groups);

        $filters = [];

        if (empty($form->get(StatisticType::FIELD_STREAMS)->getData()) === false) {
            $filters[StatisticType::FIELD_STREAMS] = $form->get(StatisticType::FIELD_STREAMS)->getData();
        }

        if (empty($form->get(StatisticType::FIELD_OFFERS)->getData()) === false) {
            $filters[StatisticType::FIELD_OFFERS] = $form->get(StatisticType::FIELD_OFFERS)->getData();
        }

        if (empty($form->get(StatisticType::FIELD_GEO)->getData()) === false) {
            $filters[StatisticType::FIELD_GEO] = $form->get(StatisticType::FIELD_GEO)->getData();
        }

        $tokens = [
            'token_1' => $form->get(StatisticType::FIELD_TOKEN_1)->getData(),
            'token_2' => $form->get(StatisticType::FIELD_TOKEN_2)->getData(),
            'token_3' => $form->get(StatisticType::FIELD_TOKEN_3)->getData(),
            'token_4' => $form->get(StatisticType::FIELD_TOKEN_4)->getData(),
            'token_5' => $form->get(StatisticType::FIELD_TOKEN_5)->getData(),
            'token_6' => $form->get(StatisticType::FIELD_TOKEN_6)->getData(),
            'token_7' => $form->get(StatisticType::FIELD_TOKEN_7)->getData(),
            'token_8' => $form->get(StatisticType::FIELD_TOKEN_8)->getData(),
            'token_9' => $form->get(StatisticType::FIELD_TOKEN_9)->getData(),
            'token_10' => $form->get(StatisticType::FIELD_TOKEN_10)->getData(),
            'token_11' => $form->get(StatisticType::FIELD_TOKEN_11)->getData(),
            'token_12' => $form->get(StatisticType::FIELD_TOKEN_12)->getData(),
            'token_13' => $form->get(StatisticType::FIELD_TOKEN_13)->getData(),
            'token_14' => $form->get(StatisticType::FIELD_TOKEN_14)->getData(),
            'token_15' => $form->get(StatisticType::FIELD_TOKEN_15)->getData(),
            'token_16' => $form->get(StatisticType::FIELD_TOKEN_16)->getData(),
        ];

        foreach ($tokens as $tokenKey => $tokenValue) {

            if (is_null($tokenValue)) {
                continue;
            }

            $t = explode('_', $tokenKey);

            $res = $tokenDictionaryRepository->getByNameAndValue($tokenKey, '%' . $tokenValue . '%');

            //TODO: Костыль для пустых значений
            if (empty($res)) {
                $res[] = ['id' => -1];
            }

            $ids = [];
            foreach ($res as $val) {
                $ids[] = $val['id'];
            }
            $key = $t[0] . $t[1] . '_id';
            $filters[$key] = $ids;
        }

        $statistics = $statisticRepository->getStatForUser($user, $dateFrom, $dateTo, $groups, $filters);

        $dataAdapter = [
            'datatype' => 'array',
            'localdata' => $statistics,
            'datafields' => [
                ['name' => 'totalClicks', 'type' => 'number'],
                ['name' => 'epc', 'type' => 'number'],
                ['name' => 'cr', 'type' => 'number'],
                ['name' => 'totalLeads', 'type' => 'number'],
                ['name' => 'newLeads', 'type' => 'number'],
                ['name' => 'confirmLeads', 'type' => 'number'],
                ['name' => 'declineLeads', 'type' => 'number'],
                ['name' => 'trashLeads', 'type' => 'number'],
                ['name' => 'totalMoney', 'type' => 'number'],
                ['name' => 'newMoney', 'type' => 'number'],
                ['name' => 'confirmMoney', 'type' => 'number'],
                ['name' => 'declineMoney', 'type' => 'number'],
                ['name' => 'trashMoney', 'type' => 'number'],
            ],
        ];

        $columns = [
            [
                'text' => 'Клики',
                'datafield' => 'totalClicks',
            ],
            [
                'text' => 'EPC',
                'datafield' => 'epc',
                'width' => '100px',
            ],
            [
                'text' => 'CR',
                'datafield' => 'cr',
                'width' => '100px',
            ],
            [
                'text' => 'Лиды (всего)',
                'datafield' => 'totalLeads',
            ],
            [
                'text' => 'Лиды (новые)',
                'datafield' => 'newLeads',
            ],
            [
                'text' => 'Лиды (подтверждёные)',
                'datafield' => 'confirmLeads',
            ],
            [
                'text' => 'Лиды (откланёные)',
                'datafield' => 'declineLeads',
            ],
            [
                'text' => 'Лиды (треш)',
                'datafield' => 'trashLeads',
            ],
            [
                'text' => 'Деньги (новые)',
                'datafield' => 'newMoney',
            ],
            [
                'text' => 'Деньги (подтверждёные)',
                'datafield' => 'confirmMoney',
            ],
        ];

        foreach ($groups as $group) {
            array_unshift($columns, [
                'text' => array_flip(StatisticType::GROUP_CHOICES)[$group],
                'datafield' => $group,
                'hidden' => true,
            ]);

            $dataAdapter['datafields'][] = ['name' => $group, 'type' => 'string'];
        }

        $jqxGrid = [
            'theme' => 'material',
            'width' => '100%',
            'height' => '700px',
            'source' => $dataAdapter,
            'groupable' => true,
            'columnsreorder' => true,
            'selectionmode' => 'singlecell',
            'groups' => array_filter($groups),
            'columns' => $columns,
        ];

        return $this->json($jqxGrid);
    }
}
