<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\User;
use App\Form\Cabinet\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HelpController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/profile", name="cabinet_profile_")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->render('cabinet/profile/index.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit", name="edit")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Request $request)
    {

        $user = $this->getUser();

        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('cabinet_profile_index');
        }

        return $this->render('cabinet/profile/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);

    }
}
