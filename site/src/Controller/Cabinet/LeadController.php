<?php

namespace App\Controller\Cabinet;

use App\Entity\Main\User;
use App\Repository\Main\LeadRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HelpController
 * @package App\Controller\Cabinet
 *
 * @Route("/cabinet/lead", name="cabinet_lead_")
 */
class LeadController extends AbstractController
{
    /**
     * @Route("/{page?}", name="index", requirements={"page"="\d+"}, defaults={"page"="1"})
     * @param LeadRepository $leadRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(LeadRepository $leadRepository, PaginatorInterface $paginator, int $page)
    {
        /** @var User $user */
        $user = $this->getUser();
        $tokenMap = $user->getParams()['tokens'] ?? [];

        $query = $leadRepository->createQueryBuilder('lead')
            ->addSelect(['stream', 'trafficSource', 'click', 'geo', 'userAgent', 'ipAddress', 'tokens'])
            ->join('lead.click', 'click')
            ->join('click.stream', 'stream')
            ->join('stream.trafficSource', 'trafficSource')
            ->join('lead.geo', 'geo')
            ->leftJoin('lead.userAgent', 'userAgent')
            ->leftJoin('lead.ipAddress', 'ipAddress')
            ->leftJoin('lead.tokens', 'tokens')
            ->where('trafficSource.user = :user')
            ->setParameter(':user', $user)
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('cabinet/lead/index.html.twig', [
            'pagination' => $pagination,
            'tokenMap' => $tokenMap,
        ]);
    }
}
