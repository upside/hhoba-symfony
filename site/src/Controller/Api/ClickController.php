<?php

namespace App\Controller\Api;

use App\Entity\Main\Stream;
use App\Helpers\DateTimeHelper;
use App\Message\ClickMessage;
use DateTime;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ClickController extends AbstractController
{
    /**
     * @Route("/api/click/{uuid}", name="api_click", methods={"GET"}, defaults={"_format"="json"})
     * @param Request $request
     * @param MessageBusInterface $messageBus
     * @param Stream $stream
     * @return JsonResponse
     */
    public function index(Request $request, MessageBusInterface $messageBus, Stream $stream)
    {

        $clickUuid = Uuid::uuid4();

        $clickMessage = new ClickMessage(
            $stream->getUuid(),
            $clickUuid,
            (new DateTime(DateTimeHelper::NOW))->format(DateTimeHelper::DATE_TIME_YMD),
            [
                'ip' => $request->query->get('ip'),
                'useragent' => $request->query->get('useragent'),
                'geo' => $request->query->get('geo'),
            ]
        );

        $messageBus->dispatch($clickMessage);

        return $this->json([
            'click' => $clickUuid,
        ]);
    }
}
