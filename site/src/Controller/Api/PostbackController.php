<?php

namespace App\Controller\Api;

use App\Entity\Main\Advertiser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostbackController extends AbstractController
{
    /**
     * @Route("/api/postback/{advertiser}", name="api_postback")
     * @param Request $request
     * @param Advertiser $advertiser
     * @return JsonResponse
     */
    public function index(Request $request, Advertiser $advertiser)
    {
        return $this->json([
            'advertiser' => $advertiser->getId(),
        ]);
    }
}
