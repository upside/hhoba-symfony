<?php

namespace App\Controller\Api;

use App\Entity\Main\Click;
use App\Helpers\DateTimeHelper;
use App\Message\ClickMessage;
use App\Message\LeadMessage;
use DateTime;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class LeadController extends AbstractController
{
    /**
     * @Route("/api/lead/{uuid}", name="api_lead", methods={"POST"}, defaults={"_format"="json"})
     * @param Request $request
     * @param MessageBusInterface $messageBus
     * @param Click $click
     * @return JsonResponse
     */
    public function index(Request $request, MessageBusInterface $messageBus, Click $click)
    {
        $now = (new DateTime(DateTimeHelper::NOW))->format(DateTimeHelper::DATE_TIME_YMD);
        $geoParam = $request->request->get('geo');
        $ipAddressParam = $request->request->get('ip');
        $useragentParam = $request->request->get('useragent');

        $clickUuid = $click->getUuid();
        $newClick = false;

        if (is_null($click->getLead()) === false) {

            $clickUuid = Uuid::uuid4();
            $newClick = true;

            $clickMessage = new ClickMessage(
                $click->getStream()->getUuid(),
                $clickUuid,
                $now,
                [
                    'geo' => $geoParam,
                    'ip' => $ipAddressParam,
                    'useragent' => $useragentParam,
                ]
            );

            $messageBus->dispatch($clickMessage);
        }

        $leadUuid = Uuid::uuid4();

        $phone = $request->request->get('phone');
        if (is_null($phone)) {
            throw new BadRequestHttpException('Неверный параметр phone: ' . $phone);
        }

        $leadMessage = new LeadMessage(
            $clickUuid,
            $leadUuid,
            $phone,
            $now,
            [
                'name' => $request->request->get('name'),
                'comment' => $request->request->get('comment'),
                'ip' => $ipAddressParam,
                'useragent' => $useragentParam,
                'geo' => $geoParam,
            ]
        );

        $messageBus->dispatch($leadMessage);

        return $this->json([
            'lead' => $leadUuid,
            'click' => $clickUuid,
            'isNewClick' => $newClick,
        ]);
    }
}
