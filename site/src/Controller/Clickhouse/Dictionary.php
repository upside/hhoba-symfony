<?php

namespace App\Controller\Clickhouse;

use App\Repository\Main\GeoRepository;
use App\Repository\Main\OfferRepository;
use App\Repository\Main\StreamRepository;
use App\Repository\Main\TokenDictionaryRepository;
use App\Repository\Main\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/clickhouse", name="clickhouse_")
 */
class Dictionary extends AbstractController
{
    /**
     * @Route("/geo", name="geo", methods={"GET"})
     * @param GeoRepository $geoRepository
     * @return Response
     */
    public function geo(GeoRepository $geoRepository)
    {
        $geo = $geoRepository->createQueryBuilder('g')
            ->select(['g.id', 'g.iso_2'])
            ->getQuery()
            ->getResult();

        $fp = fopen('php://output', 'w');

        foreach ($geo as $fields) {
            fputcsv($fp, $fields);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="geo.csv"');
        return $response;
    }

    /**
     * @Route("/streams", name="streams", methods={"GET"})
     * @param StreamRepository $streamRepository
     * @return Response
     */
    public function streams(StreamRepository $streamRepository)
    {
        $streams = $streamRepository->createQueryBuilder('s')
            ->select(['s.id', 's.name'])
            ->getQuery()
            ->getResult();

        $fp = fopen('php://output', 'w');

        foreach ($streams as $fields) {
            fputcsv($fp, $fields);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="streams.csv"');
        return $response;
    }

    /**
     * @Route("/offers", name="offers", methods={"GET"})
     * @param OfferRepository $offerRepository
     * @return Response
     */
    public function offers(OfferRepository $offerRepository)
    {
        $offers = $offerRepository->createQueryBuilder('o')
            ->select(['o.id', 'o.name'])
            ->getQuery()
            ->getResult();

        $fp = fopen('php://output', 'w');

        foreach ($offers as $fields) {
            fputcsv($fp, $fields);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="offers.csv"');
        return $response;
    }

    /**
     * @Route("/tokens", name="tokens", methods={"GET"})
     * @param TokenDictionaryRepository $tokenDictionaryRepository
     * @return Response
     */
    public function tokens(TokenDictionaryRepository $tokenDictionaryRepository)
    {
        $tokens = $tokenDictionaryRepository->createQueryBuilder('t')
            ->select(['t.id', 't.value'])
            ->getQuery()
            ->getResult();

        $fp = fopen('php://output', 'w');

        foreach ($tokens as $fields) {
            fputcsv($fp, $fields);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="tokens.csv"');
        return $response;
    }

    /**
     * @Route("/users", name="users", methods={"GET"})
     * @param UserRepository $userRepository
     * @return Response
     */
    public function users(UserRepository $userRepository)
    {
        $tokens = $userRepository->createQueryBuilder('u')
            ->select(['u.id', 'u.email'])
            ->getQuery()
            ->getResult();

        $fp = fopen('php://output', 'w');

        foreach ($tokens as $fields) {
            fputcsv($fp, $fields);
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="users.csv"');
        return $response;
    }
}
