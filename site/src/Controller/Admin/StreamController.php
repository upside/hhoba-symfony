<?php

namespace App\Controller\Admin;

use App\Entity\Main\Offer;
use App\Entity\Main\Stream;
use App\Form\StreamType;
use App\Repository\Main\StreamRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/stream", name="admin_stream_")
 */
class StreamController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param StreamRepository $streamRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(StreamRepository $streamRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $streamRepository->createQueryBuilder('s')
            ->leftJoin('s.offer', 'o')
            ->leftJoin('s.trafficSource', 'ts')
            ->leftJoin('ts.user', 'u')
            ->addSelect(['o', 'u', 'ts'])
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('admin/stream/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new/{offer}/{trafficSource}", name="new", methods={"GET","POST"})
     * @param Request $request
     * @param Offer|null $offer
     * @param TrafficSource|null $trafficSource
     * @return Response
     */
    public function new(Request $request, ?Offer $offer = null, ?TrafficSource $trafficSource = null): Response
    {
        $stream = new Stream();
        $stream->setOffer($offer);
        $stream->setTrafficSource($trafficSource);

        $form = $this->createForm(StreamType::class, $stream);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stream);
            $entityManager->flush();

            return $this->redirectToRoute('admin_stream_index');
        }

        return $this->render('admin/stream/new.html.twig', [
            'stream' => $stream,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Stream $stream
     * @return Response
     */
    public function show(Stream $stream): Response
    {
        return $this->render('admin/stream/show.html.twig', [
            'stream' => $stream,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Stream $stream
     * @return Response
     */
    public function edit(Request $request, Stream $stream): Response
    {
        $form = $this->createForm(StreamType::class, $stream);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_stream_index');
        }

        return $this->render('admin/stream/edit.html.twig', [
            'stream' => $stream,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Stream $stream
     * @return Response
     */
    public function delete(Request $request, Stream $stream): Response
    {
        if ($this->isCsrfTokenValid('delete' . $stream->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($stream);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_stream_index');
    }
}
