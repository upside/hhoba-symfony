<?php

namespace App\Controller\Admin;

use App\Agency\Vo\MyTarget\AgencyClient;
use App\Agency\Vo\MyTarget\AgencyClientCollection;
use App\Agency\Vo\MyTarget\Error;
use App\Agency\Vo\MyTarget\Transaction;
use App\Notifier\LowBalanceNotification;
use App\Notifier\TelegramMessageOptions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 *
 * @Route("/admin/dashboard", name="admin_dashboard_")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(NotifierInterface $notifier)
    {

        //dd($serializer->deserialize('{"error":{"code":"not_found","message":"client not found"}}', Error::class, 'json'));

        //$message = new LowBalanceNotification('You got a new invoice for 15 EUR.', ['chat/telegram'], new TelegramMessageOptions('525775843'));

        //$notifier->send($message, new Recipient('525775843'));

        return $this->render('admin/dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
