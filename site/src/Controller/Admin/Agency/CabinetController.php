<?php

namespace App\Controller\Admin\Agency;

use App\Agency\MyTargetApi;
use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\User;
use App\Form\AgencyCabinetType;
use App\Form\Cabinet\Agency\FromFileAgencyCabinetType;
use App\Repository\Main\Agency\AgencyCabinetRepository;
use App\Repository\Main\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @Route("/admin/agency/cabinet", name="admin_agency_cabinet_")
 */
class CabinetController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(AgencyCabinetRepository $agencyCabinetRepository, UserRepository $userRepository, PaginatorInterface $paginator, int $page = 1)
    {
        $query = $agencyCabinetRepository
            ->createQueryBuilder('ac')
            ->addSelect(['ac', 'u'])
            ->leftJoin('ac.user', 'u')
            ->orderBy('ac.name')
            ->orderBy('ac.id', 'DESC')
            ->getQuery();
        $pagination = $paginator->paginate($query, $page, 100);

        $users = $userRepository->findAll();

        return $this->render('admin/agency/cabinet/index.html.twig', [
            'pagination' => $pagination,
            'users' => $users,
        ]);
    }

    /**
     * @Route("/cabinet-to-user", name="cabinet_to_user")
     * @param Request $request
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param UserRepository $userRepository
     * @return RedirectResponse
     */
    public function cabinetToUser(Request $request, AgencyCabinetRepository $agencyCabinetRepository, UserRepository $userRepository)
    {
        $agencyCabinet = $agencyCabinetRepository->find($request->request->get('id'));
        $toUser = $userRepository->find($request->request->get('user_id'));

        if (is_null($agencyCabinet) !== null && is_null($toUser) !== null) {
            $agencyCabinet->setUser($toUser);
            $this->getDoctrine()->getManager()->persist($agencyCabinet);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('admin_agency_cabinet_index');
    }


    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param MyTargetApi $myTargetApi
     * @param SerializerInterface $serializer
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function new(MyTargetApi $myTargetApi, SerializerInterface $serializer, UserRepository $userRepository, Request $request)
    {
        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => User::SYSTEM_USER_MYTARGET]);
        $agencyCabinet = new AgencyCabinet();

        $form = $this->createForm(AgencyCabinetType::class, $agencyCabinet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $apiClient = $myTargetApi->getClientByUsername($agencyCabinet->getApiName());

            if (is_null($apiClient) === false) {
                $agencyCabinet->setUser($user);
                $agencyCabinet->setApiId($apiClient->getUser()->getId());
                $agencyCabinet->setBalance($apiClient->getUser()->getAccount()->getBalance());
                $agencyCabinet->setAutoCompletion(false);
                $agencyCabinet->setBalanceBelow(0);
                $agencyCabinet->setReplenishmentAmount(0);
                $agencyCabinet->setStatus(1);
                $agencyCabinet->setType(AgencyCabinet::TYPE_MYTARGET);
                $agencyCabinet->setParams(json_decode($serializer->serialize($apiClient, 'json'), true));

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($agencyCabinet);
                $entityManager->flush();
            }

            return $this->redirectToRoute('admin_agency_cabinet_index');
        }

        return $this->render('admin/agency/cabinet/new.html.twig', [
            'agencyCabinet' => $agencyCabinet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/load-from-file", name="load_from_file", methods={"GET","POST"})
     * @param MyTargetApi $myTargetApi
     * @param UserRepository $userRepository
     * @param AgencyCabinetRepository $agencyCabinetRepository
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     * @throws InvalidArgumentException
     */
    public function loadFromFile(MyTargetApi $myTargetApi, UserRepository $userRepository, AgencyCabinetRepository $agencyCabinetRepository, SerializerInterface $serializer, Request $request)
    {
        $form = $this->createForm(FromFileAgencyCabinetType::class);
        $form->handleRequest($request);

        $errors = [];
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $userRepository->findOneBy(['email' => User::SYSTEM_USER_MYTARGET]);

            /** @var UploadedFile $brochureFile */
            $brochureFile = $form->get('file')->getData();

            $handle = @fopen($brochureFile->getPathname(), 'r');

            $accounts = [];
            if ($handle) {
                while (($buffer = fgets($handle, 4096)) !== false) {
                    $accounts[] = explode(':', trim($buffer));
                }
                //if (!feof($handle)) {
                //    echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
                //}
                fclose($handle);
            }

            $entityManager = $this->getDoctrine()->getManager();

            foreach ($accounts as $account) {
                try {
                    $apiClient = $myTargetApi->getClientByUsername($account[0]);
                    $agencyCabinet = $agencyCabinetRepository->findOneBy(['apiId' => $apiClient->getUser()->getId()]);

                    if (is_null($agencyCabinet) === false) {
                        throw new \Exception('Кабинет уже сущевствует');
                    }

                    $agencyCabinet = new AgencyCabinet();
                    $agencyCabinet->setUser($user);
                    $agencyCabinet->setApiName($apiClient->getUser()->getUsername());
                    $agencyCabinet->setName($apiClient->getUser()->getUsername());
                    $agencyCabinet->setPassword($account[1]);
                    $agencyCabinet->setApiId($apiClient->getUser()->getId());
                    $agencyCabinet->setBalance($apiClient->getUser()->getAccount()->getBalance());
                    $agencyCabinet->setAutoCompletion(false);
                    $agencyCabinet->setBalanceBelow(0);
                    $agencyCabinet->setReplenishmentAmount(0);
                    $agencyCabinet->setStatus(1);
                    $agencyCabinet->setType(AgencyCabinet::TYPE_MYTARGET);
                    $agencyCabinet->setParams(json_decode($serializer->serialize($apiClient, 'json'), true));

                    $entityManager->persist($agencyCabinet);

                } catch (\Throwable $exception) {
                    $errors[] = $account;
                }

                $entityManager->flush();
            }
        }

        return $this->render('admin/agency/cabinet/load_from_file.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
        ]);

    }
}
