<?php

namespace App\Controller\Admin\Agency;

use App\Repository\Main\UserBalanceTransactionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin/agency/transaction", name="admin_agency_transaction_")
 */
class TransactionController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param UserBalanceTransactionRepository $userBalanceTransactionRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(UserBalanceTransactionRepository $userBalanceTransactionRepository, PaginatorInterface $paginator, int $page = 1)
    {
        $query = $userBalanceTransactionRepository
            ->createQueryBuilder('ubtr')
            ->addSelect(['acf', 'act', 'ubf', 'ubt', 'fu', 'tu'])
            ->leftJoin('ubtr.agencyCabinetFrom', 'acf')
            ->leftJoin('ubtr.agencyCabinetTo', 'act')
            ->leftJoin('ubtr.userBalanceFrom', 'ubf')
            ->leftJoin('ubtr.userBalanceTo', 'ubt')
            ->leftJoin('ubf.user', 'fu')
            ->leftJoin('ubt.user', 'tu')
            ->orderBy('ubtr.created_at', 'DESC')
            ->getQuery();
        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('admin/agency/transaction/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
