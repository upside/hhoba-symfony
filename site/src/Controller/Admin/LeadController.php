<?php

namespace App\Controller\Admin;

use App\Entity\Main\Click;
use App\Entity\Main\Lead;
use App\Form\LeadType;
use App\Repository\Main\LeadRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/lead", name="admin_lead_")
 */
class LeadController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param LeadRepository $leadRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(LeadRepository $leadRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $pagination = $paginator->paginate($leadRepository->getForAdminList(), $page, 100);

        return $this->render('admin/lead/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new/{click}", name="new", methods={"GET","POST"})
     * @param Request $request
     * @param Click $click
     * @return Response
     */
    public function new(Request $request, Click $click): Response
    {
        $lead = new Lead();
        $lead->setClick($click);
        $form = $this->createForm(LeadType::class, $lead);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lead);
            $entityManager->flush();

            return $this->redirectToRoute('admin_lead_index');
        }

        return $this->render('admin/lead/new.html.twig', [
            'lead' => $lead,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Lead $lead
     * @return Response
     */
    public function show(Lead $lead): Response
    {
        return $this->render('admin/lead/show.html.twig', [
            'lead' => $lead,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Lead $lead
     * @return Response
     */
    public function edit(Request $request, Lead $lead): Response
    {
        $form = $this->createForm(LeadType::class, $lead);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_lead_index');
        }

        return $this->render('admin/lead/edit.html.twig', [
            'lead' => $lead,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Lead $lead
     * @return Response
     */
    public function delete(Request $request, Lead $lead): Response
    {
        if ($this->isCsrfTokenValid('delete' . $lead->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lead);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_lead_index');
    }
}
