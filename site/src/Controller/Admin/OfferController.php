<?php

namespace App\Controller\Admin;

use App\Entity\Main\Advertiser;
use App\Entity\Main\Offer;
use App\Form\OfferType;
use App\Repository\Main\OfferRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/offer", name="admin_offer_")
 */
class OfferController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param OfferRepository $offerRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(OfferRepository $offerRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $offerRepository->createQueryBuilder('o')
            ->join('o.advertiser', 'a')
            ->leftJoin('o.tariffs', 't')
            ->leftJoin('t.geo', 'g')
            ->leftJoin('t.currency', 'c')
            ->leftJoin('t.user', 'u')
            ->addSelect(['a', 't', 'g', 'c', 'u'])
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 20);

        return $this->render('admin/offer/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new/{advertiser}", name="new", methods={"GET","POST"})
     * @param Request $request
     * @param Advertiser $advertiser
     * @return Response
     */
    public function new(Request $request, Advertiser $advertiser): Response
    {
        $offer = new Offer();
        $offer->setAdvertiser($advertiser);
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $brochureFile */
            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                $filename = uniqid('offer_') . '.' . $imageFile->guessExtension();

                try {
                    $imageFile->move(
                        $this->getParameter('offer_image_directory'),
                        $filename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $offer->setImage($filename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($offer);
            $entityManager->flush();

            return $this->redirectToRoute('admin_offer_index');
        }

        return $this->render('admin/offer/new.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Offer $offer
     * @return Response
     */
    public function show(Offer $offer): Response
    {
        return $this->render('admin/offer/show.html.twig', [
            'offer' => $offer,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Offer $offer
     * @return Response
     */
    public function edit(Request $request, Offer $offer): Response
    {
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $brochureFile */
            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                $filename = uniqid('offer_') . '.' . $imageFile->guessExtension();

                try {
                    $imageFile->move(
                        $this->getParameter('offer_image_directory'),
                        $filename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $offer->setImage($filename);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_offer_index');
        }

        return $this->render('admin/offer/edit.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Offer $offer
     * @return Response
     */
    public function delete(Request $request, Offer $offer): Response
    {
        if ($this->isCsrfTokenValid('delete' . $offer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($offer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_offer_index');
    }
}
