<?php

namespace App\Controller\Admin;

use App\Entity\Main\User;
use App\Entity\Main\UserBalance;
use App\Entity\Main\UserBalanceTransaction;
use App\Form\UserType;
use App\Repository\Main\CurrencyRepository;
use App\Repository\Main\UserBalanceRepository;
use App\Repository\Main\UserRepository;
use App\Services\UserBalanceTransactionService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/user", name="admin_user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(UserRepository $userRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $userRepository->createQueryBuilder('u')
            ->addSelect(['ub'])
            ->leftJoin('u.userBalances', 'ub')
            ->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        $balanceTypes = UserBalance::TYPES;

        return $this->render('admin/user/index.html.twig', [
            'pagination' => $pagination,
            'balanceTypes' => $balanceTypes,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData())
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/balance/edit", name="balance_edit")
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserBalanceRepository $userBalanceRepository
     * @param CurrencyRepository $currencyRepository
     * @param UserBalanceTransactionService $balanceTransactionService
     * @return RedirectResponse
     */
    public function editBalance(
        Request $request,
        UserRepository $userRepository,
        UserBalanceRepository $userBalanceRepository,
        CurrencyRepository $currencyRepository,
        UserBalanceTransactionService $balanceTransactionService
    )
    {
        $type = $request->request->get('balance_type');
        $amount = $request->request->get('amount');

        $user = $userRepository->find($request->request->get('id'));

        $mtUser = $userRepository->findOneBy(['email' => User::SYSTEM_USER_MYTARGET]);

        $userBalance = $userBalanceRepository->findOneBy([
            'user' => $user,
            'type' => $type,
        ]);

        $mtBalance = $userBalanceRepository->findOneBy([
            'user' => $mtUser,
            'type' => $type,
        ]);

        $currency = $currencyRepository->findOneBy(['code' => 'USD']);

        if (is_null($userBalance)) {
            $userBalance = new UserBalance();
            $userBalance->setUser($user);
            $userBalance->setType($type);
            $userBalance->setCurrency($currency);
            $userBalance->setAmount('0');
        }

        $balanceTransactionService->newTransaction(
            $mtBalance,
            $userBalance,
            UserBalanceTransaction::OPERATION_DEPOSIT,
            $amount
        );

        return $this->redirectToRoute('admin_user_index');
    }
}
