<?php

namespace App\Controller\Admin;

use App\Entity\Main\Advertiser;
use App\Form\AdvertiserType;
use App\Repository\Main\AdvertiserRepository;
use App\Repository\Main\OfferRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/advertiser", name="admin_advertiser_")
 */
class AdvertiserController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param AdvertiserRepository $advertiserRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(AdvertiserRepository $advertiserRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $advertiserRepository->createQueryBuilder('a')->getQuery();
        $pagination = $paginator->paginate($query, $page, 20);

        return $this->render('admin/advertiser/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new/{apiId}", name="new", methods={"GET","POST"})
     * @param Request $request
     * @param int $apiId
     * @return Response
     */
    public function new(Request $request, int $apiId): Response
    {
        $advertiser = new Advertiser();
        $advertiser->setApiId($apiId);
        $form = $this->createForm(AdvertiserType::class, $advertiser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertiser);
            $entityManager->flush();

            return $this->redirectToRoute('admin_advertiser_index');
        }

        return $this->render('admin/advertiser/new.html.twig', [
            'advertiser' => $advertiser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param OfferRepository $offerRepository
     * @param Advertiser $advertiser
     * @return Response
     */
    public function show(OfferRepository $offerRepository, Advertiser $advertiser): Response
    {
        $offers = $offerRepository->createQueryBuilder('o')
            ->where('o.advertiser = :advertiser')
            ->setParameter(':advertiser', $advertiser->getId())
            ->getQuery()
            ->getResult();

        return $this->render('admin/advertiser/show.html.twig', [
            'advertiser' => $advertiser,
            'offers' => $offers,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Advertiser $advertiser
     * @return Response
     */
    public function edit(Request $request, Advertiser $advertiser): Response
    {
        $form = $this->createForm(AdvertiserType::class, $advertiser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_advertiser_index');
        }

        return $this->render('admin/advertiser/edit.html.twig', [
            'advertiser' => $advertiser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Advertiser $advertiser
     * @return Response
     */
    public function delete(Request $request, Advertiser $advertiser): Response
    {
        if ($this->isCsrfTokenValid('delete' . $advertiser->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($advertiser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_advertiser_index');
    }
}
