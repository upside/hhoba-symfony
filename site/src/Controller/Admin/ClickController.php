<?php

namespace App\Controller\Admin;

use App\Entity\Main\Click;
use App\Form\ClickType;
use App\Repository\Main\ClickRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/click", name="admin_click_")
 */
class ClickController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param ClickRepository $clickRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(ClickRepository $clickRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $clickRepository->createQueryBuilder('c')
            ->leftJoin('c.lead', 'l')
            ->addSelect(['l']);

        $pagination = $paginator->paginate($query->getQuery(), $page, 100);

        return $this->render('admin/click/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $click = new Click();
        $form = $this->createForm(ClickType::class, $click);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($click);
            $entityManager->flush();

            return $this->redirectToRoute('admin_click_index');
        }

        return $this->render('admin/click/new.html.twig', [
            'click' => $click,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Click $click
     * @return Response
     */
    public function show(Click $click): Response
    {
        return $this->render('admin/click/show.html.twig', [
            'click' => $click,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Click $click
     * @return Response
     */
    public function edit(Request $request, Click $click): Response
    {
        $form = $this->createForm(ClickType::class, $click);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_click_index');
        }

        return $this->render('admin/click/edit.html.twig', [
            'click' => $click,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Click $click
     * @return Response
     */
    public function delete(Request $request, Click $click): Response
    {
        if ($this->isCsrfTokenValid('delete' . $click->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($click);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_click_index');
    }
}
