<?php

namespace App\Controller\Admin;

use App\Entity\Main\Tariff;
use App\Form\TariffType;
use App\Repository\Main\TariffRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/tariff", name="admin_tariff_")
 */
class TariffController extends AbstractController
{
    /**
     * @Route("/{page}", name="index",  requirements={"page"="\d+"},  methods={"GET"})
     * @param TariffRepository $tariffRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(TariffRepository $tariffRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $tariffRepository->createQueryBuilder('t')->getQuery();
        $pagination = $paginator->paginate($query, $page, 20);

        return $this->render('admin/tariff/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $tariff = new Tariff();
        $form = $this->createForm(TariffType::class, $tariff);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tariff);
            $entityManager->flush();

            return $this->redirectToRoute('admin_tariff_index');
        }

        return $this->render('admin/tariff/new.html.twig', [
            'tariff' => $tariff,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Tariff $tariff
     * @return Response
     */
    public function show(Tariff $tariff): Response
    {
        return $this->render('admin/tariff/show.html.twig', [
            'tariff' => $tariff,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Tariff $tariff
     * @return Response
     */
    public function edit(Request $request, Tariff $tariff): Response
    {
        $form = $this->createForm(TariffType::class, $tariff);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_tariff_index');
        }

        return $this->render('admin/tariff/edit.html.twig', [
            'tariff' => $tariff,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Tariff $tariff
     * @return Response
     */
    public function delete(Request $request, Tariff $tariff): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tariff->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tariff);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_tariff_index');
    }
}
