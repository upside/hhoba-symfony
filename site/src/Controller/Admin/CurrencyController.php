<?php

namespace App\Controller\Admin;

use App\Entity\Main\Currency;
use App\Form\CurrencyType;
use App\Repository\Main\CurrencyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/currency", name="admin_currency_")
 */
class CurrencyController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param CurrencyRepository $currencyRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(CurrencyRepository $currencyRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $currencyRepository->createQueryBuilder('c')->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('admin/currency/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $currency = new Currency();
        $form = $this->createForm(CurrencyType::class, $currency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($currency);
            $entityManager->flush();

            return $this->redirectToRoute('admin_currency_index');
        }

        return $this->render('admin/currency/new.html.twig', [
            'currency' => $currency,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Currency $currency
     * @return Response
     */
    public function show(Currency $currency): Response
    {
        return $this->render('admin/currency/show.html.twig', [
            'currency' => $currency,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Currency $currency
     * @return Response
     */
    public function edit(Request $request, Currency $currency): Response
    {
        $form = $this->createForm(CurrencyType::class, $currency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_currency_index');
        }

        return $this->render('admin/currency/edit.html.twig', [
            'currency' => $currency,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Currency $currency
     * @return Response
     */
    public function delete(Request $request, Currency $currency): Response
    {
        if ($this->isCsrfTokenValid('delete' . $currency->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($currency);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_currency_index');
    }
}
