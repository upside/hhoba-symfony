<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatisticController
 * @package App\Controller\Admin
 *
 * @Route("/admin/statistic", name="admin_statistic_")
 */
class StatisticController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('admin/statistic/index.html.twig', [
            'controller_name' => 'StatisticController',
        ]);
    }
}
