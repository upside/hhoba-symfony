<?php

namespace App\Controller\Admin;

use App\Entity\Main\Geo;
use App\Form\GeoType;
use App\Repository\Main\GeoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/geo", name="admin_geo_")
 */
class GeoController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"}, methods={"GET"})
     * @param GeoRepository $geoRepository
     * @param PaginatorInterface $paginator
     * @param int $page
     * @return Response
     */
    public function index(GeoRepository $geoRepository, PaginatorInterface $paginator, int $page = 1): Response
    {
        $query = $geoRepository->createQueryBuilder('g')->getQuery();

        $pagination = $paginator->paginate($query, $page, 100);

        return $this->render('admin/geo/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $geo = new Geo();
        $form = $this->createForm(GeoType::class, $geo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($geo);
            $entityManager->flush();

            return $this->redirectToRoute('admin_geo_index');
        }

        return $this->render('admin/geo/new.html.twig', [
            'geo' => $geo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Geo $geo
     * @return Response
     */
    public function show(Geo $geo): Response
    {
        return $this->render('admin/geo/show.html.twig', [
            'geo' => $geo,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Geo $geo
     * @return Response
     */
    public function edit(Request $request, Geo $geo): Response
    {
        $form = $this->createForm(GeoType::class, $geo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_geo_index');
        }

        return $this->render('admin/geo/edit.html.twig', [
            'geo' => $geo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     * @param Request $request
     * @param Geo $geo
     * @return Response
     */
    public function delete(Request $request, Geo $geo): Response
    {
        if ($this->isCsrfTokenValid('delete' . $geo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($geo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_geo_index');
    }
}
