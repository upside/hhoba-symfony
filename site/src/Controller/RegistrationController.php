<?php

namespace App\Controller;

use App\Entity\Main\User;
use App\Entity\Main\UserBalance;
use App\Form\RegistrationFormType;
use App\Repository\Main\CurrencyRepository;
use App\Security\AppLoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param AppLoginFormAuthenticator $authenticator
     * @param CurrencyRepository $currencyRepository
     * @return Response
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        AppLoginFormAuthenticator $authenticator,
        CurrencyRepository $currencyRepository
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            //TODO: Перенести обрезание собачки в форму
            if (is_null($user->getTelegram()) === false) {
                $user->setTelegram(ltrim($user->getTelegram(), '@'));
            }

            $currency = $currencyRepository->findOneBy(['code' => 'USD']);

            $userBalance = new UserBalance();
            $userBalance->setAmount(0);
            $userBalance->setType(UserBalance::TYPE_AGENCY_USD);
            $userBalance->setCurrency($currency);
            $userBalance->setUser($user);

            $user->addUserBalance($userBalance);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($userBalance);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }


        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
