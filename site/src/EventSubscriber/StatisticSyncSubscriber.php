<?php

namespace App\EventSubscriber;

use App\Entity\Main\Click;
use App\Entity\Main\Lead;
use App\Entity\Main\TokenDictionary;
use App\Entity\Statistic\Statistic;
use App\Event\ClickPlacedEvent;
use App\Event\LeadPlacedEvent;
use App\Event\LeadUpdatedEvent;
use App\Event\LeadUpdateEvent;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class StatisticSyncSubscriber implements EventSubscriberInterface
{

    public const CLICKHOUSE_CACHE_KEY = 'clickhouse';

    /**
     * @var AdapterInterface
     */
    private AdapterInterface $adapter;

    public static function getSubscribedEvents()
    {
        return [
            ClickPlacedEvent::class => 'onClickPlaced',
            LeadPlacedEvent::class => 'onLeadPlaced',
            LeadUpdateEvent::class => 'onLeadUpdate',
            LeadUpdatedEvent::class => 'onLeadUpdated',
        ];
    }

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param ClickPlacedEvent $event
     * @throws InvalidArgumentException
     */
    public function onClickPlaced(ClickPlacedEvent $event)
    {
        $statistic = new Statistic();
        $statistic->setVersion(1);
        $this->setClickData($statistic, $event->getClick());
        $this->addItemToCache($statistic);
    }

    /**
     * @param LeadPlacedEvent $event
     * @throws InvalidArgumentException
     */
    public function onLeadPlaced(LeadPlacedEvent $event)
    {
        $statisticOld = new Statistic();
        $statisticOld->setVersion(-1);
        $this->setClickData($statisticOld, $event->getLead()->getClick());
        $this->addItemToCache($statisticOld);

        $statisticNew = new Statistic();
        $statisticNew->setVersion(1);
        $this->setClickLeadData($statisticNew, $event->getLead()->getClick(), $event->getLead());
        $this->addItemToCache($statisticNew);
    }

    /**
     * @param LeadUpdateEvent $event
     * @throws InvalidArgumentException
     */
    public function onLeadUpdate(LeadUpdateEvent $event)
    {
        $statistic = new Statistic();
        $statistic->setVersion(-1);
        $this->setClickLeadData($statistic, $event->getLead()->getClick(), $event->getLead());
        $this->addItemToCache($statistic);

        dump($event);
    }

    /**
     * @param LeadUpdateEvent $event
     * @throws InvalidArgumentException
     */
    public function onLeadUpdated(LeadUpdateEvent $event)
    {
        $statistic = new Statistic();
        $statistic->setVersion(1);
        $this->setClickLeadData($statistic, $event->getLead()->getClick(), $event->getLead());
        $this->addItemToCache($statistic);

        dump($event);
    }

    /**
     * @param Statistic $statistic
     * @throws InvalidArgumentException
     */
    private function addItemToCache(Statistic $statistic)
    {
        $item = $this->adapter->getItem(self::CLICKHOUSE_CACHE_KEY);

        $value = $item->get();
        if (is_array($value)) {
            $value[] = $statistic;
            $item->set($value);
        } else {
            $item->set([$statistic]);
        }

        $this->adapter->save($item);
    }

    /**
     * Наполняет Statistic из Click и Lead
     *
     * @param Statistic $statistic
     * @param Click $click
     * @param Lead $lead
     */
    private function setClickLeadData(Statistic $statistic, Click $click, Lead $lead)
    {
        $this->setClickData($statistic, $click);
        $this->setLeadData($statistic, $lead);
    }

    /**
     * Наполняет Statistic из Click
     *
     * @param Statistic $statistic
     * @param Click $click
     */
    private function setClickData(Statistic $statistic, Click $click)
    {
        $statistic->setClickId($click->getId());
        $statistic->setClickCreatedAt($click->getCreatedAt());
        $statistic->setTrafficSourceId($click->getStream()->getTrafficSource()->getId());
        $statistic->setUserId($click->getStream()->getTrafficSource()->getUser()->getId());
        $statistic->setStreamId($click->getStream()->getId());
        $statistic->setOfferId($click->getStream()->getOffer()->getId());
        $statistic->setGeoId($click->getGeo()->getId());
        $statistic->setHour($click->getCreatedAt()->format('H'));
        $statistic->setDay($click->getCreatedAt()->format('d'));
        $statistic->setWeek($click->getCreatedAt()->format('W'));
        $statistic->setMonth($click->getCreatedAt()->format('m'));
        $statistic->setYear($click->getCreatedAt()->format('Y'));
        $statistic->setDate($click->getCreatedAt());

        $this->setTokens($statistic, ...$click->getTokens());
    }

    /**
     * Наполняет Statistic из Lead
     *
     * @param Statistic $statistic
     * @param Lead $lead
     */
    private function setLeadData(Statistic $statistic, Lead $lead)
    {
        $statistic->setLeadId($lead->getId());
        $statistic->setLeadStatus($lead->getStatus());
        $statistic->setLeadPrice($lead->getPrice());
        $statistic->setLeadCurrencyCode($lead->getCurrency()->getCode());
        $statistic->setLeadCurrencyRate($lead->getCurrency()->getRate());
        $statistic->setLeadCreatedAt($lead->getCreatedAt());
    }

    /**
     * Наполняет Statistic токенами TokenDictionary
     *
     * @param Statistic $statistic
     * @param TokenDictionary ...$tokens
     */
    private function setTokens(Statistic $statistic, TokenDictionary...$tokens)
    {
        foreach ($tokens as $token) {
            switch ($token->getName()) {
                case TokenDictionary::TOKEN1_NAME:
                    $statistic->setToken1Id($token->getId());
                    break;
                case TokenDictionary::TOKEN2_NAME:
                    $statistic->setToken2Id($token->getId());
                    break;
                case TokenDictionary::TOKEN3_NAME:
                    $statistic->setToken3Id($token->getId());
                    break;
                case TokenDictionary::TOKEN4_NAME:
                    $statistic->setToken4Id($token->getId());
                    break;
                case TokenDictionary::TOKEN5_NAME:
                    $statistic->setToken5Id($token->getId());
                    break;
                case TokenDictionary::TOKEN6_NAME:
                    $statistic->setToken6Id($token->getId());
                    break;
                case TokenDictionary::TOKEN7_NAME:
                    $statistic->setToken7Id($token->getId());
                    break;
                case TokenDictionary::TOKEN8_NAME:
                    $statistic->setToken8Id($token->getId());
                    break;
                case TokenDictionary::TOKEN9_NAME:
                    $statistic->setToken9Id($token->getId());
                    break;
                case TokenDictionary::TOKEN10_NAME:
                    $statistic->setToken10Id($token->getId());
                    break;
                case TokenDictionary::TOKEN11_NAME:
                    $statistic->setToken11Id($token->getId());
                    break;
                case TokenDictionary::TOKEN12_NAME:
                    $statistic->setToken12Id($token->getId());
                    break;
                case TokenDictionary::TOKEN13_NAME:
                    $statistic->setToken13Id($token->getId());
                    break;
                case TokenDictionary::TOKEN14_NAME:
                    $statistic->setToken14Id($token->getId());
                    break;
                case TokenDictionary::TOKEN15_NAME:
                    $statistic->setToken15Id($token->getId());
                    break;
                case TokenDictionary::TOKEN16_NAME:
                    $statistic->setToken16Id($token->getId());
                    break;
            }

        }

    }
}
