<?php

namespace App\Form;

use App\Entity\Main\Lead;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LeadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status')
            ->add('phone')
            ->add('name')
            ->add('comment')
            ->add('cpaId')
            //->add('click')
            ->add('geo')
            ->add('cpaGeo')
            ->add('tokens');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lead::class,
        ]);
    }
}
