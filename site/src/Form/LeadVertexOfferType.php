<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;

class LeadVertexOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('goodID', IntegerType::class, [
                'help' => '<span class="form-text text-muted">LeadVertex goodID</span>',
                'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('quantity', IntegerType::class, [
                'help' => '<span class="form-text text-muted">LeadVertex quantity</span>',
                'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('price', MoneyType::class, [
                'help' => '<span class="form-text text-muted">LeadVertex price</span>',
                'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);
    }
}
