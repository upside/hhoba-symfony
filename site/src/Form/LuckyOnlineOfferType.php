<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LuckyOnlineOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('campaignHash', TextType::class, [
                'help' => '<span class="form-text text-muted">LuckyOnline campaignHash</span>',
                'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);
    }
}
