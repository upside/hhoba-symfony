<?php

namespace App\Form\Cabinet\Agency;

use App\Entity\Main\Agency\AgencyCabinet;
use App\Entity\Main\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MyTargetCabinetToCabinetType extends AbstractType
{

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $authStorage;

    public function __construct(TokenStorageInterface $authStorage)
    {
        $this->authStorage = $authStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var User $user */
        $user = $this->authStorage->getToken()->getUser();

        $builder
            ->add('cabinet_to', ChoiceType::class, [
                'required' => true,
                'label' => 'Куда',
                'multiple' => false,
                'choice_translation_domain' => false,
                'choices' => $user->getAgencyCabinets(),
                'choice_value' => function (?AgencyCabinet $entity) {
                    return $entity ? $entity->getId() : '';
                },
                'choice_label' => function ($choice, $key, $value) {
                    return $choice;
                },
            ])
            ->add('amount', MoneyType::class, [
                'label' => 'Сумма перевода',
                'currency' => 'USD',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
