<?php

namespace App\Form\Cabinet;

use App\Entity\Main\TrafficSource;
use App\Entity\Main\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TrafficSourceType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $auth_storage;

    public function __construct(TokenStorageInterface $auth_storage)
    {
        $this->auth_storage = $auth_storage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $source_keys = array_keys(TrafficSource::SOURCE_TYPES);

        $builder
            ->add('title', TextType::class, [
                'label' => 'Название',
                'required' => true
            ])
            ->add('source_type', ChoiceType::class, [
                'label' => 'Тип источника',
                'choices' => array_flip(TrafficSource::SOURCE_TYPES),
                'multiple' => true,
                'attr' => ['class' => 'select']
            ])
        ;

        $builder->get('source_type')->addModelTransformer(new CallbackTransformer(function ($sources) use ($source_keys) {
            return array_values(array_filter($source_keys, function($source_key) use ($sources) {
                return ($sources & $source_key) > 0;
            }));
        }, function (?array $sources) {
            return array_sum($sources ?? []);
        }));

        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event){
            /** @var TrafficSource $traffic_source */
            $traffic_source = $event->getData();
            if (!$traffic_source->getId()) {
                $traffic_source->setModeration(TrafficSource::MODERATION_PENDING);
                /** @var User $user */
                $user = $this->auth_storage->getToken()->getUser();
                $traffic_source->setUser($user);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrafficSource::class,
        ]);
    }
}
