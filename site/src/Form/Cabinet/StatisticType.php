<?php

namespace App\Form\Cabinet;

use App\Entity\Main\User;
use App\Repository\Main\GeoRepository;
use App\Repository\Main\OfferRepository;
use App\Repository\Main\StreamRepository;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class StatisticType extends AbstractType
{
    public const FIELD_GROUP_1 = 'group_1';
    public const FIELD_GROUP_2 = 'group_2';
    public const FIELD_GROUP_3 = 'group_3';
    public const FIELD_DATE = 'date';
    public const FIELD_STREAMS = 'stream_id';
    public const FIELD_OFFERS = 'offer_id';
    public const FIELD_GEO = 'geo_id';

    public const FIELD_TOKEN_1 = 'token1_id';
    public const FIELD_TOKEN_2 = 'token2_id';
    public const FIELD_TOKEN_3 = 'token3_id';
    public const FIELD_TOKEN_4 = 'token4_id';
    public const FIELD_TOKEN_5 = 'token5_id';
    public const FIELD_TOKEN_6 = 'token6_id';
    public const FIELD_TOKEN_7 = 'token7_id';
    public const FIELD_TOKEN_8 = 'token8_id';
    public const FIELD_TOKEN_9 = 'token9_id';
    public const FIELD_TOKEN_10 = 'token10_id';
    public const FIELD_TOKEN_11 = 'token11_id';
    public const FIELD_TOKEN_12 = 'token12_id';
    public const FIELD_TOKEN_13 = 'token13_id';
    public const FIELD_TOKEN_14 = 'token14_id';
    public const FIELD_TOKEN_15 = 'token15_id';
    public const FIELD_TOKEN_16 = 'token16_id';

    public const GROUP_CHOICES = [
        'Час' => 'hour',
        'День' => 'day',
        'Неделя' => 'week',
        'Месяц' => 'month',
        'Год' => 'year',
        'Поток' => 'stream_id',
        'Оффер' => 'offer_id',
        'Гео' => 'geo_id',
        'Токен 1' => 'token1_id',
        'Токен 2' => 'token2_id',
        'Токен 3' => 'token3_id',
        'Токен 4' => 'token4_id',
        'Токен 5' => 'token5_id',
        'Токен 6' => 'token6_id',
        'Токен 7' => 'token7_id',
        'Токен 8' => 'token8_id',
        'Токен 9' => 'token9_id',
        'Токен 10' => 'token10_id',
        'Токен 11' => 'token11_id',
        'Токен 12' => 'token12_id',
        'Токен 13' => 'token13_id',
        'Токен 14' => 'token14_id',
        'Токен 15' => 'token15_id',
        'Токен 16' => 'token16_id',
    ];

    /**
     * @var Security
     */
    private Security $security;

    /**
     * @var GeoRepository
     */
    private GeoRepository $geoRepository;

    /**
     * @var StreamRepository
     */
    private StreamRepository $streamRepository;

    /**
     * @var OfferRepository
     */
    private OfferRepository $offerRepository;

    public function __construct(
        Security $security,
        GeoRepository $geoRepository,
        StreamRepository $streamRepository,
        OfferRepository $offerRepository
    )
    {
        $this->security = $security;
        $this->geoRepository = $geoRepository;
        $this->streamRepository = $streamRepository;
        $this->offerRepository = $offerRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $tokenMap = $user->getParams()['tokens'] ?? [];

        $builder
            ->add(self::FIELD_GROUP_1, ChoiceType::class, [
                'required' => false,
                'label' => 'Group 1',
                'placeholder' => 'Not selected',
                'choice_translation_domain' => false,
                'choices' => self::GROUP_CHOICES,
            ])
            ->add(self::FIELD_GROUP_2, ChoiceType::class, [
                'required' => false,
                'label' => 'Group 2',
                'placeholder' => 'Not selected',
                'choice_translation_domain' => false,
                'choices' => self::GROUP_CHOICES,
            ])
            ->add(self::FIELD_GROUP_3, ChoiceType::class, [
                'required' => false,
                'label' => 'Group 3',
                'placeholder' => 'Not selected',
                'choice_translation_domain' => false,
                'choices' => self::GROUP_CHOICES,
            ])
            ->add(self::FIELD_DATE, TextType::class, [
                'label' => 'Date',
            ])
            ->add(self::FIELD_STREAMS, ChoiceType::class, [
                'required' => false,
                'label' => 'Streams',
                'multiple' => true,
                'choice_translation_domain' => false,
                'choices' => $this->streamRepository->getStatisticChoices($user),
            ])
            ->add(self::FIELD_OFFERS, ChoiceType::class, [
                'required' => false,
                'label' => 'Offers',
                'multiple' => true,
                'choice_translation_domain' => false,
                'choices' => $this->offerRepository->getStatisticChoices($user)
            ])
            ->add(self::FIELD_GEO, ChoiceType::class, [
                'required' => false,
                'label' => 'Geo',
                'multiple' => true,
                'choice_translation_domain' => false,
                'choices' => $this->geoRepository->getStatisticChoices($user),
            ])
            ->add(self::FIELD_TOKEN_1, TextType::class, ['required' => false, 'label' => $tokenMap['token_1'] ?? 'Token 1'])
            ->add(self::FIELD_TOKEN_2, TextType::class, ['required' => false, 'label' => $tokenMap['token_2'] ?? 'Token 2'])
            ->add(self::FIELD_TOKEN_3, TextType::class, ['required' => false, 'label' => $tokenMap['token_3'] ?? 'Token 3'])
            ->add(self::FIELD_TOKEN_4, TextType::class, ['required' => false, 'label' => $tokenMap['token_4'] ?? 'Token 4'])
            ->add(self::FIELD_TOKEN_5, TextType::class, ['required' => false, 'label' => $tokenMap['token_5'] ?? 'Token 5'])
            ->add(self::FIELD_TOKEN_6, TextType::class, ['required' => false, 'label' => $tokenMap['token_6'] ?? 'Token 6'])
            ->add(self::FIELD_TOKEN_7, TextType::class, ['required' => false, 'label' => $tokenMap['token_7'] ?? 'Token 7'])
            ->add(self::FIELD_TOKEN_8, TextType::class, ['required' => false, 'label' => $tokenMap['token_8'] ?? 'Token 8'])
            ->add(self::FIELD_TOKEN_9, TextType::class, ['required' => false, 'label' => $tokenMap['token_9'] ?? 'Token 9'])
            ->add(self::FIELD_TOKEN_10, TextType::class, ['required' => false, 'label' => $tokenMap['token_10'] ?? 'Token 10'])
            ->add(self::FIELD_TOKEN_11, TextType::class, ['required' => false, 'label' => $tokenMap['token_11'] ?? 'Token 11'])
            ->add(self::FIELD_TOKEN_12, TextType::class, ['required' => false, 'label' => $tokenMap['token_12'] ?? 'Token 12'])
            ->add(self::FIELD_TOKEN_13, TextType::class, ['required' => false, 'label' => $tokenMap['token_13'] ?? 'Token 13'])
            ->add(self::FIELD_TOKEN_14, TextType::class, ['required' => false, 'label' => $tokenMap['token_14'] ?? 'Token 14'])
            ->add(self::FIELD_TOKEN_15, TextType::class, ['required' => false, 'label' => $tokenMap['token_15'] ?? 'Token 15'])
            ->add(self::FIELD_TOKEN_16, TextType::class, ['required' => false, 'label' => $tokenMap['token_16'] ?? 'Token 16']);

        $builder->get(self::FIELD_DATE)->addModelTransformer(new CallbackTransformer(
            function ($dateRangeArray) {
                return is_null($dateRangeArray) ? null : $dateRangeArray['from']->format('d-m-Y') . '/' . $dateRangeArray['to']->format('d-m-Y');
            },
            function ($stringDateRange) {
                $dates = explode('/', $stringDateRange);
                return [
                    'from' => DateTime::createFromFormat('d-m-Y', $dates[0]),
                    'to' => DateTime::createFromFormat('d-m-Y', $dates[1]),
                ];
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
