<?php

namespace App\Form\Cabinet;

use App\Entity\Main\User;
use App\Repository\Main\GeoRepository;
use App\Repository\Main\OfferRepository;
use App\Repository\Main\StreamRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ClickFilterType extends AbstractType
{
    /**
     * @var Security
     */
    private Security $security;
    /**
     * @var GeoRepository
     */
    private GeoRepository $geoRepository;
    /**
     * @var StreamRepository
     */
    private StreamRepository $streamRepository;
    /**
     * @var OfferRepository
     */
    private OfferRepository $offerRepository;

    public function __construct(
        Security $security,
        GeoRepository $geoRepository,
        StreamRepository $streamRepository,
        OfferRepository $offerRepository
    )
    {
        $this->security = $security;
        $this->geoRepository = $geoRepository;
        $this->streamRepository = $streamRepository;
        $this->offerRepository = $offerRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var User $user */
        $user = $this->security->getUser();
        $tokenMap = $user->getParams()['tokens'] ?? [];

        $builder
            ->add('date')
            ->add('id', TextType::class, [
                'required' => false,
                'label' => 'ID или uuid',
            ])
            ->add('streams', ChoiceType::class, [
                'required' => false,
                'label' => 'Streams',
                'multiple' => true,
                'choice_translation_domain' => false,
                'choices' => $this->streamRepository->getStatisticChoices($user),
            ])
            ->add('geo', ChoiceType::class, [
                'required' => false,
                'label' => 'Geo',
                'multiple' => true,
                'choice_translation_domain' => false,
                'choices' => $this->geoRepository->getStatisticChoices($user),
            ])
            ->add('offers', ChoiceType::class, [
                'required' => false,
                'label' => 'Offers',
                'multiple' => true,
                'choice_translation_domain' => false,
                'choices' => $this->offerRepository->getStatisticChoices($user)
            ])
            ->add('ip', TextType::class, [
                'required' => false,
                'label' => 'IP Адресс',
            ])
            ->add('useragent')
            ->add('token_1', TextType::class, ['required' => false, 'label' => $tokenMap['token_1'] ?? 'Token 1'])
            ->add('token_2', TextType::class, ['required' => false, 'label' => $tokenMap['token_2'] ?? 'Token 2'])
            ->add('token_3', TextType::class, ['required' => false, 'label' => $tokenMap['token_3'] ?? 'Token 3'])
            ->add('token_4', TextType::class, ['required' => false, 'label' => $tokenMap['token_4'] ?? 'Token 4'])
            ->add('token_5', TextType::class, ['required' => false, 'label' => $tokenMap['token_5'] ?? 'Token 5'])
            ->add('token_6', TextType::class, ['required' => false, 'label' => $tokenMap['token_6'] ?? 'Token 6'])
            ->add('token_7', TextType::class, ['required' => false, 'label' => $tokenMap['token_7'] ?? 'Token 7'])
            ->add('token_8', TextType::class, ['required' => false, 'label' => $tokenMap['token_8'] ?? 'Token 8'])
            ->add('token_9', TextType::class, ['required' => false, 'label' => $tokenMap['token_9'] ?? 'Token 9'])
            ->add('token_10', TextType::class, ['required' => false, 'label' => $tokenMap['token_10'] ?? 'Token 10'])
            ->add('token_11', TextType::class, ['required' => false, 'label' => $tokenMap['token_11'] ?? 'Token 11'])
            ->add('token_12', TextType::class, ['required' => false, 'label' => $tokenMap['token_12'] ?? 'Token 12'])
            ->add('token_13', TextType::class, ['required' => false, 'label' => $tokenMap['token_13'] ?? 'Token 13'])
            ->add('token_14', TextType::class, ['required' => false, 'label' => $tokenMap['token_14'] ?? 'Token 14'])
            ->add('token_15', TextType::class, ['required' => false, 'label' => $tokenMap['token_15'] ?? 'Token 15'])
            ->add('token_16', TextType::class, ['required' => false, 'label' => $tokenMap['token_16'] ?? 'Token 16']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
