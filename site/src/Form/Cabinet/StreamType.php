<?php

namespace App\Form\Cabinet;

use App\Entity\Main\Offer;
use App\Entity\Main\Stream;
use App\Entity\Main\TrafficSource;
use App\Entity\Main\User;
use App\Repository\Main\TrafficSourceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class StreamType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $authStorage;

    public function __construct(TokenStorageInterface $authStorage)
    {
        $this->authStorage = $authStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $this->authStorage->getToken()->getUser();

        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('traffic_source', EntityType::class, [
                'required' => true,
                'class' => TrafficSource::class,
                'query_builder' => function (TrafficSourceRepository $repository) use ($user) {
                    return $repository->createQueryBuilder('ts')
                        ->where('ts.user = :user')
                        ->setParameter('user', $user)
                        ->orderBy('ts.title', 'ASC');
                },
                'choice_label' => 'title',
                'label' => 'Источник трафика'
            ])
            ->add('offer', EntityType::class, [
                'required' => true,
                'class' => Offer::class,
                'choice_label' => 'name',
                'label' => 'Оффер',
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event){
            /** @var Stream $stream */
            $stream = $event->getData();
            if (!$stream->getId()) {
                $stream->setStatus(Stream::STATUS_INACTIVE);
            }
        });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stream::class,
        ]);
    }
}
