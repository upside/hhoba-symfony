<?php

namespace App\Form\Cabinet;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TokenMapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('token_1', TextType::class, ['required' => false, 'label' => 'Токен 1', 'help' => 'Отображаемое название для токена 1'])
            ->add('token_2', TextType::class, ['required' => false, 'label' => 'Токен 2', 'help' => 'Отображаемое название для токена 2'])
            ->add('token_3', TextType::class, ['required' => false, 'label' => 'Токен 3', 'help' => 'Отображаемое название для токена 3'])
            ->add('token_4', TextType::class, ['required' => false, 'label' => 'Токен 4', 'help' => 'Отображаемое название для токена 4'])
            ->add('token_5', TextType::class, ['required' => false, 'label' => 'Токен 5', 'help' => 'Отображаемое название для токена 5'])
            ->add('token_6', TextType::class, ['required' => false, 'label' => 'Токен 6', 'help' => 'Отображаемое название для токена 6'])
            ->add('token_7', TextType::class, ['required' => false, 'label' => 'Токен 7', 'help' => 'Отображаемое название для токена 7'])
            ->add('token_8', TextType::class, ['required' => false, 'label' => 'Токен 8', 'help' => 'Отображаемое название для токена 8'])
            ->add('token_9', TextType::class, ['required' => false, 'label' => 'Токен 9', 'help' => 'Отображаемое название для токена 9'])
            ->add('token_10', TextType::class, ['required' => false, 'label' => 'Токен 10', 'help' => 'Отображаемое название для токена 10'])
            ->add('token_11', TextType::class, ['required' => false, 'label' => 'Токен 11', 'help' => 'Отображаемое название для токена 11'])
            ->add('token_12', TextType::class, ['required' => false, 'label' => 'Токен 12', 'help' => 'Отображаемое название для токена 12'])
            ->add('token_13', TextType::class, ['required' => false, 'label' => 'Токен 13', 'help' => 'Отображаемое название для токена 13'])
            ->add('token_14', TextType::class, ['required' => false, 'label' => 'Токен 14', 'help' => 'Отображаемое название для токена 14'])
            ->add('token_15', TextType::class, ['required' => false, 'label' => 'Токен 15', 'help' => 'Отображаемое название для токена 15'])
            ->add('token_16', TextType::class, ['required' => false, 'label' => 'Токен 16', 'help' => 'Отображаемое название для токена 16']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
