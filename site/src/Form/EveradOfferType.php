<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class EveradOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('campaign_id', IntegerType::class, [
                'help' => '<span class="form-text text-muted">LuckyOnline API Token</span>',
                'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);
    }
}
