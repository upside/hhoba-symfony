<?php

namespace App\Form;

use App\Entity\Main\Tariff;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TariffType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'Тип тарифа',
                'choices' => array_flip(Tariff::TYPES),
            ])
            ->add('value')
            ->add('offer')
            ->add('currency')
            ->add('geo')
            ->add('user')
            ->add('submit', SubmitType::class, [
                'label' => is_null($options['data']->getCreatedAt()) ? 'Save' : 'Update',
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tariff::class,
        ]);
    }
}
