<?php

namespace App\Form;

use App\AdvertisersApi\AdvertisersApiInterface;
use App\Entity\Main\Offer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, ['choices' => array_flip(Offer::STATUSES)])
            ->add('name')
            ->add('description')
            ->add('image', FileType::class, [
                'label' => 'Image',
                'mapped' => false,
                'required' => false,
                //'accept' => '.png,.jpg,.jpeg,.gif',
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/*',
                        ]
                    ])
                ],
            ]);

        switch ($options['data']->getAdvertiser()->getApiId()) {
            case AdvertisersApiInterface::ADVERTISER_LUCKY_ONLINE:
                $builder->add('params', LuckyOnlineOfferType::class);
                break;
            case AdvertisersApiInterface::ADVERTISER_LEAD_VERTEX:
                $builder->add('params', LeadVertexOfferType::class);
                break;
            case AdvertisersApiInterface::ADVERTISER_EVERAD:
                $builder->add('params', EveradOfferType::class);
                break;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
