<?php

namespace App\Form;

use App\AdvertisersApi\AdvertisersApiInterface;
use App\Entity\Main\Advertiser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertiserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                //'help' => 'Set the title of a web page',
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Status',
                'choices' => array_flip(Advertiser::STATUSES),
            ])
            ->add('submit', SubmitType::class, [
                'label' => is_null($options['data']->getCreatedAt()) ? 'Save' : 'Update',
            ]);

        switch ($options['data']->getApiId()) {
            case AdvertisersApiInterface::ADVERTISER_LUCKY_ONLINE:
                $builder->add('params', LuckyOnlineType::class);
                break;
            case AdvertisersApiInterface::ADVERTISER_LEAD_VERTEX:
                $builder->add('params', LeadVertexType::class);
                break;
            case AdvertisersApiInterface::ADVERTISER_EVERAD:
                $builder->add('params', EveradType::class);
                break;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Advertiser::class,
        ]);
    }
}
