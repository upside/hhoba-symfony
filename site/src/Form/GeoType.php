<?php

namespace App\Form;

use App\Entity\Main\Geo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_en')
            ->add('name_ru')
            ->add('iso_2')
            ->add('iso_3')
            ->add('iso_num');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Geo::class,
        ]);
    }
}
