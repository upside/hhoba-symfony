<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LeadVertexType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('token', TextType::class, [
                'label' => 'Token',
                //'help' => '<span class="form-text text-muted">LeadVertex API Token</span>',
                //'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('webmasterID', TextType::class, [
                'label' => 'WebmasterID',
                //'help' => '<span class="form-text text-muted">LeadVertex webmasterID</span>',
                //'help_html' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);
    }
}
